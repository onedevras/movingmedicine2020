<section class="section cg-tool content-section common-concerns-tool">
	<div class="container thin center">

		<div class="table share-benefits-table">

			<div class="title">
				<h5><?php echo get_field('common_concerns_title'); ?></h5>
			</div>

			<div class="description">
				<?php echo get_field('common_concerns_description'); ?>
			</div>

			<?php if(have_rows('common_concerns_content')): ?>

			<div class="accordion">

				<?php while(have_rows('common_concerns_content')): the_row(); ?>
				<?php 
					$icon = get_sub_field('icon');
					$concern = get_sub_field('concern');
					$content = get_sub_field('content');
					if( $icon && $concern ):
				?>
				<div class="accordion-item">
				<div class="image svg"><?php echo get_svg_from_url($icon['url']); ?></div>
					<div class="content">
						<header><h5><?php echo $concern; ?></h5></header>
						<?php if( $content ): ?>
						<main>
							<?php echo $content; ?>
						</main>
						<?php endif; ?>		
					</div>
					<?php if( $content ): ?>
					<div class="expander">
						<a href="#">
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"></path></svg>
						</a>
					</div>
					<?php endif; ?>
				</div>
					<?php endif; ?>
				<?php endwhile; ?>

			</div>

			<?php endif; ?>

		</div>
	</div>
</section>