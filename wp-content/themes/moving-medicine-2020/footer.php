  <?php if(get_field('activate_exit_intent_popup', 'options')) one_get_content('popups', 'exit-intent', array()); ?>

  <?php if(mm_footer_has_logos(array('endorsed','developed'))): ?>
    <aside class="logo-footer">
      <div class="container">
        <?php get_footer_logos('endorsed'); ?>
        <?php get_footer_logos('developed'); ?>
      </div>
    </aside>
  <?php endif; ?>

  <?php if(mm_footer_has_logos('commissioned')): ?>
    <aside class="logo-footer">
      <div class="container">
        <?php get_footer_logos('commissioned'); ?>
      </div>
    </aside>
  <?php endif; ?>

  <footer id="main-footer">
  	<div class="container">

      <div class="logo">
    		<a href="<?= site_url() ?: '#'; ?>" target="_blank">
          <?php one_get_content('content-parts', 'mm_logo', array("gradient_id" => "footer_gradient")); ?>
        </a>
      </div>

      <?php if ( have_rows('footer_logos_sitewide','option') ): ?>
    		<div class="authors">
    			<h6>Moving Medicine is an initiative by</h6>
          <div class="logos">
      			<?php
            while( have_rows('footer_logos_sitewide','option') ):
              the_row();
              $image = get_sub_field('logo');
              $url = get_sub_field('url');
              ?>
              <a class="logo" href="<?= $url ?: '#'; ?>">
                <img src="<?= $image['sizes']['medium']; ?>" alt="<?= $image['alt']; ?>" title="<?= $image['title']; ?>" />
              </a>
              <?php
            endwhile;
            ?>
          </div>
        </div>
      <?php endif; ?>

			<div class="social">
				<h6>Keep in touch</h6>
        <?php get_social_logos(); ?>
  		</div>

  	</div>

    <aside class="bottom-bar">
      <div class="container">
        <nav class="footer-nav">
          <?php
          wp_nav_menu(array(
            'theme_location' => 'footer-navigation',
            'container' => '',
            'menu_class' => 'footer-menu',
          ));
          ?>
        </nav>
        <div class="footer-info">
           <p>©FSEM 2020 Faculty of Sport and Exercise Medicine UK.</p>
        </div>
      </div>
    </aside>

  </footer>
  <?php wp_footer(); ?>
  </body>
</html>
