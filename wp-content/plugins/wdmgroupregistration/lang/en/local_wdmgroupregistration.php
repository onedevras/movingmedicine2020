<?php

/**
 * strings for wdmgroupregistration
 * @package local
 * @subpackage wdmgroupregistration
 * @copyright 2017
 * @author wdm
 * @license GNU
 */


$string['pluginname'] = "wdmgroupregistration";
$string['menuoption'] = "local_wdmgroupregistration";
$string['privacy:metadata'] = 'The wdm Group Registration local plugin does not store any personal data.';
