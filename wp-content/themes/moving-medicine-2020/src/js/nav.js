(function($) {

  var header,
      hamburger, 
      back_button,
      main_nav,
      patient_radios,
      submenus,
      secondaryHeader,
      secondaryHamburger,
      cg_finder_radios;

  function init() {
    header = $('#main-header');
    if(!header) return false;

    // Setup vars
    hamburger = header.find('.hamburger');
    back_button = header.find('.menu-back-button');
    main_nav = header.find('.main-menu');
    patient_radios = $('input[type="radio"][name="patients"]');
    submenus = main_nav.find('.sub-menu-wrap');
    cg_finder_radios = main_nav.find('.nav-consultation-finder .ages input[type=radio]');

    secondaryHeader = $('.secondary-header');
    secondaryHamburger = secondaryHeader.find('.secondary-hamburger');

    // Bind events
    $(document).on('click', deactivate);
    hamburger.on('click', toggle_navigation);
    header.find('*').on('focusin', showMainHeader);
    header.find('*').on('focusout', hideMainHeader);
    main_nav.find('a, input, button').on('focusin', activate);
    $('a, input, button').on('focusout', deactivate);
    submenus.on('mouseenter', showArrow);
    main_nav.find('li > a').on('click', handle_nav_click);
    back_button.on('click', handle_nav_back);
    patient_radios.on('change', update_nav_finder);
    secondaryHamburger.on('click', toggleMainHeader);
    secondaryHamburger.keyup( function(e){
      if( e.key === 'Enter' ){
        $(this).trigger('click');
      }
    });
    cg_finder_radios.on('change', updateFinderRadios);
  }

  function updateFinderRadios() {
    $('input[name=' + this.value + ']').first().attr('checked', 'checked');
  }

  function showArrow() {
    $(this).closest('.menu-item-has-children').addClass('showArrow');
    $(this).on('mouseleave', hideArrow);
  }

  function hideArrow() {
    $(this).closest('.menu-item-has-children').removeClass('showArrow');
  }

  function reposition_submenus(el) {
    if($(window).width() < 1024)
      return;

    var submenu = $(el).closest('.menu-item-has-children').find('.sub-menu-wrap');

    if(!submenu.length)
      return;

    var isFinder = submenu.hasClass('has-finder'),
      left = submenu.offset().left,
      width = submenu.width(),
      right = left + width,
      windowWidth = $(window).width(),
      values;

    if(isFinder) {
      // Center the menu
      if(right > $(window).width()) {
        values = 'translateX(-50%) translateX(-' + ( left - (windowWidth - width)/2 ) + 'px)';
      }
      else if(left < 0) {
        values = 'translateX(-50%) translateX(' + ( Math.abs(left) + (windowWidth - width)/2) + 'px)';
      }
    }
    else if(right > windowWidth) {
      values = 'translateX(-50%) translateX(' + ( windowWidth - right ) + 'px)';
    }


    if(values) {
      submenu.css({'webkitTransform': values});
      submenu.css({'msTransform': values});
      submenu.css({'Transform': values});
    }
  }

  // Deselect the submenu after clicking a menu item and moving the mouse away
  function blur() {
    $('.menu-item-has-children.active').removeClass('active').find('a').blur();
  }

  // Keeps menus visible while visitors tab through them
  function activate(el) {
    if(el && el.type)
      el = this;
    $(el).closest('.menu-item-has-children:not(.active)').addClass('active')
      .siblings().removeClass('active');
    reposition_submenus(el);
  }

  // Hides the menus when visitors tab away from them
  function deactivate(event) {
    if((!event.relatedTarget && !$(event.target).closest('.menu-item-has-children').length) || (event.relatedTarget && !$(event.relatedTarget).closest('.menu-item-has-children').length)) {
      $('.menu-item-has-children.active')
        .removeClass('active')
        .find('.sub-menu-wrap')
        .css({'-webkit-transform': '', '-ms-transform': '', 'transform': ''});
    }
  }

  function resetSubmenuTransforms() {
    submenus.css('transform', '');
  }

  function toggle_navigation() {
    header.toggleClass('menu-open');
    $('body').toggleClass('scroll-lock');
    $(this).toggleClass('active');
    resetSubmenuTransforms();
    return false;
  }

  function handle_nav_click() {
    var parent = $(this).parent(),
        submenu = parent.find('.sub-menu-wrap');

    if(submenu.length < 1) return;

    activate(this);
    
    submenu.addClass('open');
    update_back_button();
    return false;
  }

  function handle_nav_back() {
    var open_menus = $('.sub-menu-wrap.open');
    open_menus.removeClass('open');
    update_back_button();
    return false;
  }

  function update_back_button() {
    var open_menus = $('.sub-menu-wrap.open');
    if(open_menus.length > 0) back_button.addClass('show');
    else back_button.removeClass('show');
  }

  function update_nav_finder() {
    var patient = this.value;

    $('[data-patient='+patient+']').addClass('active')
      .siblings()
      .removeClass('active');
    
  }

  function toggleMainHeader(event) {
    $(document.body).toggleClass('show-main-nav');
    $(this).toggleClass('active');
  }

  function showMainHeader(event) {
    $(document.body).not('.show-main-nav').addClass('show-main-nav');
    $('.secondary-hamburger:not(.active)').addClass('active');
  }

  function hideMainHeader(event) {
    if(event.relatedTarget !== null && !$(event.relatedTarget).closest('.main-header').length) {
      $(document.body).removeClass('show-main-nav');
      $('.secondary-hamburger.active').removeClass('active');
    }
  }

  $(init);
})(jQuery);
