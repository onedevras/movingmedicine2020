<div class="list">
<?php $count = 1; ?>
<?php foreach( $args as $item ): ?>

  <?php $item['count'] = $count; ?>
  <div class="list-item">
    <?php one_get_content('content-parts', 'image-icon-numbered', $item); $count++; ?>
    <div class="content <?php echo !$item['icon_url'] && !$item['numbered'] ? 'no-image' : '' ?>">
      <header>
        <h5><?php echo $item['title']; ?></h5>
      </header>
      <div class="description">
        <?php echo autop($item['description']); ?>
      </div>
    </div>
    <?php if($item['url']): ?>
    <div class="link">
      <a class="button" target="_blank" href="<?php echo $item['url']; ?>"><?php echo $item['link_text'] ?: 'Visit'; ?></a>
    </div>
    <? endif; ?>
  </div>

<?php endforeach; ?>

</div>