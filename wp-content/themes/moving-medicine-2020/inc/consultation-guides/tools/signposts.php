<section class="cg-tool content-section signposts-tool">
	<div class="container thin center">

		<?php 
			$signposts = get_field('signposts');
			$items = array();
		?>
		<?php foreach($signposts as $item): ?>
			<?php
				$blocks = parse_blocks($item->post_content);
				foreach($blocks as $block):
					if( 'acf/mm-organisation' === $block['blockName'] ):
						$block_data = $block['attrs']['data'];
						$icon_url = wp_get_attachment_url( $block_data['icon'] );
						$items[] = array(
							'icon_round' => ('1' === $block_data['icon_round'] ? 'circular' : ''),
							'icon_url' => $icon_url,
							'title' => get_the_title( $item ),
							'description' => $block_data['description'],
							'url' => $block_data['url']
						);
					endif;
				endforeach;
			?>
		<?php endforeach; ?>
		
		<div class="title">
			<h5><?php echo get_field('signposts_title') ?></h5>
		</div>
		<?php if(get_field('signposts_description')): ?>
		<div class="description">
			<?php echo get_field('signposts_description'); ?>
		</div>
		<?php endif; ?>
		<?php one_get_content('content-parts', 'list', $items) ?>
	</div>
</section>