(function($) {

  function init() {
    $('.cycles-container a[data-impact]').click(update_steps);
    $('.cycles-container a[data-symptom]').click(change_symptom);
	}
	
	function update_steps() {
    var container = $(this).parents('.cycles').first(),
  		impact = $(this).attr('data-impact'),
			impacts = container.find('.cycles-impacts a'),
			activeSteps = container.find('.steps ul.active'),
			symptom = activeSteps.attr('data-symptom'),
			steps = container.find('.steps ul:not(.active)[data-symptom="'+symptom+'"]');
			
  	steps.addClass('active')
			.siblings()
			.removeClass('active');

  	impacts.removeClass('active');
		$(this).addClass('active');
		$(document).trigger('slideHeightChanged');
		
  	return false;
  }

  function change_symptom() {
		var container = $(this).parents('.cycles').first(),
				symptom = $(this).attr('data-symptom'),
				symptoms = container.find('a[data-symptom]'),
				activeImpacts = container.find('.cycles-impacts a.active'),
				impact = activeImpacts.attr('data-impact'),
				steps = container.find('.steps ul[data-symptom="'+symptom+'"][data-impact="'+impact+'"]');
		
		if(steps.length) {
			steps.addClass('active')
				.siblings()
				.removeClass('active');
		} else {
			container.find('.steps ul.active').removeClass('active');
		}

		symptoms.removeClass('active');
		$(this).addClass('active');
		$(document).trigger('slideHeightChanged');

  	return false;
	}
  
  $(init);
})(jQuery);
