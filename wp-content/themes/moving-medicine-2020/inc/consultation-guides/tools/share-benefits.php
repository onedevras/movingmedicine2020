<section class="cg-tool content-section share-benefits-tool">
	<div class="container thin center">

		<div class="table share-benefits-table">

			<div class="title">
				<h5><?php echo get_field('share_benefits_title') ?></h5>
			</div>

			<div class="description">
				<?php echo get_field('share_benefits_description') ?>
			</div>

			<?php if( have_rows('share_benefits_content') ): ?>

			<?php 
				$fields = get_field_object('share_benefits_content');
				$count = (count($fields['value']));
			?>

			<div class="share-benefits-container">

			<?php if($count > 1): // We only need tabs if there's nore than one condition?>
				
				<ul class="share-benefits-conditions">

				<?php $active = 'active'; ?>
				<?php while ( have_rows('share_benefits_content') ) : the_row(); ?>

					<li class="tool-item <?php echo $active ?>"><a href="#" data-condition="<?php echo the_sub_field('condition_name'); ?>"><?php echo the_sub_field('condition_name'); ?></a></li>

					<?php $active = ''; ?>
				<?php endwhile; ?>
	 				
				</ul>
			
			<?php endif; ?>
				
			<?php $active = 'active'; ?>
			<?php while ( have_rows('share_benefits_content') ) : the_row(); ?>

				<div class="table-container <?php echo $active; ?>" data-condition="<?php echo the_sub_field('condition_name'); ?>">

				<?php 
					$active = "";
					$increases = get_sub_field('increases_content');
					$decreases = get_sub_field('decreases_content');
				?>

				<?php if( $increases || $decreases ): ?>
					<?php $active = 'class="active"'; ?>

					<ul class="table-categories share-benefits-list">
						
					<?php if ( $increases ): ?>
						<li class="tool-item"><a href="#" data-category="increases" <?php echo $active; $active = "" ?>>Improves</a></li>
					<?php endif; ?>

					<?php if( $decreases ):  ?>
						<li class="tool-item"><a href="#" data-category="decreases" <?php echo $active; $active = "" ?>>Reduces</a></li>
					<?php endif; ?>

					</ul>

					<div class="table-items">
						<?php $active = 'class="active"'; ?>
						<?php if ( $increases ): ?>
						<div data-category="increases" <?php echo $active; $active = "" ?>>
							<?php foreach($increases as $benefit): ?>
							<div class="table-item">
								<?php echo get_svg_from_url( get_field( 'icon', $benefit )['url']); ?>
								<span><?php echo get_field('display_title', $benefit) ?: get_the_title($benefit); echo ' [ev id=' . $benefit->ID . ']'; ?></span>
							</div>
							<? endforeach; ?>
						</div>
						<?php endif; ?>

						<?php if ( $decreases ): ?>
						<div data-category="decreases" <?php echo $active; $active = "" ?>>
							<?php foreach($decreases as $benefit): ?>
							<div class="table-item">
								<?php echo get_svg_from_url( get_field( 'icon', $benefit )['url']); ?>
								<span><?php echo get_field('display_title', $benefit) ?: get_the_title($benefit); echo ' [ev id=' . $benefit->ID . ']'; ?></span>
							</div>
							<?php endforeach; ?>
						</div>
						<?php endif; ?>

					</div>

				<?php endif; ?>
					
				</div>
				
			<?php endwhile; ?>
			

			<div class="share-benefits-supplementary-text">
				<?php 
					$patient = get_the_terms(get_the_ID(), 'patient');
					if($patient[0]) {
						echo get_field('share_benefits_text', $patient[0]);
					}
				?>
			</div>

			</div>
		</div>

		<?php endif; ?>

	</div>
</section>