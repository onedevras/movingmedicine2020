<?php get_header(); ?>

<div class="outer page-outer" id="main">
	<div class="inner container full">
  <div class="outer page-outer" id="main">
	  <div class="container thin">

      <section class="section billboard-section full-width has-video">
        <?php 
        $bg_video = get_field('video', 'options');
        // Get the background video
        if ( $bg_video ) {
          $params = array(
            'autoplay' => 'on',
            'loop' => 'on',
            'mute' => 'on'
          );

          $bg_video = str_replace('/]', http_build_query($params, '', ' ') . ' /]', $bg_video);
          $code = do_shortcode( $bg_video );
          echo str_replace('autoplay="1"', 'autoplay muted', $code);

        }

        ?>
        <div class="content">
          <?php if( get_field('title', 'options') ): ?>
          <div class="heading">
            <h1><?php the_field('title', 'options'); ?></h1>
          </div>
          <?php endif; ?>
          <p><a href="/" class="button dark"><?php the_field('button_text', 'options'); ?></a></p>
        </div>
      </section>

    </div>
  </div>
</div>

<?php get_footer(); ?>
