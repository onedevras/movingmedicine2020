(function($) {
  function init() {
    $('.accordion .expander a, .accordion header').click(toggle_accordion);
  }

  function toggle_accordion() {
  	var item = $(this).parents('.accordion-item'),
  		content = item.find('.content main');
  	item.toggleClass('open');
    content.stop().slideToggle(function() {
      $(document).trigger('slideHeightChanged');
    });
    
  	return false;
  }
  
  $(init);
})(jQuery);
