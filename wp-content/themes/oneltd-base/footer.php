    <footer>
      <p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
      <p class="by-one">Site by <a href="http://oneltd.co.uk">One</a></p>
    </footer>
    <?php wp_footer(); ?>
  </body>
</html>
