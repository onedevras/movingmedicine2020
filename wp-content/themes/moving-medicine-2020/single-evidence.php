<?php get_header(); ?>

<?php if (have_posts()): ?>
	<?php while(have_posts()): ?>
		<?php the_post(); ?>

		<?php one_get_content('content-parts', 'article-header'); ?>
		
		<div class="outer evidence-outer" id="main">
			<div class="inner container full">
				<div class="container thin">

			<?php the_content(); ?>

				</div>
			</div>
		</div>

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>