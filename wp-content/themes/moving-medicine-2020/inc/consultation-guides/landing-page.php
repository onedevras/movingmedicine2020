<?php 
get_header();
$landing_image = get_field('condition_landing_page_image');
?>

<section id="main" class="consultation-landing" style="background-image: url(<?= $landing_image['url']; ?>)">
	<div class="container">

		<div class="content">

			<div class="details">
				<h1><?php echo the_title(); ?></h1>
				<?php $terms = get_the_terms(get_the_id(), 'patient'); ?>
				<?php if( count( $terms ) ): ?>
					<div class="description">
					<?php echo autop($terms[0]->description); ?>
					</div>
				<?php endif; ?>
			</div>

			<div class="tiles">
				<a class="one-minute" href="one_minute/">
				<?php get_svg('cg-one-minute'); ?>
					<span>The 1 minute conversation</span>
				</a>
				<a class="five-minute" href="five_minute/">
				<?php get_svg('cg-five-minute'); ?>
					<span>The 5 minute conversation</span>
				</a>
				<a class="more-minute"  href="more_minute/">
				<?php get_svg('cg-more-minute'); ?>
					<span>The more minute conversation</span>
				</a>
			</div>

		</div>
	</div>
</section>