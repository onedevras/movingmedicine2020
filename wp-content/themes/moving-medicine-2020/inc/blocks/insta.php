<?php 


$instagram = mm_instagram_posts();

?>
<section class="section content-section full-width tiles-container instagram-container <?php echo get_field('background') ?: 'white' ?>">
	<div class="container full">

    <div class="tiles" data-items="4">

      <?php if( get_field('title') ): ?>
      <div class="title">
				<h2><?php the_field('title'); ?></h2>
      </div>
      <?php endif; ?>

      <?php if( get_field('description') ): ?>
      <div class="description">
				<?php the_field('description'); ?>
      </div>
      <?php endif; ?>

			<div class="items">

      <?php $count = 0; foreach ($instagram as $djm) : $count++; if ($count > 4) continue; ?>
        <?php 
          $image_args = array(
            'icon_url' => $djm['image'],
            'icon_round' => false
          );
        ?>
        <a href="<?= $djm["link"]; ?>" target="_blank">
          <div class="tile">
            <?php one_get_content('content-parts', 'image-icon-numbered', $image_args); ?>
            <div class="content">
              <div class="content-inner">
                <span class="ig-logo"><?= get_svg('instagram-logo'); ?></span>
                <span class="ig-title"><?php $text = substr($djm["title"], 0, 192); ?>
                <?= substr($text, 0, strripos($text, " ", -1)) . ' …'; ?></span>
              </div>
            </div>
          </div>
        </a>
      <?php endforeach; ?>
			</div>
		</div>
  </div>
</section>