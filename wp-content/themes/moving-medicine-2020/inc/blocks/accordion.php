<?php if(have_rows('items')): ?>

<!-- accordion -->
<section class="section content-section full-width accordion-container <?php the_field('background') ?: 'white' ?>">
	<div class="inner container full">
	  <div class="container thin">
    <div class="accordion">

      <?php if( get_field('title') ): ?>
      <div class="title">
				<h4><?php the_field('title'); ?></h4>
      </div>
      <?php endif; ?>

      <?php $count = 1; ?>
      <?php while(have_rows('items')): the_row(); ?>
      <?php 
        $image = get_sub_field('icon');
        $title = get_sub_field('title');
        $content = get_sub_field('content');
        $image_args = array(
          'numbered' => get_field('numbered'),
          'count' => $count,
          'icon' => $image,
          'icon_round' => get_field('icon_round') ? 'circular' : false,
        );
        if( $title ):
      ?>
      <div class="accordion-item">
        <?php one_get_content('content-parts', 'image-icon-numbered', $image_args); $count++; ?>
        <div class="content">
          <header><h5><?php echo $title; ?></h5></header>
          <?php if( $content ): ?>
          <main>
            <?php echo $content; ?>
          </main>
          <?php endif; ?>		
        </div>
        <?php if( $content ): ?>
        <div class="expander">
          <a href="#">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"></path></svg>
          </a>
        </div>
        <?php endif; ?>
      </div>
      <?php endif; ?>
      <?php endwhile; ?>

      </div>
    </div>
	</div>
</section>

<?php endif; ?>