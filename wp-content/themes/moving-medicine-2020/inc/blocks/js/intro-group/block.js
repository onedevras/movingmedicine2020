( function( blocks, element, blockEditor ) {
  var el = element.createElement;
  var InnerBlocks = blockEditor.InnerBlocks;

  const INTRO_TEMPLATE = [
    [ 'acf/cg-slide-title', {}],
    [ 'core/image', { 
      align: 'left', 
      url: '/wp-content/themes/moving-medicine-2020/images/speech-bubbles.svg', 
      'alt': 'icon',
      'width': 120
      } 
    ],
    [ 'core/group', {}, [ 
      [ 'core/heading', { placeholder: 'Enter the question text…', level: '2' } ],
    ]]
  ];

  blocks.registerBlockType( 'mm/cg-intro-group', {
      title: 'Intro Group',
      category: 'mm-blocks',

      edit: function( props ) {
          return el(
              'div',
              { className: props.className + ' mm-group intro-group' },
              el(
                'div',
                { className: 'mm-group_inner intro-group_inner thin' },
                el( InnerBlocks,
                  {
                      template: INTRO_TEMPLATE,
                      templateLock: "all",
                  }
                )
              )
              
          );
      },

      save: function( props ) {
        return el(
          'div',
          { className: 'mm-group intro-group' },
          el(
            'div',
            { className: 'mm-group_inner intro-group_inner thin' },
            el( InnerBlocks.Content )
          )
        );
      },
  } );
} (
  window.wp.blocks,
  window.wp.element,
  window.wp.blockEditor,
) );