<?php
if (is_user_logged_in()) {
    //    wp_enqueue_style('wdm_bootstrap_css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css');
    global $wpdb;
    $user = wp_get_current_user();
    $tableName = $wpdb->prefix . "bp_cohort_info";
    $query = $wpdb->prepare("SELECT ID, NAME, COHORT_NAME, MDL_COHORT_ID, PRODUCTS FROM $tableName WHERE COHORT_MANAGER = %d AND SYNC='1'", $user->ID);
    $result = $wpdb->get_results($query, ARRAY_A);
    /*    $group_products = get_user_meta(get_current_user_id(), 'group_products', true);
      if (!is_array($group_products)) {
      $group_products = array();
      } */
    ?>
    <div id="wdm_eb_enroll_user_page">
        <div id='wdm_eb_message'>
            <div class="wdm_select_course_msg">
                <i class='fa fa-times-circle wdm_select_course_msg_dismiss'></i>
                <lable class='wdm_enroll_warning_message_lable'>
                    <?php _e('Please select Group', 'ebbp-textdomain');
                    ?>
            </div>
        </div>
        <div id="wdm-eb-enroll-msg"> <span></span> <i class="fa fa-times-circle wdm_grp_update_msg_dismiss"></i></div>
        <form name="wdm_eb_enroll_user" id ="wdm_eb_enroll_user" method="POST" enctype="multipart/form-data">
            <div>
                <div>
                    <div id="wdm-course-button">
                        <!-- <li class="enroll-button-grid"> -->
                            <div>
                                <span class="wdm_eb_lable"><?php _e('Select Group:', 'ebbp-textdomain');
                                ?></span>
                                <select id="edb_course_product_name" name="edb_course_product_name">
                                    <option value="0">
                                        <?php _e('Select Group', 'ebbp-textdomain'); ?>
                                    </option>
                                    <?php
                                    foreach ($result as $row) {
                                        $productsQuantity = unserialize($row['PRODUCTS']);
                                        $productsQuantity = array_values($productsQuantity);
                                        $productsQuantity = min($productsQuantity);
                                        ?>
                                        <option data-qty= "<?= $productsQuantity ?>"  data-name="<?php
                                            echo !empty($row['NAME']) ? $row['NAME'] : str_replace($user->user_login . "_", "", $row['COHORT_NAME']);
                                        ?>" value="<?php echo $row['MDL_COHORT_ID']; ?>">
                                        <?php
                                            echo !empty($row['NAME']) ? $row['NAME']."<span> (" . $productsQuantity . ") </span>" : str_replace($user->user_login . "_", "", $row['COHORT_NAME']) . "<span> (" . $productsQuantity . ") </span>";
                                        ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>


                                <!-- <div> -->
                                    <ul class="course-select">
                                        <?php
                                        /**
                                         * add quatity and add view associated course.
                                         * @since 1.1.0
                                         */
                                        ?>
                                        <!-- <li class="enroll-button-grid">
                                            <div>
                                                <button class="enroll-student-page-button" id="view-associated-button">
                                                    <?php _e('Associated Courses', 'ebbp-textdomain');?>
                                                </button>
                                            </div>
                                        </li> -->
                                        <li class="enroll-button-grid">
                                            <div>
                                                <button class="enroll-student-page-button" id="add-product-button">
                                                    <?php _e('Add Product', 'ebbp-textdomain');?>
                                                </button>
                                            </div>
                                        </li>
                                        <li class="enroll-button-grid">
                                            <div>
                                                <button class="enroll-student-page-button" id="add-quantity-button">
                                                    <?php _e('Add Quantity', 'ebbp-textdomain');?>
                                                </button>
                                            </div>
                                        </li>
                                        <li class="enroll-button-grid">
                                            <div>
                                                <button class="enroll-student-page-button" id="bp-delete-cohort">
                                                    <?php _e('Delete Group', 'ebbp-textdomain');?>
                                                </button>
                                            </div>
                                        </li>
                                    </ul>
                                <!-- </div> -->



                                <div id="loding-icon"></div>
                            </div>
                        <!-- </li> -->
                    </div>
                </div>
                <!-- <div>
                    <ul class="course-select">
                        <li class="enroll-button-grid">
                            <div>
                                <button class="enroll-student-page-button" id="add-product-button">
                                    <?php _e('Add Product', 'ebbp-textdomain');?>
                                </button>
                            </div>
                        </li>
                        <li class="enroll-button-grid">
                            <div>
                                <button class="enroll-student-page-button" id="add-quantity-button">
                                    <?php _e('Add Quantity', 'ebbp-textdomain');?>
                                </button>
                            </div>
                        </li>
                        <li class="enroll-button-grid">
                            <div>
                                <button class="enroll-student-page-button" id="bp-delete-cohort">
                                    <?php _e('Delete Group', 'ebbp-textdomain');?>
                                </button>
                            </div>
                        </li>
                    </ul>
                </div> -->

                <!-- Div to add ajax returned content and show it in pop-up -->
                <div id="add-quantity-popup"></div>
            </div>


            <!-- Associated courses -->
            <div id = "wdm_group_details">
                <div class="eb-enroll-student-tab-container">
                    <div class="eb-enroll-student-tab eb-enroll-student-tab-active" data-section="eb_enroll_students">
                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                        <?php _e("Enrollment Details", "ebbp-textdomain") ?>
                    </div>

                    <div class="eb-enroll-student-tab" data-section="eb_group_info">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <?php _e("Group Details", "ebbp-textdomain") ?>
                    </div>


                   <!--  <div class="eb-enroll-student-tab" data-section="eb_enrolled_students">
                        <i class="fa fa-users" aria-hidden="true"></i>
                        <?php _e("Enrolled Users", "ebbp-textdomain") ?>
                    </div>
                    <div class="eb-enroll-student-tab" data-section="eb_associated_courses">
                        <i class="fa fa-book" aria-hidden="true"></i>
                        <?php _e("Associated Courses", "ebbp-textdomain") ?>
                    </div> -->
                    <?php

                    /**
                     * Data section of the div should match the id of the content div.
                     */
                    do_action("eb_add_tab_on_enroll_students_page");
                    ?>
                </div>



                <div class="eb-enroll-student-tab-content">
                    <!-- enroll users section -->
                    <div id="eb_enroll_students" class="eb_enroll_students_tab_cont_section">
                        <div>
                            <!-- <div class = "wdm_enroll_subheading">
                                <?= __('Enroll New User', 'ebbp-textdomain'); ?>
                            </div> -->

                            <div id='wdm_avaliable_reg'>
                                <div class = "wdm_seats">
                                    <?php _e('Enrolled Users ( Available Seats ) :', 'ebbp-textdomain');
                                    ?>
                                    <span class = "wdm_seats_enrolled_users"> 0 </span> ( <span class = "wdm_seats_available"> 0 </span> )
                                </div>
                               <!--  <div class = "wdm_seats">
                                    <?php _e('Available Seats :', 'ebbp-textdomain');
                                    ?>
                                    <span class = "wdm_seats_available"> 0 </span>
                                </div> -->
                            </div>

                            <div id="wdm_enroll_div">
                                <div id="enroll-new-user-btn-div">
                                    <button id='enroll-new-user'>
                                        <?php _e('Enroll User', 'ebbp-textdomain');?>
                                    </button>

                                    <button id="enroll-multiple-users">
                                        <?php _e('Enroll Multiple Users', 'ebbp-textdomain');?>
                                    </button>

                                </div>
                                <div id="wdm_eb_upload_csv" class="eb_hide">
                                    <div>
                                        <div>
                                            <input id="wdm_user_csv" name="wdm_user_csv" type="file" class="file" accept=".csv" data-show-preview="false" data-show-upload="true">
                                        </div>
                                        <div>
                                            <a id="wdm_csv_link" href="<?php echo plugin_dir_url(dirname(__FILE__)) . 'upload_users_sample.csv' ?>">
                                                <?php _e('Download Sample CSV', 'ebbp-textdomain'); ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div title='Enroll User' id='enroll-user-form-pop-up'></div>
                            </div>
                        </div>

                        <!-- Enrolled users section -->
                        <div>
                            <!-- <div class = "wdm_enroll_subheading">
                                <?= __('Enrolled Ussers', 'ebbp-textdomain'); ?>
                            </div> -->

                            <!-- FROM 2.1.0 -->
                            <div id="wdm_user_delete_msg"></div>

                            <!-- LIST OF ALL ENROLLED USERS -->
                            <div class='wdm_enrolled_users'></div>
                        </div>
                    </div>


<!--                     <div id="eb_enrolled_students" class="eb_hidden_tab_content eb_enroll_students_tab_cont_section">
                    </div>
 -->

                    <!-- edit cohort name section -->
                    <div id="eb_group_info" class="eb_hidden_tab_content eb_enroll_students_tab_cont_section">
                        <div>
                            <!-- <div class = "wdm_enroll_subheading">
                                <?= __('Edit Group Name', 'ebbp-textdomain'); ?>
                            </div> -->

                            <div class="eb_tab_subsection">
                                <form>
                                    <div class="eb_edit_cohort_name_section">
                                        <div class="eb_edit_cohort_name_sub_section">
                                            <?= __("Group Name : ", "ebbp-textdomain") ?>
                                        </div>
                                        <div class="eb_edit_cohort_name_sub_section eb_edit_cohort_name_inp_sub_section">
                                            <input type="text" id="eb_inpt_edit_cohort_name" name="eb_inpt_edit_cohort_name">
                                        </div>
                                        <div class="eb_edit_cohort_name_sub_section eb_edit_cohort_name_btn_sub_section">
                                            <button type="button" id="eb_inpt_edit_cohort_name_btn" name="eb_inpt_edit_cohort_name_btn"><?= _e('Update', "ebbp-textdomain") ?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!-- EB associated courses section-->
                        <div>
                            <div id = "wdm_associated_courses_container">
                                <div class = "wdm_enroll_subheading">
                                    <?= __('Productwise Associated Courses', 'ebbp-textdomain'); ?>
                                </div>
                                <div id = "wdm_associated_courses">
                                </div>
                            </div>
                        </div>
                    </div>

<!--                     <div id="eb_associated_courses" class="eb_hidden_tab_content eb_enroll_students_tab_cont_section">

                    </div> -->
                </div>

                <?php

                /**
                 * plese make sure id of the tab should match the data-section of the tab div
                 */
                do_action("eb_add_tab_content_on_enroll_students_page");
                ?>


            </div>
        </form>
        <div title="Enroll Users" id="enroll-user-form-csv"></div>
        <div id="enroll-user-form-pop-up">
            <div id="enroll_user-pop-up">
                <div id="enroll_user_form-msg"></div>
                <form  id="enroll_user-form" method="POST" enctype="multipart/form-data">
                    <div class="enroll_user-row">
                        <label><?php _e('First name *', 'ebbp-textdomain'); ?></label>
                        <input class="wdm-enrol-form-input" id="wdm_enroll_fname" type='text' name='firstname[]' placeholder='Enter first name' value="" required/>
                    </div>
                    <div class="enroll_user-row">
                        <label><?php _e('Last name *', 'ebbp-textdomain'); ?></label>
                        <input class="wdm-enrol-form-input" id="wdm_enroll_lname" type='text' name='lastname[]' placeholder='Enter Last name' value="" required/>
                    </div>
                    <div class="enroll_user-row">
                        <label><?php _e('Email Address *', 'ebbp-textdomain'); ?></label>
                        <input class="wdm-enrol-form-input" id="wdm_enroll_email" type='email' name='email[]' placeholder='Enter Email Address' value="" required/>
                    </div>
                    <input  id='enroll_user_course' name='edb_course_product_name' type='hidden'/>
                </form>
                <div id="popup-loding-icon" class="loader pop-up-loader"></div>
            </div>
        </div>
        <!-- <form id="enroll-user-form"></form> -->
    </div>
    </div>
    <?php
} else {
    /*
     * Show Login Request Message if user is not logged in.
     * @author Pandurang
     * @since 1.0.1
     */
    ?>
    <div class="wdmebbp-wrapper-login-req alert alert-warning">
        <span>
            <?php _e('Login required to enroll users!', 'ebbp-textdomain');?>
        </span>
        <a class="btn btn-info" href="<?php echo wp_login_url(get_permalink());?>">
            <?php _e('Sign in', 'ebbp-textdomain');?>
        </a>
    </div>
    <?php
}
