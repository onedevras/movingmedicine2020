<?php /* Template Name: Finder: Activity */ ?>

<?php 
function mm_body_classes_for_activity_finder($classes) {

  $classes[] = 'consultation-guide';

  return $classes;
}
add_filter('body_class', 'mm_body_classes_for_activity_finder');
?>

<?php get_header(); ?>

<?php one_get_content('content-parts', 'article-header'); ?>

  <div class="finder_outer-container activity-finder" id="main" data-levels="4">
    <div class="container">
      <div class="finder_inner">
      <?php 
        if( false === ( $finder = get_transient('activity_finder_list') ) ):
          $finder = build_activity_finder_list();
        endif;
        if($finder): ?>
          <dl>
            <?php array_walk($finder, 'finder_from_list'); ?>
          </dl>
        <?php else: ?>
          <p>No activities were found.</p>
        <?php endif; ?>
      </div>
    </div>
  </div>

<?php get_footer(); ?>