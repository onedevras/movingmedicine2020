( function( blocks, element, blockEditor ) {
  var el = element.createElement;
  var InnerBlocks = blockEditor.InnerBlocks;


  blocks.registerBlockType( 'mm/cg-tool-group', {
      title: 'Tool Group',
      category: 'mm-blocks',

      edit: function( props ) {
          return el(
              'div',
              { className: props.className + ' mm-group tool-group' },
              el(
                'div',
                { className: 'mm-group_inner tool-group_inner thin' },
                el( InnerBlocks )
              )
              
          );
      },

      save: function( props ) {
        return el(
          'div',
          { className: 'mm-group tool-group' },
          el(
            'div',
            { className: 'mm-group_inner tool-group_inner thin' },
            el( InnerBlocks.Content )
          )
        );
      },
  } );
} (
  window.wp.blocks,
  window.wp.element,
  window.wp.blockEditor,
) );