<?php 
if(have_rows('items')):

  while(have_rows('items')): the_row();

  $items[] = array(
    'icon_url' => get_sub_field('image') ? get_sub_field('image')['url'] : false,
    'icon_type' => get_sub_field('image') ? wp_check_filetype( get_sub_field('image')['url'] )['ext'] : false,
    'icon_round' => get_field('icon_round') ? 'circular' : '',
    'numbered' => get_field('numbered_items'),
    'title' => get_sub_field('title'),
    'description' => get_sub_field('content'),
    'is_external' => get_sub_field('is_external'),
    'url' => get_sub_field('file') ? get_sub_field('file')['url'] : get_sub_field('url'),
    'link_text' => get_sub_field('file') ? strtoupper(wp_check_filetype( get_sub_field('file')['url'] )['ext']) : get_sub_field('link_text') ?: 'Visit'

  );

  endwhile;

  $background = get_field('background');
?>

<section class="section content-section full-width list-contianer <?php the_field('background') ?: 'white' ?>">
  <div class="inner container full">  
    <div class="container thin">

      <?php if( get_field('title') ): ?>
      <div class="title">
        <h4><?php the_field('title'); ?></h4>
      </div>
      <?php endif; ?>

      <?php one_get_content('content-parts', 'list', $items) ?>
    </div>
  </div>
</section>

<?php endif; ?>