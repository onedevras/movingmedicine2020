<?php

$list_item['icon_url'] = get_field('icon')['url'];
$list_item['icon_type'] = wp_check_filetype( $list_item['icon_url'] )['ext'];
$list_item['title'] = get_field('title') ?: get_the_title( $post_id );
$list_item['description'] = get_field('description');

if( get_field('is_external') ) {
  $list_item['url'] = get_field('url');
}
else {
  $list_item['url'] = get_field('file')['url'];
  $list_item['link_text'] = wp_check_filetype( $list_item['url'] )['ext'];
}

one_get_content('content-parts', 'list', [ $list_item ]);

?>
