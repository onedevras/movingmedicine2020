<?php /* Template Name: Active Hospitals Toolkit */ ?>

<?php 
function mm_body_classes_for_active_hospitals_toolkit($classes) {

  $classes[] = 'active-hospitals-toolkit';

  return $classes;
}
add_filter('body_class', 'mm_body_classes_for_active_hospitals_toolkit');
?>

<?php get_header(); 

if(is_active_hospitals_page())
	one_get_content('content-parts', 'page-navigation');
?>
<?php one_get_content('content-parts', 'article-header'); ?>
<div class="outer page-outer" id="main">
	<div class="inner container full">
		<div class="container thin">
		<?php 
		if (have_posts()):
			while(have_posts()):
				the_post();
				the_content();
			endwhile; 
		endif;
		?>
		</div>
	</div>
</div>
<?php get_footer(); ?>