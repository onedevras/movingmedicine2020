<?php if( have_rows('steps') ): ?>

<!-- Pathway -->
<section class="section content-section full-width pathway-container <?php echo get_field('background') ?: 'white' ?>">
	<div class="inner container full">
		<div class="container thin">
			<div class="pathway">

				<?php if( get_field('title') ): ?>
				<div class="title">
					<h4><?php the_field('title'); ?></h4>
				</div>
				<?php endif; ?>

				<div class="pathway-container">

					<div class="pathway-row">
						<div class="pathway-col">
							<strong><?php echo the_field('stream_1_title'); ?></strong>
						</div>
						
						<?php $steps = get_field('steps'); ?>
						<?php foreach( $steps as $step ): ?>

						<div class="pathway-col">
							<strong><?php echo $step['stream_1']?></strong>
						</div>

						<?php endforeach; ?>
						
					</div>

					<div class="pathway-row">
						<div class="pathway-col">
							<strong><?php echo the_field('stream_2_title'); ?></strong>
						</div>
						
						<?php foreach( $steps as $step ): ?>

						<div class="pathway-col">
							<?php echo $step['stream_2']?>
						</div>
						
						<?php endforeach;?>
					</div>

					<div class="pathway-row">
						<div class="pathway-col">
							<strong><?php echo the_field('stream_3_title'); ?></strong>
						</div>
						<div class="pathway-col">
							<?php the_field('stream_3')?>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
</section>

<?php endif; ?>