(function($) {

  function init() {
    $('.table:not(.share-benefits-table) a[data-category]').click(update_table);
  }

  function update_table() {
  	var table = $(this).parents('.table').first(),
    		category = $(this).attr('data-category'),
    		categories = table.find('.table-categories a'),
    		items = table.find('div[data-category='+category+']');

  	
  	items.addClass('active')
  		   .siblings()
  		   .removeClass('active');

  	categories.removeClass('active');
  	$(this).addClass('active');

    $(document).trigger('slideHeightChanged');
    
    // fix_table_position.call( table[0] );

  	return false;
  }

  function fix_table_position(){
    var table = $(this),
        table_items = table.find('.table-items'),
        table_container = table.find('.table-container'),
        list_item = table.find('a[data-category].active').parent();

    if($(window).width() < 480)
      table_items.appendTo(list_item);
    else
      table_items.appendTo(table_container);
  }
  
  $(init);
})(jQuery);
