<?php if( have_rows( 'quotes' ) ): ?>

<!-- Quote Section -->
<section class="section full-width quote-section <?php the_field('background') ?: 'white' ?>">
	<div class="container">
		<div class="quotes">

		<?php while( have_rows( 'quotes' ) ): the_row(); ?>

			<div class="quote">
				<div class="image circular" style="background-image: url('<?php echo get_sub_field('image')['url'] ?>')"></div>
				<div class="content">
					<?php the_sub_field('quote'); ?>
					<p class="name"><?php the_sub_field('name'); ?></p>
				</div>
			</div>

		<?php endwhile; ?>

		</div>
	</div>
</section>
		
<?php endif ?>