(function($) {

  function init() {
    var tippies = $('[data-evidence-content]');
    tippies.mousedown(function(e) {e.preventDefault();});
    tippies.each(initTippy); 
  }

  function initTippy() {
    var id = $(this).attr('id');
    var options = {
      theme: 'light',
      placement: 'left',
      maxWidth: 392,
      trigger: 'click focus',
      content: this.dataset.evidenceContent,
      allowHTML: true,
      interactive: true
    };

    if($('body.consultation-guide-conversation').length) {
      options.appendTo = document.body;
    }

    tippy('#' + id + '[data-evidence-content]', options);
  }
  
  $(init);
})(jQuery);
