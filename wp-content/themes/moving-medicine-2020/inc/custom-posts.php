<?php

if(!function_exists('cptui_register_my_cpts'))
	return;

function cptui_register_my_cpts() {

	/**
	 * Post Type: Conditions.
	 */
	
	$labels = [
		"name" => __( "Conditions", "custom-post-type-ui" ),
		"singular_name" => __( "Condition", "custom-post-type-ui" ),
		"all_items" => __( "All Conditions", "custom-post-type-ui" ),
		"add_new" => __( "Add new", "custom-post-type-ui" ),
		"add_new_item" => __( "Add new Condition", "custom-post-type-ui" ),
		"edit_item" => __( "Edit Condition", "custom-post-type-ui" ),
		"new_item" => __( "New Condition", "custom-post-type-ui" ),
		"view_item" => __( "View Condition", "custom-post-type-ui" ),
		"view_items" => __( "View Conditions", "custom-post-type-ui" ),
		"search_items" => __( "Search Conditions", "custom-post-type-ui" ),
		"not_found" => __( "No Conditions found", "custom-post-type-ui" ),
		"not_found_in_trash" => __( "No Conditions found in trash", "custom-post-type-ui" ),
		"parent" => __( "Parent Condition:", "custom-post-type-ui" ),
		"featured_image" => __( "Featured image for this Condition", "custom-post-type-ui" ),
		"set_featured_image" => __( "Set featured image for this Condition", "custom-post-type-ui" ),
		"remove_featured_image" => __( "Remove featured image for this Condition", "custom-post-type-ui" ),
		"use_featured_image" => __( "Use as featured image for this Condition", "custom-post-type-ui" ),
		"archives" => __( "Condition archives", "custom-post-type-ui" ),
		"insert_into_item" => __( "Insert into Condition", "custom-post-type-ui" ),
		"uploaded_to_this_item" => __( "Upload to this Condition", "custom-post-type-ui" ),
		"filter_items_list" => __( "Filter Conditions list", "custom-post-type-ui" ),
		"items_list_navigation" => __( "Conditions list navigation", "custom-post-type-ui" ),
		"items_list" => __( "Conditions list", "custom-post-type-ui" ),
		"attributes" => __( "Conditions attributes", "custom-post-type-ui" ),
		"name_admin_bar" => __( "Condition", "custom-post-type-ui" ),
		"item_published" => __( "Condition published", "custom-post-type-ui" ),
		"item_published_privately" => __( "Condition published privately.", "custom-post-type-ui" ),
		"item_reverted_to_draft" => __( "Condition reverted to draft.", "custom-post-type-ui" ),
		"item_scheduled" => __( "Condition scheduled", "custom-post-type-ui" ),
		"item_updated" => __( "Condition updated.", "custom-post-type-ui" ),
		"parent_item_colon" => __( "Parent Condition:", "custom-post-type-ui" ),
	];
	
	$args = [
		"label" => __( "Conditions", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"menu_icon" => 'data:image/svg+xml;base64,' . base64_encode('<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" id="Layer_1" data-name="Layer 1" viewBox="0 0 54 63"><path fill="#9ea3a8" d="M24.54,40.1a2.15,2.15,0,1,0,2.14,2.15A2.15,2.15,0,0,0,24.54,40.1Z"></path><path fill="#9ea3a8" d="M28.3,25.07a1.6,1.6,0,1,0,1.59,1.59A1.59,1.59,0,0,0,28.3,25.07Z"></path><path fill="#9ea3a8" d="M20.78,22.1a1.88,1.88,0,1,0-1.88-1.88A1.89,1.89,0,0,0,20.78,22.1Z"></path><path fill="#9ea3a8" d="M36.9,22.1A1.88,1.88,0,1,0,35,20.22,1.89,1.89,0,0,0,36.9,22.1Z"></path><path fill="#9ea3a8" d="M36.9,34.19A2.15,2.15,0,1,0,39,36.34,2.15,2.15,0,0,0,36.9,34.19Z"></path><path fill="#9ea3a8" d="M33.67,43.88a1.59,1.59,0,1,0,1.59,1.59A1.59,1.59,0,0,0,33.67,43.88Z"></path><path fill="#9ea3a8" d="M49.25,31.93a4.73,4.73,0,0,0,0-9.45,4.63,4.63,0,0,0-2.84,1c-.08-.22-.17-.43-.26-.65a2.68,2.68,0,1,0-2.6-4.44,1.84,1.84,0,0,0-.17-.22,3.61,3.61,0,0,0,.78-2.23,3.53,3.53,0,0,0-3.5-3.55,4.38,4.38,0,0,0-.52,0,3.66,3.66,0,0,0,.25-1.33,3.5,3.5,0,1,0-7-.07,20.39,20.39,0,0,0-2.16-.61c0-.13,0-.26,0-.39a3.5,3.5,0,1,0-7-.06c-.72.08-1.43.18-2.12.32a4.66,4.66,0,0,0-9.25.81,4.73,4.73,0,0,0,.95,2.84c-.36.26-.71.52-1.06.8a3.49,3.49,0,0,0-3.29-2.35,3.55,3.55,0,0,0-.91,7c-.27.41-.52.83-.77,1.25a3.43,3.43,0,0,0-2.08-.7,3.55,3.55,0,0,0-1.17,6.89,4.72,4.72,0,0,0,.1,9.44,4.28,4.28,0,0,0,.81-.07c.16.69.35,1.36.57,2H5.73a3.55,3.55,0,1,0,3,5.38A17.34,17.34,0,0,0,9.9,45.08a3.57,3.57,0,0,0-1.76,3.08,3.49,3.49,0,0,0,6.75,1.31l.81.5a3.6,3.6,0,0,0-.57,2,3.49,3.49,0,0,0,7,.47c.46.1.94.17,1.41.24a2.2,2.2,0,0,0,0,.36,2.69,2.69,0,0,0,5.38,0,2.58,2.58,0,0,0,0-.29,21,21,0,0,0,2.37-.41,4.66,4.66,0,0,0,9.3-.37V51.8h.19a4.69,4.69,0,0,0,4.65-4.73,4.64,4.64,0,0,0-.18-1.3l.35,0A3.53,3.53,0,0,0,49,42.26a3.57,3.57,0,0,0-1.63-3,3.1,3.1,0,0,0,.81.11,3.55,3.55,0,0,0,0-7.1h-.3c0-.18,0-.36,0-.54A4.65,4.65,0,0,0,49.25,31.93ZM36.9,16.72a3.5,3.5,0,1,1-3.5,3.5A3.5,3.5,0,0,1,36.9,16.72Zm-16.12,0a3.5,3.5,0,1,1-3.5,3.5A3.5,3.5,0,0,1,20.78,16.72ZM17,36.22a4.72,4.72,0,1,1,4.65-4.72A4.68,4.68,0,0,1,17,36.22Zm7.52,10a4,4,0,1,1,4-4A4,4,0,0,1,24.54,46.23ZM28.3,29.62a3,3,0,1,1,2.95-3A3,3,0,0,1,28.3,29.62Zm5.37,18.8a3,3,0,1,1,3-3A2.95,2.95,0,0,1,33.67,48.42Zm3.23-8.1a4,4,0,1,1,4-4A4,4,0,0,1,36.9,40.32Z"></path></svg>'),
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => [ "slug" => "consultation-guides/condition/%patient%", "with_front" => false ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "one-minute-consultation", " five-minute-consultation", " more-minute-consultation", "revisions" ],
	];
	
	register_post_type( "condition", $args );
	
	/**
	 * Post Type: Condition Slides.
	 */
	
	$labels = [
		"name" => __( "Condition Slides", "custom-post-type-ui" ),
		"singular_name" => __( "Condition Slide", "custom-post-type-ui" ),
		"menu_name" => __( "Condition Slides", "custom-post-type-ui" ),
		"all_items" => __( "Condition Slides", "custom-post-type-ui" ),
		"add_new" => __( "Add new", "custom-post-type-ui" ),
		"add_new_item" => __( "Add new Condition Slide", "custom-post-type-ui" ),
		"edit_item" => __( "Edit Condition Slide", "custom-post-type-ui" ),
		"new_item" => __( "New Condition Slide", "custom-post-type-ui" ),
		"view_item" => __( "View Condition Slide", "custom-post-type-ui" ),
		"view_items" => __( "View Condition Slides", "custom-post-type-ui" ),
		"search_items" => __( "Search Condition Slides", "custom-post-type-ui" ),
		"not_found" => __( "No Condition Slides found", "custom-post-type-ui" ),
		"not_found_in_trash" => __( "No Condition Slides found in trash", "custom-post-type-ui" ),
		"parent" => __( "Parent Condition Slide:", "custom-post-type-ui" ),
		"featured_image" => __( "Featured image for this Condition Slide", "custom-post-type-ui" ),
		"set_featured_image" => __( "Set featured image for this Condition Slide", "custom-post-type-ui" ),
		"remove_featured_image" => __( "Remove featured image for this Condition Slide", "custom-post-type-ui" ),
		"use_featured_image" => __( "Use as featured image for this Condition Slide", "custom-post-type-ui" ),
		"archives" => __( "Condition Slide archives", "custom-post-type-ui" ),
		"insert_into_item" => __( "Insert into Condition Slide", "custom-post-type-ui" ),
		"uploaded_to_this_item" => __( "Upload to this Condition Slide", "custom-post-type-ui" ),
		"filter_items_list" => __( "Filter Condition Slides list", "custom-post-type-ui" ),
		"items_list_navigation" => __( "Condition Slides list navigation", "custom-post-type-ui" ),
		"items_list" => __( "Condition Slides list", "custom-post-type-ui" ),
		"attributes" => __( "Condition Slides attributes", "custom-post-type-ui" ),
		"name_admin_bar" => __( "Condition Slide", "custom-post-type-ui" ),
		"item_published" => __( "Condition Slide published", "custom-post-type-ui" ),
		"item_published_privately" => __( "Condition Slide published privately.", "custom-post-type-ui" ),
		"item_reverted_to_draft" => __( "Condition Slide reverted to draft.", "custom-post-type-ui" ),
		"item_scheduled" => __( "Condition Slide scheduled", "custom-post-type-ui" ),
		"item_updated" => __( "Condition Slide updated.", "custom-post-type-ui" ),
		"parent_item_colon" => __( "Parent Condition Slide:", "custom-post-type-ui" ),
	];
	
	$args = [
		"label" => __( "Condition Slides", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=condition",
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => [ "slug" => "condition_slide", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "revisions" ],
	];
	
	register_post_type( "condition_slide", $args );
	
	/**
	 * Post Type: Slide Extras.
	 */
	
	$labels = [
		"name" => __( "Slide Extras", "custom-post-type-ui" ),
		"singular_name" => __( "Slide Extra", "custom-post-type-ui" ),
		"menu_name" => __( "Slide Extras", "custom-post-type-ui" ),
		"all_items" => __( "Slide Extras", "custom-post-type-ui" ),
		"add_new" => __( "Add new", "custom-post-type-ui" ),
		"add_new_item" => __( "Add new Slide Extra", "custom-post-type-ui" ),
		"edit_item" => __( "Edit Slide Extra", "custom-post-type-ui" ),
		"new_item" => __( "New Slide Extra", "custom-post-type-ui" ),
		"view_item" => __( "View Slide Extra", "custom-post-type-ui" ),
		"view_items" => __( "View Slide Extras", "custom-post-type-ui" ),
		"search_items" => __( "Search Slide Extras", "custom-post-type-ui" ),
		"not_found" => __( "No Slide Extras found", "custom-post-type-ui" ),
		"not_found_in_trash" => __( "No Slide Extras found in trash", "custom-post-type-ui" ),
		"parent" => __( "Parent Slide Extra:", "custom-post-type-ui" ),
		"featured_image" => __( "Featured image for this Slide Extra", "custom-post-type-ui" ),
		"set_featured_image" => __( "Set featured image for this Slide Extra", "custom-post-type-ui" ),
		"remove_featured_image" => __( "Remove featured image for this Slide Extra", "custom-post-type-ui" ),
		"use_featured_image" => __( "Use as featured image for this Slide Extra", "custom-post-type-ui" ),
		"archives" => __( "Slide Extra archives", "custom-post-type-ui" ),
		"insert_into_item" => __( "Insert into Slide Extra", "custom-post-type-ui" ),
		"uploaded_to_this_item" => __( "Upload to this Slide Extra", "custom-post-type-ui" ),
		"filter_items_list" => __( "Filter Slide Extras list", "custom-post-type-ui" ),
		"items_list_navigation" => __( "Slide Extras list navigation", "custom-post-type-ui" ),
		"items_list" => __( "Slide Extras list", "custom-post-type-ui" ),
		"attributes" => __( "Slide Extras attributes", "custom-post-type-ui" ),
		"name_admin_bar" => __( "Slide Extra", "custom-post-type-ui" ),
		"item_published" => __( "Slide Extra published", "custom-post-type-ui" ),
		"item_published_privately" => __( "Slide Extra published privately.", "custom-post-type-ui" ),
		"item_reverted_to_draft" => __( "Slide Extra reverted to draft.", "custom-post-type-ui" ),
		"item_scheduled" => __( "Slide Extra scheduled", "custom-post-type-ui" ),
		"item_updated" => __( "Slide Extra updated.", "custom-post-type-ui" ),
		"parent_item_colon" => __( "Parent Slide Extra:", "custom-post-type-ui" ),
	];
	
	$args = [
		"label" => __( "Slide Extras", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=condition",
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "slide_extra", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "revisions" ],
	];
	
	register_post_type( "slide_extra", $args );
	
	/**
	 * Post Type: Hospital Resources.
	 */
	
	$labels = [
		"name" => __( "Hospital Resources", "custom-post-type-ui" ),
		"singular_name" => __( "Hospital Resource", "custom-post-type-ui" ),
		"item_updated" => __( "Resource updated.", "custom-post-type-ui" ),
	];
	
	$args = [
		"label" => __( "Hospital Resources", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"menu_icon" => 'data:image/svg+xml;base64,' . base64_encode('<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="clinic-medical" class="svg-inline--fa fa-clinic-medical fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="#9ea3a8" d="M288 115L69.47 307.71c-1.62 1.46-3.69 2.14-5.47 3.35V496a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V311.1c-1.7-1.16-3.72-1.82-5.26-3.2zm96 261a8 8 0 0 1-8 8h-56v56a8 8 0 0 1-8 8h-48a8 8 0 0 1-8-8v-56h-56a8 8 0 0 1-8-8v-48a8 8 0 0 1 8-8h56v-56a8 8 0 0 1 8-8h48a8 8 0 0 1 8 8v56h56a8 8 0 0 1 8 8zm186.69-139.72l-255.94-226a39.85 39.85 0 0 0-53.45 0l-256 226a16 16 0 0 0-1.21 22.6L25.5 282.7a16 16 0 0 0 22.6 1.21L277.42 81.63a16 16 0 0 1 21.17 0L527.91 283.9a16 16 0 0 0 22.6-1.21l21.4-23.82a16 16 0 0 0-1.22-22.59z"></path></svg>'),
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "resource", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "revisions" ],
	];
	
	register_post_type( "resource", $args );
	
	/**
	 * Post Type: Activities/Organisations.
	 */
	
	$labels = [
		"name" => __( "Activities / Organisations", "custom-post-type-ui" ),
		"singular_name" => __( "Activity / Organisation", "custom-post-type-ui" ),
		"all_items" => __( "All Activities", "custom-post-type-ui" ),
		"add_new" => __( "Add new", "custom-post-type-ui" ),
		"add_new_item" => __( "Add new Organisation", "custom-post-type-ui" ),
		"edit_item" => __( "Edit Organisation", "custom-post-type-ui" ),
		"new_item" => __( "New Organisation", "custom-post-type-ui" ),
		"view_item" => __( "View Organisation", "custom-post-type-ui" ),
		"view_items" => __( "View Organisations", "custom-post-type-ui" ),
		"search_items" => __( "Search Organisations", "custom-post-type-ui" ),
		"not_found" => __( "No Organisations found", "custom-post-type-ui" ),
		"not_found_in_trash" => __( "No Organisations found in trash", "custom-post-type-ui" ),
		"parent" => __( "Parent Organisation:", "custom-post-type-ui" ),
		"featured_image" => __( "Featured image for this Organisation", "custom-post-type-ui" ),
		"set_featured_image" => __( "Set featured image for this Organisation", "custom-post-type-ui" ),
		"remove_featured_image" => __( "Remove featured image for this Organisation", "custom-post-type-ui" ),
		"use_featured_image" => __( "Use as featured image for this Organisation", "custom-post-type-ui" ),
		"archives" => __( "Organisation archives", "custom-post-type-ui" ),
		"insert_into_item" => __( "Insert into Organisation", "custom-post-type-ui" ),
		"uploaded_to_this_item" => __( "Upload to this Organisation", "custom-post-type-ui" ),
		"filter_items_list" => __( "Filter Organisations list", "custom-post-type-ui" ),
		"items_list_navigation" => __( "Organisations list navigation", "custom-post-type-ui" ),
		"items_list" => __( "Organisations list", "custom-post-type-ui" ),
		"attributes" => __( "Organisations attributes", "custom-post-type-ui" ),
		"name_admin_bar" => __( "Organisation", "custom-post-type-ui" ),
		"item_published" => __( "Organisation published", "custom-post-type-ui" ),
		"item_published_privately" => __( "Organisation published privately.", "custom-post-type-ui" ),
		"item_reverted_to_draft" => __( "Organisation reverted to draft.", "custom-post-type-ui" ),
		"item_scheduled" => __( "Organisation scheduled", "custom-post-type-ui" ),
		"item_updated" => __( "Organisation updated.", "custom-post-type-ui" ),
		"parent_item_colon" => __( "Parent Organisation:", "custom-post-type-ui" ),
	];
	
	$args = [
		"label" => __( "Activities/Organisations", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"menu_icon" => 'data:image/svg+xml;base64,' . base64_encode('<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="snowboarding" class="svg-inline--fa fa-snowboarding fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="#9ea3a8" d="M432 96c26.5 0 48-21.5 48-48S458.5 0 432 0s-48 21.5-48 48 21.5 48 48 48zm28.8 153.6c5.8 4.3 12.5 6.4 19.2 6.4 9.7 0 19.3-4.4 25.6-12.8 10.6-14.1 7.8-34.2-6.4-44.8l-111.4-83.5c-13.8-10.3-29.1-18.4-45.4-23.8l-63.7-21.2-26.1-52.1C244.7 2 225.5-4.4 209.7 3.5c-15.8 7.9-22.2 27.1-14.3 42.9l29.1 58.1c5.7 11.4 15.6 19.9 27.7 24l16.4 5.5-41.2 20.6c-21.8 10.9-35.4 32.8-35.4 57.2v53.1l-74.1 24.7c-16.8 5.6-25.8 23.7-20.2 40.5 1.7 5.2 4.9 9.4 8.7 12.9l-38.7-14.1c-9.7-3.5-17.4-10.6-21.8-20-5.6-12-19.9-17.2-31.9-11.6s-17.2 19.9-11.6 31.9c9.8 21 27.1 36.9 48.9 44.8l364.8 132.7c9.7 3.5 19.7 5.3 29.7 5.3 12.5 0 24.9-2.7 36.5-8.2 12-5.6 17.2-19.9 11.6-31.9S474 454.7 462 460.3c-9.3 4.4-19.8 4.8-29.5 1.3l-90.8-33.1c8.7-4.1 15.6-11.8 17.8-21.9l21.9-102c3.9-18.2-3.2-37.2-18.1-48.4l-52-39 66-30.5 83.5 62.9zm-144.4 51.7l-19.7 92c-1.5 7.1-.1 13.9 2.8 20l-169.4-61.6c2.7-.2 5.4-.4 8-1.3l85-28.4c19.6-6.5 32.8-24.8 32.8-45.5V256l60.5 45.3z"></path></svg>'),
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "organisation", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "revisions" ],
	];
	
	register_post_type( "organisation", $args );
	
	/**
	 * Post Type: Patient Information.
	 */
	
	$labels = [
		"name" => __( "Patient Information", "custom-post-type-ui" ),
		"singular_name" => __( "Patient Information", "custom-post-type-ui" ),
		"all_items" => __( "Patient Information", "custom-post-type-ui" ),
	];
	
	$args = [
		"label" => __( "Patient Information", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=condition",
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "patient_information", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "revisions" ],
		"taxonomies" => [ "condition-tax" ],
	];
	
	register_post_type( "patient_information", $args );

  /**
   * Post Type: Evidence.
   */

  $labels = [
    "name" => __("Evidence", "custom-post-type-ui"),
    "singular_name" => __("Evidence", "custom-post-type-ui"),
    "all_items" => __("All Evidence", "custom-post-type-ui"),
    "add_new" => __("Add new", "custom-post-type-ui"),
    "add_new_item" => __("Add new Evidence", "custom-post-type-ui"),
    "edit_item" => __("Edit Evidence", "custom-post-type-ui"),
    "new_item" => __("New Evidence", "custom-post-type-ui"),
    "view_item" => __("View Evidence", "custom-post-type-ui"),
    "view_items" => __("View Evidence", "custom-post-type-ui"),
    "search_items" => __("Search Evidence", "custom-post-type-ui"),
    "not_found" => __("No Evidence found", "custom-post-type-ui"),
    "not_found_in_trash" => __("No Evidence found in trash", "custom-post-type-ui"),
    "parent" => __("Parent Evidence:", "custom-post-type-ui"),
    "featured_image" => __("Featured image for this Evidence", "custom-post-type-ui"),
    "set_featured_image" => __("Set featured image for this Evidence", "custom-post-type-ui"),
    "remove_featured_image" => __("Remove featured image for this Evidence", "custom-post-type-ui"),
    "use_featured_image" => __("Use as featured image for this Evidence", "custom-post-type-ui"),
    "archives" => __("Evidence archives", "custom-post-type-ui"),
    "insert_into_item" => __("Insert into Evidence", "custom-post-type-ui"),
    "uploaded_to_this_item" => __("Upload to this Evidence", "custom-post-type-ui"),
    "filter_items_list" => __("Filter Evidence list", "custom-post-type-ui"),
    "items_list_navigation" => __("Evidence list navigation", "custom-post-type-ui"),
    "items_list" => __("Evidence list", "custom-post-type-ui"),
    "attributes" => __("Evidence attributes", "custom-post-type-ui"),
    "name_admin_bar" => __("Evidence", "custom-post-type-ui"),
    "item_published" => __("Evidence published", "custom-post-type-ui"),
    "item_published_privately" => __("Evidence published privately.", "custom-post-type-ui"),
    "item_reverted_to_draft" => __("Evidence reverted to draft.", "custom-post-type-ui"),
    "item_scheduled" => __("Evidence scheduled", "custom-post-type-ui"),
    "item_updated" => __("Evidence updated.", "custom-post-type-ui"),
    "parent_item_colon" => __("Parent Evidence:", "custom-post-type-ui"),
  ];

  $args = [
    "label" => __("Evidence", "custom-post-type-ui"),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => false,
    "show_in_menu" => true,
    "menu_icon" => 'data:image/svg+xml;base64,' . base64_encode('<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="book-open" class="svg-inline--fa fa-book-open fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="#9ea3a8" d="M542.22 32.05c-54.8 3.11-163.72 14.43-230.96 55.59-4.64 2.84-7.27 7.89-7.27 13.17v363.87c0 11.55 12.63 18.85 23.28 13.49 69.18-34.82 169.23-44.32 218.7-46.92 16.89-.89 30.02-14.43 30.02-30.66V62.75c.01-17.71-15.35-31.74-33.77-30.7zM264.73 87.64C197.5 46.48 88.58 35.17 33.78 32.05 15.36 31.01 0 45.04 0 62.75V400.6c0 16.24 13.13 29.78 30.02 30.66 49.49 2.6 149.59 12.11 218.77 46.95 10.62 5.35 23.21-1.94 23.21-13.46V100.63c0-5.29-2.62-10.14-7.27-12.99z"></path></svg>'),
    "show_in_nav_menus" => true,
    "delete_with_user" => false,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => ["slug" => "evidence", "with_front" => true],
    "query_var" => true,
    "supports" => ["title", "editor", "thumbnail", "revisions"],
  ];

  register_post_type("evidence", $args);
	}
	
	add_action( 'init', 'cptui_register_my_cpts' );
	