(function($) {

  function init() {
    activity_graph();
    activity_get_result();
    activity_reset();
    results_text();
  }


  function results_text( risk ) {
    $('.results-text').each(function(){
      if( $(this).hasClass( risk )  ) {
        $(this).addClass('active');
      } else {
        $(this).removeClass('active');
      }
    });
  }

  function activity_reset() {
    $('.activity_days').first().val('');
    $('.activity_mins').first().val('');
    set_activity_graph(0);
    reset_lamp();

    var btn = $('#restart_activity');

    btn.on('click', function(){

      $('.activity_days').first().val('');
      $('.activity_mins').first().val('');
      set_activity_graph(0);
      reset_lamp();

      $('html, body').animate({
          scrollTop: $(".activity-calculator").offset().top
      }, 400);

    });
  }

  function validateActivityInput(days, mins) {

      days = parseInt( days );
      mins = parseInt( mins );

      if( days > 7 || days < 1 || typeof days !== 'number' || isNaN(days) ) {

        alert('Days: Pick a number between 1-7, please!');
        return false;
      }

      if( mins < 0 || typeof mins !== 'number' || isNaN(mins) ) {

        alert('Mins: Please fill this input to get a result.');
        return false;
      }

      return true;

  }

  function activity_get_result() {

    var btn = $('#activity_get_results');
    var secret = $('.activity-calculator .secret');
    btn.on('click', function(){
      var days = parseInt( $('.activity_days').first().val() );
      var mins = parseInt( $('.activity_mins').first().val() );

      if( validateActivityInput(days, mins) ){
        secret.slideDown(function() {
          $(document).trigger('slideHeightChanged');
        });
        
        set_activity_graph( days * mins );
        $('html, body').animate({
          scrollTop: secret.first().offset().top
        }, 300);

      }
    });
  }

  function set_activity_graph( val ) {

    var num = val;
    if( num > 300 ){ num = 300; }

    //this converts value to pixels
    var my_val = (( ($('.activity-graph').width()-9) / 300 )   ) * num;
    $('.activity-graph').find('.meter').css('left', my_val );

    set_lamp();
    if( num <= 30 ) { set_lamp(1); }
    if( num > 30 && num < 150) { set_lamp(2); }
    if( num >=150 ){ set_lamp(3); }


  }

  function set_lamp( val ) {

      reset_lamp();

      switch( val ) {

        case 1: {

            results_text('high');
        } break;


        case 2: {

            results_text('medium');
        } break;


        case 3: {

            results_text('low');
        } break;


        default: {

            reset_lamp();
        } break;
      }
  }

  function reset_lamp() {

    $('.lamp-light').each(function(){

      $(this).removeClass('active');
    });
  }

  function activity_graph() {

    var meter = $('.activity-graph').find('.meter');

    if (typeof meter.draggable === "undefined")
      return;

    meter.draggable({ 
      axis: "x", 
      containment: "parent", 
      drag: function(){

      var value = meter.position().left / (( ($('.activity-graph').width()-9) / 100 )   ) * 3;

      if( value > 300 ){ value = 300; } 

      $('.activity_mins').val( Math.floor( value / 7 ) );
      $('.activity_days').val( 7 ); 

      var num = value;

      if( num <= 30 ) { set_lamp(1); }
      if( num > 30 && num < 150) { set_lamp(2); }
      if( num >=150 ){ set_lamp(3); }

    },

    start: function(){

        meter.addClass('no-transition');
    },
    stop: function(){

        meter.removeClass('no-transition');
    }, 
  });

    activity_reset();
  }

  $(init);
})(jQuery);
