
(function($) {
  function setup_exit_intent(){

    // Bind exit intent
    $.exitIntent('enable');
    $(document).bind('exitintent', fire_exit_intent);

    // Bind close button
    $('.exit-intent-form .top-bar a').on('click', function(){
      $('.exit-intent-forms').fadeOut();
      return false;
    });

  }

  function fire_exit_intent(){
    var forms = $('.exit-intent-form'),
        rand = Math.floor(Math.random() * forms.length),
        form = forms.eq(rand);

    form.siblings().remove();

    // Check cookie
    if($.cookie("mm-exit-intent") === "true") return;

    // Show form and set cookie
    forms.parent().fadeIn();
    form.fadeIn();
    $.cookie("mm-exit-intent", "true", { expires: 10, path: '/' });
  }


  function init() {
	  setup_exit_intent();
  }

  $(init);
})(jQuery);