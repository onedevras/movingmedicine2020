<?php 

if( have_rows('tiles') ):
  $fields = get_field_object('tiles');
  $count = (count($fields['value']));

?>

<!-- Tiles -->
<section class="section content-section full-width tiles-container <?php echo get_field('background') ?: 'white' ?>">
  <?php if( 'thin' === get_field('width')): ?>
  <div class="inner container full">
  <?php endif; ?>
	<div class="container <?php echo get_field('width') ?>">

    <div class="tiles" data-items="<?php echo $count; ?>">

      <?php if( get_field('title') ): ?>
      <div class="title">
				<h4><?php the_field('title'); ?></h4>
      </div>
      <?php endif; ?>

			<div class="items">

      <?php $count = 1; ?>
      <?php while( have_rows('tiles') ): the_row(); ?>
      <?php 
        $image_args = array(
          'numbered' => get_field('numbered'),
          'count' => $count,
          'icon' => get_sub_field('icon'),
          'icon_round' => get_field('icon_round') ? 'circular' : false,
          'placeholder' => true
        );
      ?>
				<div class="tile">
          <?php one_get_content('content-parts', 'image-icon-numbered', $image_args); $count++; ?>
          <?php if( get_sub_field('title') || get_sub_field('description')): ?>
            <div class="content">
            <?php if( get_sub_field('title') ): ?>
              <h4><?php the_sub_field('title') ?></h4>
            <?php endif; ?>
            <?php if( get_sub_field('description') ): ?>
              <?php the_sub_field('description') ?>
            <?php endif; ?>
            </div>
          <?php endif; ?>
          <?php if( get_sub_field('url') ): ?>
					<div class="link">
						<a href="<?php the_sub_field('url') ?>" class="button"><?php echo get_sub_field('link_text') ?: 'View' ?></a>
          </div>
          <?php endif; ?>
        </div>
      <?php endwhile; ?>

			</div>
		</div>
  </div>
  <?php if( 'thin' === get_field('width')): ?>
  </div>
  <?php endif; ?>
</section>

<?php endif; ?>