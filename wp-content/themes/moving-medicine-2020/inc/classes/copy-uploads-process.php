<?php

class WP_Copy_Uploads_Process extends WP_Background_Process {

	/**
	 * @var string
	 */
	protected $action = 'copy_uploads_process';

	/**
	 * Task
	 *
	 * Override this method to perform any actions required on each
	 * queue item. Return the modified item for further processing
	 * in the next pass through. Or, return false to remove the
	 * item from the queue.
	 *
	 * @param mixed $item Queue item to iterate over
	 *
	 * @return mixed
	 */
	protected function task( $item ) {
		$src = $item['src'];
		$dst = $item['dst'];
		
		// If src doesn't exist then remove from queue
		if(!file_exists($src)) {
			// _log( '😳 No src file.');
			return false;
		}

		// If destination already exists then remove from queue
		if(file_exists($dst)) {
			// _log( '😅 File exists at dst.');
			return false;
		}

		// Create directory
		$dst_dir = dirname($dst);
		if( !file_exists($dst_dir) ) {
			if( !mkdir( $dst_dir, 0777, true) ) {
				// _log( '😳 Can\'t make diretory.');
				return false;
			}
		}

		// Copy the file
		if(copy($src, $dst)) {
			// _log( '🎉 File copied.');
		}
		else {
			// _log( '😳 Can\'t copy file.');
			return false;
		}
	}

	/**
	 * Complete
	 *
	 * Override if applicable, but ensure that the below actions are
	 * performed, or, call parent::complete().
	 */
	protected function complete() {
		parent::complete();

		// Show notice to user or perform some other arbitrary task...
	}
}

$process_all = new WP_Copy_Uploads_Process();