<?php

// TODO 👉 Sort tab index and make them accessible
function evidence_shortcode( $atts = array() ) {

  $id = $atts['id'];
  $post = get_post($id);

  if(!$post)
    return;

  $is_reference = get_field('is_reference', $id);

  if($is_reference) {
    $post_content = $post->post_content;
    $post_content = apply_filters('the_content', $post_content);
    $post_content = str_replace(']]>', ']]&gt;', $post_content);
    $content = str_replace('<a ', '<a target="_blank" ', $post_content);
  }
  else {
    // TODO 👉 Localisation stuff for this
    $content .= '<div class="evidence-link"><p class="evidence-content evidence-' . $id . '">Read the evidence about<br> <a href="' . get_post_permalink( $post ) . '" target="_blank">' . get_the_title( $post ) . '</a></p><a href="' . get_post_permalink( $post ) . '" target="_blank"><i class="icon far fa-external-link-alt"></i></a></div>';
  }

  $evidence .= '<i tabindex="0" id="evidence-' . uniqid() . '" class="far fa-plus-circle evidence-marker" data-evidence-content="' . htmlspecialchars($content) . '"></i>';

  return $evidence;
}

add_shortcode('ev', 'evidence_shortcode');

function phpqrcode( $atts = array()) {
	// set up default parameters
    extract(shortcode_atts(array(
     'data' => '',
     'width' => 12,
     'link' => '#',
	 'title' => '',
	 'target' => '_blank'
    ), $atts));

	$qr_code_width = (int) ($width / 25);
	ob_start();
	QRCode::png($data, null,'L', $width, 0);
	$imageString = base64_encode( ob_get_contents() );
	ob_end_clean();

	$image_width = $width * 25;

	return '<span class="su-qrcode su-qrcode-align-none qrcode-image su-qrcode-clickable">
			<a href="'.$link.'" target="'.$target.'" title="'.$title.'">
			<img loading="lazy" decoding="async" async src="data:image/png;base64,'.$imageString.'" alt="'.$title.'" width="'.$image_width.'">
			</a>
			</span>';

}
add_shortcode('phpqrcode', 'phpqrcode');