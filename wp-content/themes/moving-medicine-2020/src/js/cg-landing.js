(function($) {
  function init() {
    if(!document.querySelectorAll(".consultation-guide-landing").length)
      return;

    $(".tiles a").mouseover(moveArrow);
  }

  function moveArrow(event) {
    $(".details").attr("class", "details " + $(this).attr('class'));

  	return false;
  }
  
  $(init);
})(jQuery);
