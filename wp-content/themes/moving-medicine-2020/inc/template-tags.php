<?php

// add functions here to get bits of content, then use these in your
// theme. e.g. function sitename_course_title() { return get_field('course_title');
// then use <h1><?php echo sitename_course_title(); ></h1>
//
// this enables front and backend to work seperately. frontend can define
// the HTML they'd like, and backend can fill out functions to return specified html

$slide_num = 0;
/**
 *
 * Walk an array and convert to a list.
 *
 * @param array $options Options are:
 * multi – output a multi-level list or a flat one
 * item_classes - Add classes to each list item
 * submenu_classes - Add classes to each new submenu
 *
 **/
function array_to_list($item, $key, $options = array()) {
	// Setting options defaults
	// Set $multi if unset or set to true, if false is passed then make it single
	$options['multi'] = $multi = null === $options['multi'] || $options['multi'] ? true : false;
	// Set $item_classes and $submenu_classes to the values passed in or set them to ''
	$options['item_classes'] = $options['item_classes'] ?: '';
	$options['submenu_classes'] = $options['submenu_classes'] ?: '';
	global $slide_num;

	$item_with_classes = $options['item_classes'] ? '<li class="' . $options['item_classes'] . '">' : '<li>';
	$submenu_with_classes = $options['submenu_classes'] ? '<ul class="' . $options['submenu_classes'] . '"' : '<ul>';

	if (is_array($item)) {
		if ($multi) {
			echo <<<EOT
              $item_with_classes
                <a href="?$key" data-slide-index="$slide_num">$key</a>
                $submenu_with_classes
      EOT;
			array_walk($item, 'array_to_list', $options);
			echo <<<EOT
                </ul>
              </li>
      EOT;
		} else {
			array_walk($item, 'array_to_list', $options);
		}
	} elseif (null !== $item) {
		echo <<<EOT
                $item_with_classes
                  <a href="?$item" data-slide-index="$slide_num">$item</a>
                </li>
    EOT;
		$slide_num++;
	}
}

/**
 *
 * Generate a new menu with classes
 *
 * @param string $nav_classes A string of classes to add to any list item generated
 *
 **/
function generate_menu_with_classes($menu_items, $nav_classes = '', $additional_items = '')
{
  if (count($menu_items)) {
    $classes = esc_attr($nav_classes); ?>

    <nav class="<?php echo $classes; ?>">
      <ul>

        <?php
        // Walk through the multi-dimensional array to generate list code
        array_walk($menu_items, 'array_to_list');
        echo $additional_items;
        ?>

      </ul>
    </nav>
  <?php
  }
}



// Consultation guide specific function so keeping here
function get_content_for_section($section, $count = 0, $name = '') {

	$slides_from_five = array(
		 'focus_m' => array(
			  'find_out' => 'explore_5',
			  'share' => 'explore_5',
			  'encourage' => 'explore_5',
		 ),
		 'strengthen_m' => array(
			  'make_it_personal' => 'explore_5'
		 ),
		 'decide_m' => array(
			  'summarise' => 'decide_5',
			  'ask' => 'decide_5'
		 ),
		 'plan_m' => array(
			  'agree' => 'decide_5'
		 ),
		 'support_m' => array(
			  'signpost' => 'decide_5'
		 )
	);

	$slides = $section['slides'];
	$did_you_knows = $section['did_you_know'];
	$real_impacts = $section['real_impact'];

	// Replace null entries with the fields from the array
	if ($slides_from_five[$name] && count($slides_from_five[$name])) {
		$five_min = get_field('five_minute');
		foreach ($slides_from_five[$name] as $key => $slide_from_five) {
			$replacement_slides[$key] = $five_min[$slide_from_five]['slides'][$key];
		}
		$index = array_search('', array_keys($slides));
		array_splice($slides, $index, 1, $replacement_slides);
	}

	// display slides
	// $count = 0;
	foreach ($slides as $slide) {
		$blocks = parse_blocks($slide->post_content);

		// Search for the title of the block to use in the menus
		foreach ($blocks as $block) {
			if (count($block['innerBlocks'])) {
				$section_menu[] = search_inner_blocks($block['innerBlocks']);
			} elseif ('acf/cg-slide-title' === $block['blockName']) {
				$section_menu[] = $block['attrs']['data']['slide_title'];
			}
		}
		$rendered_block = one_render_reusable_block($slide);
		$output .= <<<EOT
      <div class="slide slide_$count">
      $rendered_block
    EOT;
		$dir = get_stylesheet_directory_uri();
		$output .= <<<EOT
      <div class="mm-group extras">
        <div class="mm-group_inner thin center">
          <h2 class="slide-title">Did you know?</h2>
          <div class="extras">
            <div class="icon"><img src="$dir/images/did-you-know.svg" alt="icon" class="did-you-know" aria-hidden="true"></div>
    EOT;
		foreach ($did_you_knows as $dyk) {
			$rendered_block = one_render_reusable_block($dyk);
			$output .= <<<EOT
            <div class="extra">
              $rendered_block
            </div>
      EOT;
		}

		$output .= <<<EOT
          </div>
          <h2 class="slide-title">Real impact</h2>
          <div class="extras">
            <div class="icon"><img src="$dir/images/real-impact.svg" alt="icon" class="real-impact" aria-hidden="true"></div>
    EOT;
		foreach ($real_impacts as $ri) {
			$rendered_block = one_render_reusable_block($ri);
			$output .= <<<EOT
              <div class="extra">
                $rendered_block
              </div>
      EOT;
		}

		$output .= <<<EOT
          </div>
        </div>
      </div>
    </div>
    EOT;
		$count++;
	}

	return array('menu' => $section_menu, 'html' => $output, 'count' => $count);
}

/**
 * Replaces double line-breaks with paragraph elements.
 *
 * A group of regex replaces used to identify text formatted with newlines and
 * replace double line-breaks with HTML paragraph tags. The remaining
 * line-breaks after conversion become <<br />> tags, unless $br is set to '0'
 * or 'false'.
 *
 * @param string $pee The text which has to be formatted.
 * @param bool $br Optional. If set, this will convert all remaining line-breaks after paragraphing. Default true.
 * @return string Text which has been converted into correct paragraph tags.
 */
function autop($pee, $br = true)
{
  $pre_tags = array();

  if (trim($pee) === '')
    return '';

  $pee = $pee . "\n"; // just to make things a little easier, pad the end

  if (strpos($pee, '<pre') !== false) {
    $pee_parts = explode('</pre>', $pee);
    $last_pee = array_pop($pee_parts);
    $pee = '';
    $i = 0;

    foreach ($pee_parts as $pee_part) {
      $start = strpos($pee_part, '<pre');

      // Malformed html?
      if ($start === false) {
        $pee .= $pee_part;
        continue;
      }

      $name = "<pre wp-pre-tag-$i></pre>";
      $pre_tags[$name] = substr($pee_part, $start) . '</pre>';

      $pee .= substr($pee_part, 0, $start) . $name;
      $i++;
    }

    $pee .= $last_pee;
  }

  $pee = preg_replace('|<br />\s*<br />|', "\n\n", $pee);
  // Space things out a little
  $allblocks = '(?:table|thead|tfoot|caption|col|colgroup|tbody|tr|td|th|div|dl|dd|dt|ul|ol|li|pre|select|option|form|map|area|blockquote|address|math|style|p|h[1-6]|hr|fieldset|noscript|samp|legend|section|article|aside|hgroup|header|footer|nav|figure|figcaption|details|menu|summary)';
  $pee = preg_replace('!(<' . $allblocks . '[^>]*>)!', "\n$1", $pee);
  $pee = preg_replace('!(</' . $allblocks . '>)!', "$1\n\n", $pee);
  $pee = str_replace(array("\r\n", "\r"), "\n", $pee); // cross-platform newlines
  if (strpos($pee, '<object') !== false) {
    $pee = preg_replace('|\s*<param([^>]*)>\s*|', "<param$1>", $pee); // no pee inside object/embed
    $pee = preg_replace('|\s*</embed>\s*|', '</embed>', $pee);
  }
  $pee = preg_replace("/\n\n+/", "\n\n", $pee); // take care of duplicates
  // make paragraphs, including one at the end
  $pees = preg_split('/\n\s*\n/', $pee, -1, PREG_SPLIT_NO_EMPTY);
  $pee = '';
  foreach ($pees as $tinkle)
    $pee .= '<p>' . trim($tinkle, "\n") . "</p>\n";
  $pee = preg_replace('|<p>\s*</p>|', '', $pee); // under certain strange conditions it could create a P of entirely whitespace
  $pee = preg_replace('!<p>([^<]+)</(div|address|form)>!', "<p>$1</p></$2>", $pee);
  $pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee); // don't pee all over a tag
  $pee = preg_replace("|<p>(<li.+?)</p>|", "$1", $pee); // problem with nested lists
  $pee = preg_replace('|<p><blockquote([^>]*)>|i', "<blockquote$1><p>", $pee);
  $pee = str_replace('</blockquote></p>', '</p></blockquote>', $pee);
  $pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)!', "$1", $pee);
  $pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee);
  if ($br) {
    $pee = preg_replace_callback('/<(script|style).*?<\/\\1>/s', create_function('$matches', 'return str_replace("\n", "<PreserveNewline />", $matches[0]);'), $pee);
    $pee = preg_replace('|(?<!<br />)\s*\n|', "<br />\n", $pee); // optionally make line breaks
    $pee = str_replace('<PreserveNewline />', "\n", $pee);
  }
  $pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*<br />!', "$1", $pee);
  $pee = preg_replace('!<br />(\s*</?(?:p|li|div|dl|dd|dt|th|pre|td|ul|ol)[^>]*>)!', '$1', $pee);
  $pee = preg_replace("|\n</p>$|", '</p>', $pee);

  if (!empty($pre_tags))
    $pee = str_replace(array_keys($pre_tags), array_values($pre_tags), $pee);

  return $pee;
}

function finder_from_list($item, $key, $options = array())
{

  if (is_array($item)) {
    echo '<dd><a href="#" class="' . slugify($key) . '"><span class="' . slugify($key) . '">' . $key . "</span></a>";
    echo '<dl>';
    array_walk($item, 'finder_from_list');
    echo '</dl>';
    echo '</dd>';
  } else {
    echo $item;
  }
}

function get_slugs($post)
{
  return $post->post_name;
}

function build_active_hospital_menu()
{
  global $post;

  $ah_post = get_page_by_path('active-hospitals');
  $ah_post_id = $ah_post->ID;

  $args = array(
    'post_parent' => $ah_post_id,
    'post_type' => 'page',
    'orderby' => 'menu_order',
    'order' => 'ASC'
  );

  $menu = array();

  $query = new WP_Query($args);
  while ($query->have_posts()) :
    $query->the_post();
    $children = array();
    $slug = $post->post_name;

    if ('template-active-hospitals-toolkit.php' === get_page_template_slug(get_the_ID())) :
      $menu[$slug] = array(
        'ID' => get_the_ID(),
        'text' => get_the_title(),
        'url' => get_permalink(),
      );

      $child_args = array(
        'post_parent' => get_the_ID(),
        'post_type' => 'page',
        'orderby' => 'menu_order'
      );

      $child_query = new WP_Query($child_args);
      if ($child_query->have_posts()) :
        while ($child_query->have_posts()) :
          $child_query->the_post();
          $children[] = array(
            'ID' => get_the_ID(),
            'text' => get_the_title(),
            'url' => get_permalink(),
          );
        endwhile;
        $menu[$slug]['children'] = $children;
      endif;

    endif;

  endwhile;
  wp_reset_query();

  set_transient('active_hospital_menu', $menu, 2 * WEEK_IN_SECONDS);

  return $menu;
}

function build_finder_array()
{
  $terms = get_terms(
    array(
      'taxonomy' => 'patient',
      // 'orderby' => 'menu_order',
      'fields' => 'names'
    )
  );
  array_unshift($terms, 'dt_title');

  return array_fill_keys($terms, null);
}

function build_evidence_finder_list()
{

  $finder = build_finder_array();

  $excluded_patients = get_field('disable_patient_types', 'options');
  $excluded_conditions = get_field('disable_conditions', 'options');

  // Get all the evidence
  $query_args = array(
    'numberposts' => -1,
    'post_type' => 'evidence',
    'meta_query' => array(
      'relation' => 'AND',
      array(
        'key' => 'is_reference',
        'value' => 0,
      ),
      array(
        'key' => 'remove_from_evidence_finder',
        'value' => 0,
      )
    ),
    'orderby' => 'title',
    'order' => 'ASC'
  );
  $evidence = get_posts($query_args);

  // For every piece of evidence we need to find out where it's been used on the site
  foreach ($evidence as $ev) {

    $conditions = get_the_terms($ev, 'condition-tax');

    if ($conditions) {
      foreach ($conditions as $condition) {

        // We're storing the condition ID in the description of the term
        if(is_array($excluded_conditions) && in_array($condition->description, $excluded_conditions)) continue;

        $ages = get_the_terms($condition->description, 'patient');
        if ($ages) {
          foreach ($ages as $age) {

            if(is_array($excluded_patients) && in_array($age->term_id, $excluded_patients)) continue;

            $finder['dt_title'] = '<dt>' . get_taxonomy('patient')->labels->singular_name . '</dt>';
            $finder[$age->name]['dt_title'] = '<dt>' . get_post_type_object('condition')->labels->singular_name . '</dt>';
            $finder[$age->name][$condition->name]['dt_title'] = '<dt>' . get_post_type_object('evidence')->labels->singular_name . '</dt>';
            $finder[$age->name][$condition->name][$ev->post_title] = '<dd><h3><a href="' . get_permalink($ev) . '">' . (get_field('display_title', $ev) ?: $ev->post_title) . '</a></h3></dd>';
          }
        }
      }
    }
  }

  set_transient('evidence_finder_list', $finder, 2 * MONTH_IN_SECONDS);

  return $finder;
}

function build_consultation_finder_list()
{

  $finder = build_finder_array();

  $excluded_patients = get_field('disable_patient_types', 'options');
  $excluded_conditions = get_field('disable_conditions', 'options');

  $query_args = array(
    'numberposts' => -1,
    'post_type' => 'condition',
    'orderby' => 'title',
    'order' => 'ASC',
    'exclude' => $excluded_conditions,
    'tax_query' => array(
      'relation' => 'AND',
      array(
        'taxonomy' => 'patient',
        'field'    => 'term_id',
        'terms'    => $excluded_patients,
        'operator' => 'NOT IN',
      ),
    ),
  );

  $conditions = get_posts($query_args);

  foreach ($conditions as $condition) {
    $ages = get_the_terms($condition->ID, 'patient');
    if ($ages) {
      foreach ($ages as $age) {
        $finder['dt_title'] = '<dt>' . get_taxonomy('patient')->labels->singular_name . '</dt>';
        $finder[$age->name]['dt_title'] = '<dt>' . get_post_type_object('condition')->labels->singular_name . '</dt>';
        $finder[$age->name][$condition->post_title] = '<dd><a href="' . get_permalink($condition) . '" data-text="' . $condition->post_title . '">' . get_svg_from_url(get_field('condition_icon', $condition->ID)['url']) . $condition->post_title . '</a></dd>';
      }
    }
  }



  // Reorder finder array (Adult, Young Person, Child)

  set_transient('consultation_finder_list', $finder, 2 * WEEK_IN_SECONDS);

  return $finder;
}

function build_patient_information_finder_list()
{

  $finder = build_finder_array();
  $excluded_patients = get_field('disable_patient_types', 'options');
  $excluded_conditions = get_field('disable_conditions', 'options');

  // Get all the patient info
  $query_args = array(
    'numberposts' => -1,
    'post_type' => 'patient_information',
    'orderby' => 'title',
    'order' => 'ASC'
  );
  $patient_info = get_posts($query_args);

  foreach ($patient_info as $pi) {

    $conditions = get_the_terms($pi, 'condition-tax');

    if ($conditions) {
      foreach ($conditions as $condition) {

        // We're storing the condition ID in the description of the term
        if(in_array($condition->description, $excluded_conditions)) continue;

        $ages = get_the_terms($condition->description, 'patient');

        if ($ages) {
          foreach ($ages as $age) {

            if(in_array($age->term_id, $excluded_patients)) continue;

            $finder['dt_title'] = '<dt>' . get_taxonomy('patient')->labels->singular_name . '</dt>';
            $finder[$age->name]['dt_title'] = '<dt>' . get_post_type_object('condition')->labels->singular_name . '</dt>';
            $finder[$age->name][$condition->name]['dt_title'] = '<dt>' . get_post_type_object('patient_information')->labels->singular_name . '</dt>';

            $guid = uniqid();
            $list_item = '';
            $blocks = parse_blocks($pi->post_content);
            foreach ($blocks as $block) :
              if ('acf/mm-patient-information' === $block['blockName']) :
                $block_data = $block['attrs']['data'];
                $icon_url = wp_get_attachment_url($block_data['icon']);
                $title = $block_data['title'] ?: get_the_title($pi);
                $description = do_shortcode(autop($block_data['description']));
                $link_text = '';
                if ('1' === $block_data['is_external']) {
                  $url = $block_data['url'];
                  $hide_link = true;
                } else {
                  $file_id = $block_data['file'];
                  $url = wp_get_attachment_url($file_id);
                  $link_text = strtoupper(wp_check_filetype($url)['ext']);
                  $modal_link_text = 'Download ' . $link_text;
                }
                $link_text = $link_text ?: 'View';
                $modal_link_text = $modal_link_text ?: 'View';
              endif;
            endforeach;

            $list_item .=  <<<EOT
              <dd>
                <div class="finder-info-cols">
            EOT;
            $list_item .= get_svg_from_url($icon_url);
            $list_item .=  <<<EOT
                  <div class="content">
                    <div class="heading"><h4>$title</h4></div>
                    <div class="description">$description</div>
                  </div>
                  <div class="links">
                    <a href="#" class="button" data-modal="qr-$guid">QR Code</a>
                    <a href="$url" target="_blank" class="button">$link_text</a>
                    <section class="modal resources-modal" data-modal-id="qr-$guid">
                    <div class="content">
                      <a href="#" data-modal-action="close">
                        Close
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"></path></svg>
                      </a>
                      <div class="inner">
                        <div class="info">
                          <h1>Scan this code with your mobile device to get the resource.</h1>
                          <p>Or click below to access it directly</p>
                          <p><a href="$url" class="button">$modal_link_text</a></p>
                        </div>
                        <div class="code">
                          <h5>$title</h5>
            EOT;

            $list_item .= do_shortcode("[phpqrcode data='$url' title='$title' width=12 link='$url' target='_blank']");
            $list_item .=  <<<EOT
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            </dd>
            EOT;

            $finder[$age->name][$condition->name][$pi->post_title] = $list_item;
          }
        }
      }
    }
  }

  set_transient('patient_information_finder_list', $finder, 2 * WEEK_IN_SECONDS);

  return $finder;
}

function build_resource_finder_list()
{

  $finder = build_finder_array();

  $query_args = array(
    'numberposts' => -1,
    'post_type' => 'resource',
    'orderby' => 'title',
    'order' => 'ASC'
  );
  $resources = get_posts($query_args);

  foreach ($resources as $resource) {

    $subjects = get_the_terms($resource->ID, 'subject');

    if ($subjects) {
      foreach ($subjects as $subject) {

        $finder['dt_title'] = '<dt>' . get_taxonomy('subject')->labels->singular_name . '</dt>';
        $finder[$subject->name]['dt_title'] = '<dt>' . get_post_type_object('resource')->labels->singular_name . '</dt>';

        $list_item = '';
        $blocks = parse_blocks($resource->post_content);
        foreach ($blocks as $block) :
          if ('acf/mm-resource' === $block['blockName']) :
            $block_data = $block['attrs']['data'];
            $icon_url = wp_get_attachment_url($block_data['icon']);
            $title = $block_data['title'] ?: get_the_title($resource);
            $description = do_shortcode(autop($block_data['description']));
            $link_text = '';
            if ('1' === $block_data['is_external']) {
              $url = $block_data['url'];
              $hide_link = true;
              $link_text = 'Visit';
            } else {
              $file_id = $block_data['file'];
              $url = wp_get_attachment_url($file_id);
              $link_text = strtoupper(wp_check_filetype($url)['ext']);
            }
          endif;
        endforeach;

        $list_item .=  <<<EOT
          <dd>
            <div class="finder-info-cols">
        EOT;
        $list_item .= get_svg_from_url($icon_url);
        $list_item .=  <<<EOT
              <div class="content">
                <div class="heading"><h4>$title</h4></div>
                <div class="description">$description</div>
              </div>
              <div class="links">
                <a href="$url" class="button">$link_text</a>
              </div>
            </div>
          </dd>
          EOT;

        $finder[$subject->name][] = $list_item;
      }
    }
  }

  set_transient('resources_finder_list', $finder, 2 * WEEK_IN_SECONDS);

  return $finder;
}

function build_activity_finder_list()
{

  $finder = build_finder_array();
  $excluded_patients = get_field('disable_patient_types', 'options');
  $excluded_conditions = get_field('disable_conditions', 'options');


  // Get all the activities
  $query_args = array(
    'numberposts' => -1,
    'post_type' => 'organisation',
    'orderby' => 'title',
    'order' => 'ASC',
  );
  $activities = get_posts($query_args);

  foreach ($activities as $act) :

    $types = get_the_terms($act, 'organisation_type');
    $conditions = get_the_terms($act, 'condition-tax');

    if ($conditions) :
      foreach ($conditions as $condition) :

        if(in_array($condition->description, $excluded_conditions)) continue;

        $ages = get_the_terms($condition->description, 'patient');

        if ($ages) :
          foreach ($ages as $age) :

            if(in_array($age->term_id, $excluded_patients)) continue;

            if ($types) :
              foreach ($types as $type) :
                // if($type->parent > 0):
                $finder['dt_title'] = '<dt>' . get_taxonomy('patient')->labels->singular_name . '</dt>';
                $finder[$age->name]['dt_title'] = '<dt>' . get_post_type_object('condition')->labels->singular_name . '</dt>';
                $finder[$age->name][$condition->name]['dt_title'] = '<dt>' . get_taxonomy('organisation_type')->labels->singular_name . '</dt>';
                $finder[$age->name][$condition->name][$type->name]['dt_title'] = '<dt>' . get_post_type_object('organisation')->labels->singular_name . '</dt>';
                $guid = uniqid();
                $list_item = '';
                $blocks = parse_blocks($act->post_content);
                foreach ($blocks as $block) :
                  if ('acf/mm-organisation' === $block['blockName']) :
                    $block_data = $block['attrs']['data'];
                    $icon_url = wp_get_attachment_url($block_data['icon']);
                    $title = $block_data['title'] ?: get_the_title($act);
                    $description = do_shortcode(autop($block_data['description']));
                    $link_text = '';
                    $url = $block_data['url'];
                    $link_text = 'Visit';
                    $button = '';
                    if ($url) {
                      $button .= "\t<div class='links'>\n";
                      $button .= "\t\t<a href='" . add_scheme($url) . "' class='button'>$link_text</a>\n";
                      $button .= "\t</div>\n";
                    }
                  endif;
                endforeach;

                $list_item .=  <<<EOT
                    <dd>
                      <div class="finder-info-cols">
                  EOT;
                $list_item .=  <<<EOT
                        <div class="image" style="background-image:url('$icon_url')"></div>
                        <div class="content">
                          <div class="heading"><h4>$title</h4></div>
                          <div class="description">$description</div>
                        </div>
                        $button
                      </div>
                    </dd>
                  EOT;
                $finder[$age->name][$condition->name][$type->name][$act->post_title] = $list_item;
              // endif;
              endforeach;
            endif;
          endforeach;
        endif;
      endforeach;
    endif;
  endforeach;

  set_transient('activity_finder_list', $finder, 2 * WEEK_IN_SECONDS);

  return $finder;
}

function build_condition_radios_for_patient($patient, $active, $force = false)
{

  $patient_slug = $patient->slug;
  $active = $active ? 'active' : '';

  if (!$force && false !== ($content = get_transient($patient_slug . '_condition_radios')))
    return $content;

  $excluded_conditions = get_field('disable_conditions', 'options');

  $query_args = array(
    'numberposts' => -1,
    'post_type' => 'condition',
    'orderby' => 'title',
    'order' => 'ASC',
    'exclude' => $excluded_conditions,
    'tax_query' => array(
      array(
        'taxonomy' => 'patient',
        'field' => 'term_id',
        'terms' => $patient->term_id,
        'include_children' => false
      )
    )
  );

  $conditions = get_posts($query_args);

  if ($conditions) :

    $content .=  "<div class='values $active' data-patient='$patient_slug'>\n";
    $content .=  "\t<ul>\n";
    $checked = $active ? 'checked' : '';

    foreach ($conditions as $condition) :

      $slug = $condition->post_name;
      $name = $condition->post_title;

      $content .= "\t<li>\n";
      $content .= "\t\t<input type='radio' name='$patient_slug' id='$slug' value='$slug' $checked/>\n";
      $content .= "\t\t<label for='$slug'>$name</label>\n";
      $content .= "\t</li>\n";
      $checked = '';

    endforeach;

    $content .=  "\t</ul>\n";
    $content .=  "</div>\n";

  endif;

  set_transient($patient_slug . '_condition_radios', $content, 2 * WEEK_IN_SECONDS);
  return $content;
}

function build_nav_finder_patient_radios($force = false)
{

  if (!$force && false !== ($content = get_transient('patient_radios')))
    return $content;


  $excluded_patients = get_field('disable_patient_types', 'options');

  $patients = get_terms(array(
    'taxonomy' => 'patient',
    'exclude' => $excluded_patients
  ));

  if ($patients) :

    $content .= "<ul>\n";
    $checked = 'checked ';

    foreach ($patients as $patient) :

      $slug = $patient->slug;
      $name = $patient->name;

      $content .= "\t<li>\n";
      $content .= "\t\t<input type='radio' name='patients' id='$slug' value='$slug' $checked/>\n";
      $content .= "\t\t<label for='$slug'>$name</label>\n";
      $content .= "\t</li>\n";
      $checked = '';

    endforeach;

    $content .= "</ul>\n";

  endif;

  set_transient('patient_radios', $content, 2 * WEEK_IN_SECONDS);

  return $content;
}

function build_nav_finder_conditions_radios($force)
{

  $excluded = get_field('disable_patient_types', 'options');

  $patients = get_terms(array(
    'taxonomy' => 'patient',
    'exclude' => $excluded
  ));

  if ($patients) :

    $active = true;
    $content = '';

    foreach ($patients as $patient) :

      $content .= build_condition_radios_for_patient($patient, $active, $force);
      $active = false;

    endforeach;

  endif;

  return $content;
}

function build_nav_consultation_finder($force = false)
{

  if (!$force && false !== ($content = get_transient('nav_consultation_finder')))
    return $content;

  $patient_radios = build_nav_finder_patient_radios($force);
  $condition_radios = build_nav_finder_conditions_radios($force);
  $action = esc_url(admin_url('admin-post.php'));

  $content = <<<EOT
  <form class="nav-consultation-finder" action="$action" method="post">
    <h4>Find the right consultation</h4>
    <div class="options ages">
      <div class="labels">
        <p>1. Age</p>
      </div>
      <div class="values">
        $patient_radios
      </div>
    </div>
    <div class="options conditions">
      <div class="labels">
        <p>2. Condition</p>
      </div>
      $condition_radios
    </div>
    <div class="button-container">
      <input type="hidden" name="action" value="nav_consultation_finder">
      <button class="button" href="#" type="submit">
        Find
      </button>
    </div>
  </form>
  EOT;

  set_transient('nav_consultation_finder', $content, 2 * WEEK_IN_SECONDS);

  return $content;
}

function get_social_share_buttons($post_id = null)
{
  echo '<div class="logos">';
  $socials = array('twitter' => 'http://twitter.com/share?text={{twitter_text}}&hashtags={{twitter_hashtags}}&url=', 'facebook' => 'https://www.facebook.com/sharer/sharer.php?u=');
  foreach ($socials as $social => $share_url) :

    $hashtags = array();
    if ('twitter' === $social && have_rows('twitter_share_hashtags', $post_id)) {
      while (have_rows('twitter_share_hashtags', $post_id)) {
        the_row();
        $hashtags[] = get_sub_field('hashtag');
      }
    }
    $share_url = str_replace('{{twitter_hashtags}}', implode(',', $hashtags), $share_url);
    $share_url = str_replace('{{twitter_text}}', (get_field('twitter_share_text', $post_id) ?: ''), $share_url);
    $url = $share_url . get_permalink();
  ?>
    <a href="<?= $url; ?>" target="_blank" class="logo">
      <?php get_svg($social . '-logo'); ?>
    </a>
    <?php
  endforeach;
  echo '</div>';
}

function get_social_logos()
{
  if (have_rows('social_accounts', 'option')) :
    echo '<div class="logos">';
    while (have_rows('social_accounts', 'option')) :
      the_row();
      $type = get_sub_field('type');
      $url = get_sub_field('profile_url');
    ?>
      <a href="<?= $url; ?>" target="_blank" class="logo">
        <?php if ('other' === $type['value']) : ?>
          <?php if (get_sub_field('icon') && 'svg' === wp_check_filetype(get_sub_field('icon')['url'])['ext']) : ?>
            <?php echo get_svg_from_url(get_sub_field('icon')['url']); ?>
          <?php else : ?>
            <?php wp_get_attachment_image(get_sub_field('icon')); ?>
          <?php endif; ?>
        <?php else : ?>
          <?php get_svg($type['value'] . '-logo'); ?>
        <?php endif; ?>
      </a>
    <?php
    endwhile;
    echo '</div>';
  endif;
}

function get_footer_logos($type)
{
  global $post;
  $field_name = $type . '_logos';
  $post_id = $post->ID;
  $title = $post->post_title;
  $custom_title = get_field($type . '_title');

  // If not top level page, get top level logos
  if (is_page()) {
    if ($post->post_parent) {
      $ancestors = get_post_ancestors($post->ID);
      $root = count($ancestors) - 1;
      $post_id = $ancestors[$root];
      $title = get_the_title($post_id);
      $custom_title = get_field($type . '_title', $post_id);
    }
  }

  if (have_rows($field_name, $post_id)) :
    ?>
    <div class="logo-section">
      <?php if ($custom_title) : ?>
        <p><?= $custom_title; ?></p>
      <?php else : ?>
        <p><strong><?= $title; ?></strong> is <?= $type; ?> by</p>
      <?php endif ?>
      <div class="logos">
        <?php
        while (have_rows($field_name, $post_id)) :
          the_row();
          $image = get_sub_field('logo');
          $url = get_sub_field('link');
        ?>
          <a class="logo" href="<?= $url ? $url['url'] : '#'; ?>" target="_blank">
            <img src="<?= $image['sizes']['medium']; ?>" alt="<?= $image['alt']; ?>" title="<?= $image['title']; ?>" />
          </a>
        <?php
        endwhile;
        ?>
      </div>
    </div>
<?php
  endif;
}

function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function add_scheme($url, $scheme = 'http://')
{
  return parse_url($url, PHP_URL_SCHEME) === null ?
    $scheme . $url : $url;
}

function get_video($video, $params = array(), $guid = false)
{

  // Add autoplay functionality to the video code
  if (preg_match('/src="(.+?)"/', $video, $matches)) {
    // Video source URL
    $src = $matches[1];
    $slash_pos = strrpos($src, '/') + 1;
    $query_vars_pos = strrpos($src, '?') - 1;

    // Add the oaylist id if the vid loops (YuTube)
    if (1 === $params['loop']) {
      $video_id = substr($src, $slash_pos, $query_vars_pos - $slash_pos);
      $params['playlist'] = $video_id;
    }

    $params['enablejsapi'] = $params['enablejsapi'] === 0 ? 0 : 1;
    $params['rel'] = 0;

    $new_src = add_query_arg($params, $src);

    $video = str_replace($src, $new_src, $video);

    $attributes = array();

    // add extra attributes to iframe html
    if ($guid) {
      $attributes['data-video-id'] = "mm-video-{$guid}";
      $attributes['id'] = "mm-video-{$guid}";
    }
    $attributes['frameborder'] = '0';
    $attributes['tabindex'] = '-1';

    $title = $params['title'] ?: get_bloginfo('name') . ' video';

    $video = str_replace('></iframe>', ' title="' . $title . '" ' . http_build_query($attributes, '', ' ') . '></iframe>', $video);
  }

  // If we're muted and looped then we can assume we want an overlay (is this true?)
  $overlay = 1 === $params['loop'] && 1 === $params['mute'] ? '<div class="cover-video"></div>' : '';

  return '<div class="mm-video">' . $video . $overlay . '</div>';
}

function the_video($video, $params = array(), $guid = false)
{
  echo get_video($video, $params, $guid);
}
