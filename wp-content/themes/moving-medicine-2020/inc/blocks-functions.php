<?php

// hide unused blocks
add_filter( 'allowed_block_types', 'one_allowed_block_types' );
function one_allowed_block_types( $allowed_blocks ) {
  return array(
    'core/image',
    'core/paragraph',
    'core/heading',
    'core/list',
    'core/quote',
    'core/pullquote',
    'core/table',
    'core/spacer',
    'core/group',
    'core/shortcode',
    'acf/mm-organisation',
    'acf/mm-resource',
    'acf/mm-patient-information',
    'acf/mm-billboard',
    'acf/mm-accordian',
    'acf/mm-list',
    'acf/mm-pathway',
    'acf/mm-tiles',
    'acf/mm-split-section',
    'acf/mm-quote',
    'acf/mm-quiz',
    'acf/mm-video',
    'acf/mm-insta'
  );
}

function one_render_reusable_block( $block_id = '' ) {
  if(is_a($block_id, 'WP_Post')) {
    $block_id = $block_id->ID;
  }
  elseif ( empty( $block_id ) || (int) $block_id !== $block_id ) {
      return;
  }
	$content = get_post_field( 'post_content', $block_id );
  return apply_filters( 'the_content', $content );
}


// Add block categories
function movingmedicine2020_block_category($categories, $post)
{
  return array_merge(
    $categories,
    array(
      array(
        'slug' => 'mm-blocks',
        'title' => __('MM Blocks', 'mm-blocks'),
      ),
    )
  );
}
add_filter('block_categories', 'movingmedicine2020_block_category', 10, 2);


// ACF blocks
function mm2020_register_blocks()
{

  if (!function_exists('acf_register_block_type'))
    return;

  acf_register_block_type(array(
    'name'      => 'cg-slide-title',
    'title'     => __('Condition Slide Title'),
    'category'    => 'mm-blocks',
    'icon'      => '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="heading" class="svg-inline--fa fa-heading fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M448 96v320h32a16 16 0 0 1 16 16v32a16 16 0 0 1-16 16H320a16 16 0 0 1-16-16v-32a16 16 0 0 1 16-16h32V288H160v128h32a16 16 0 0 1 16 16v32a16 16 0 0 1-16 16H32a16 16 0 0 1-16-16v-32a16 16 0 0 1 16-16h32V96H32a16 16 0 0 1-16-16V48a16 16 0 0 1 16-16h160a16 16 0 0 1 16 16v32a16 16 0 0 1-16 16h-32v128h192V96h-32a16 16 0 0 1-16-16V48a16 16 0 0 1 16-16h160a16 16 0 0 1 16 16v32a16 16 0 0 1-16 16z"></path></svg>',
    'mode'      => 'auto',
    'keywords'    => array(),
    'post_types' => array('condition_slide'),
    'render_callback' => 'mm_slide_title_block_render_callback'
  ));
  
  acf_register_block_type(array(
    'name'      => 'cg-tool-selector',
    'title'     => __('Consultation Guide Tool Selector'),
    'category'    => 'mm-blocks',
    'icon'      => '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="hammer" class="svg-inline--fa fa-hammer fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M571.31 193.94l-22.63-22.63c-6.25-6.25-16.38-6.25-22.63 0l-11.31 11.31-28.9-28.9c5.63-21.31.36-44.9-16.35-61.61l-45.25-45.25c-62.48-62.48-163.79-62.48-226.28 0l90.51 45.25v18.75c0 16.97 6.74 33.25 18.75 45.25l49.14 49.14c16.71 16.71 40.3 21.98 61.61 16.35l28.9 28.9-11.31 11.31c-6.25 6.25-6.25 16.38 0 22.63l22.63 22.63c6.25 6.25 16.38 6.25 22.63 0l90.51-90.51c6.23-6.24 6.23-16.37-.02-22.62zm-286.72-15.2c-3.7-3.7-6.84-7.79-9.85-11.95L19.64 404.96c-25.57 23.88-26.26 64.19-1.53 88.93s65.05 24.05 88.93-1.53l238.13-255.07c-3.96-2.91-7.9-5.87-11.44-9.41l-49.14-49.14z"></path></svg>',
    'mode'      => 'auto',
    'keywords'    => array(),
    'post_types' => array('condition_slide'),
    'render_callback' => 'mm_tool_selector_block_render_callback'
	));
	
	acf_register_block_type(array(
    'name'      => 'mm-organisation',
    'title'     => __('Organisation'),
    'render_template' => 'inc/blocks/organisation.php',
    'category'    => 'formatting',
    'icon'      => '<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="building" class="svg-inline--fa fa-building fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M128 148v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12h-40c-6.6 0-12-5.4-12-12zm140 12h40c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12zm-128 96h40c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12zm128 0h40c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12zm-76 84v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm76 12h40c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12zm180 124v36H0v-36c0-6.6 5.4-12 12-12h19.5V24c0-13.3 10.7-24 24-24h337c13.3 0 24 10.7 24 24v440H436c6.6 0 12 5.4 12 12zM79.5 463H192v-67c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v67h112.5V49L80 48l-.5 415z"></path></svg>',
    'mode'      => 'auto',
    'keywords'    => array(),
    'post_types' => array('organisation'),
  ));
  
  acf_register_block_type(array(
    'name'      => 'mm-resource',
    'title'     => __('Resource'),
    'render_template' => 'inc/blocks/resource.php',
    'category'    => 'formatting',
    'icon'      => '<svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="file-alt" class="svg-inline--fa fa-file-alt fa-w-12" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M369.9 97.9L286 14C277 5 264.8-.1 252.1-.1H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V131.9c0-12.7-5.1-25-14.1-34zm-22.6 22.7c2.1 2.1 3.5 4.6 4.2 7.4H256V32.5c2.8.7 5.3 2.1 7.4 4.2l83.9 83.9zM336 480H48c-8.8 0-16-7.2-16-16V48c0-8.8 7.2-16 16-16h176v104c0 13.3 10.7 24 24 24h104v304c0 8.8-7.2 16-16 16zm-48-244v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm0 64v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm0 64v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12z"></path></svg>',
    'mode'      => 'auto',
    'keywords'    => array(),
    'post_types' => array('resource'),
  ));
  
  acf_register_block_type(array(
    'name'      => 'mm-patient-information',
    'title'     => __('Patient Information'),
    'render_template' => 'inc/blocks/resource.php',
    'category'    => 'formatting',
    'icon'      => '<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="info-circle" class="svg-inline--fa fa-info-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm0-338c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z"></path></svg>',
    'mode'      => 'auto',
    'keywords'    => array(),
    'post_types' => array('patient_information'),
    // 'render_callback' => 'slide_title_block_render_callback'
  ));
  
  acf_register_block_type(array(
    'name'      => 'mm-billboard',
    'title'     => __('Billboard'),
    'render_template' => 'inc/blocks/billboard.php',
    'category'    => 'common',
    'icon'      => 'video-alt3',
    'mode'      => 'auto',
    'keywords'    => array('billboard', 'video', 'hero'),
    'post_types' => array(),
    // 'render_callback' => 'slide_title_block_render_callback'
  ));
  
  acf_register_block_type(array(
    'name'      => 'mm-accordian',
    'title'     => __('Accordion'),
    'render_template' => 'inc/blocks/accordion.php',
    'category'    => 'formatting',
    'icon'      => '<svg style="display: block; transform: scale(-1,1)" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="th-list" class="svg-inline--fa fa-th-list fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><g class="fa-group"><path class="fa-secondary" fill="currentColor" d="M488 352H205.33a24 24 0 0 0-24 24v80a24 24 0 0 0 24 24H488a24 24 0 0 0 24-24v-80a24 24 0 0 0-24-24zm0-320H205.33a24 24 0 0 0-24 24v80a24 24 0 0 0 24 24H488a24 24 0 0 0 24-24V56a24 24 0 0 0-24-24zm0 160H205.33a24 24 0 0 0-24 24v80a24 24 0 0 0 24 24H488a24 24 0 0 0 24-24v-80a24 24 0 0 0-24-24z" opacity="0.4"></path><path class="fa-primary" fill="currentColor" d="M125.33 192H24a24 24 0 0 0-24 24v80a24 24 0 0 0 24 24h101.33a24 24 0 0 0 24-24v-80a24 24 0 0 0-24-24zm0-160H24A24 24 0 0 0 0 56v80a24 24 0 0 0 24 24h101.33a24 24 0 0 0 24-24V56a24 24 0 0 0-24-24zm0 320H24a24 24 0 0 0-24 24v80a24 24 0 0 0 24 24h101.33a24 24 0 0 0 24-24v-80a24 24 0 0 0-24-24z"></path></g></svg>',
    'mode'      => 'auto',
    'keywords'    => array('accordion', 'list'),
    'post_types' => array(),
    // 'render_callback' => 'slide_title_block_render_callback'
  ));
  
  acf_register_block_type(array(
    'name'      => 'mm-list',
    'title'     => __('List'),
    'render_template' => 'inc/blocks/list-block.php',
    'category'    => 'formatting',
    'icon'      => '<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="list" class="svg-inline--fa fa-list fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M80 48H16A16 16 0 0 0 0 64v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16V64a16 16 0 0 0-16-16zm0 160H16a16 16 0 0 0-16 16v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16v-64a16 16 0 0 0-16-16zm0 160H16a16 16 0 0 0-16 16v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16v-64a16 16 0 0 0-16-16zm416-136H176a16 16 0 0 0-16 16v16a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16v-16a16 16 0 0 0-16-16zm0 160H176a16 16 0 0 0-16 16v16a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16v-16a16 16 0 0 0-16-16zm0-320H176a16 16 0 0 0-16 16v16a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16V88a16 16 0 0 0-16-16z"></path></svg>',
    'mode'      => 'auto',
    'keywords'    => array('accordion', 'list'),
    'post_types' => array(),
  ));
  
  acf_register_block_type(array(
    'name'      => 'mm-pathway',
    'title'     => __('Pathway'),
    'render_template' => 'inc/blocks/pathway.php',
    'category'    => 'formatting',
    'icon'      => '<svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="sitemap" class="svg-inline--fa fa-sitemap fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M608 352h-32v-97.59c0-16.77-13.62-30.41-30.41-30.41H336v-64h48c17.67 0 32-14.33 32-32V32c0-17.67-14.33-32-32-32H256c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h48v64H94.41C77.62 224 64 237.64 64 254.41V352H32c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h96c17.67 0 32-14.33 32-32v-96c0-17.67-14.33-32-32-32H96v-96h208v96h-32c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h96c17.67 0 32-14.33 32-32v-96c0-17.67-14.33-32-32-32h-32v-96h208v96h-32c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h96c17.67 0 32-14.33 32-32v-96c0-17.67-14.33-32-32-32zm-480 32v96H32v-96h96zm240 0v96h-96v-96h96zM256 128V32h128v96H256zm352 352h-96v-96h96v96z"></path></svg>',
    'mode'      => 'auto',
    'keywords'    => array('pathway', 'list'),
    'post_types' => array(),
    // 'render_callback' => 'slide_title_block_render_callback'
  ));
  
  acf_register_block_type(array(
    'name'      => 'mm-tiles',
    'title'     => __('Tiles'),
    'render_template' => 'inc/blocks/tiles.php',
    'category'    => 'formatting',
    'icon'      => '<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="stop" class="svg-inline--fa fa-stop fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-6 400H54c-3.3 0-6-2.7-6-6V86c0-3.3 2.7-6 6-6h340c3.3 0 6 2.7 6 6v340c0 3.3-2.7 6-6 6z"></path></svg>',
    'mode'      => 'auto',
    'keywords'    => array('tiles', 'list'),
    'post_types' => array(),
  ));
  
  acf_register_block_type(array(
    'name'      => 'mm-split-section',
    'title'     => __('Split Section'),
    'render_template' => 'inc/blocks/split-section.php',
    'category'    => 'formatting',
    'icon'      => '<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="columns" class="svg-inline--fa fa-columns fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M464 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V80c0-26.51-21.49-48-48-48zM232 432H54a6 6 0 0 1-6-6V112h184v320zm226 0H280V112h184v314a6 6 0 0 1-6 6z"></path></svg>',
    'mode'      => 'auto',
    'keywords'    => array('tiles', 'split', '50/50', '75/25', 'splitter'),
    'post_types' => array(),
    // 'render_callback' => 'slide_title_block_render_callback'
  ));
  
  acf_register_block_type(array(
    'name'      => 'mm-quote',
    'title'     => __('Quotes / Testimonials'),
    'render_template' => 'inc/blocks/quote.php',
    'category'    => 'common',
    'icon'      => '<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="comment-lines" class="svg-inline--fa fa-comment-lines fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M368 168H144c-8.8 0-16 7.2-16 16v16c0 8.8 7.2 16 16 16h224c8.8 0 16-7.2 16-16v-16c0-8.8-7.2-16-16-16zm-96 96H144c-8.8 0-16 7.2-16 16v16c0 8.8 7.2 16 16 16h128c8.8 0 16-7.2 16-16v-16c0-8.8-7.2-16-16-16zM256 32C114.6 32 0 125.1 0 240c0 47.6 19.9 91.2 52.9 126.3C38 405.7 7 439.1 6.5 439.5c-6.6 7-8.4 17.2-4.6 26S14.4 480 24 480c61.5 0 110-25.7 139.1-46.3C192 442.8 223.2 448 256 448c141.4 0 256-93.1 256-208S397.4 32 256 32zm0 368c-26.7 0-53.1-4.1-78.4-12.1l-22.7-7.2-19.5 13.8c-14.3 10.1-33.9 21.4-57.5 29 7.3-12.1 14.4-25.7 19.9-40.2l10.6-28.1-20.6-21.8C69.7 314.1 48 282.2 48 240c0-88.2 93.3-160 208-160s208 71.8 208 160-93.3 160-208 160z"></path></svg>',
    'mode'      => 'auto',
    'keywords'    => array('quote', 'quotes', 'testimonial'),
    'post_types' => array(),
    // 'render_callback' => 'slide_title_block_render_callback'
  ));

  acf_register_block_type(array(
    'name'      => 'mm-quiz',
    'title'     => __('Quiz'),
    'render_template' => 'inc/blocks/quiz.php',
    'category'    => 'formatting',
    'icon'      => '<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="question-square" class="svg-inline--fa fa-question-square fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M448 80v352c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h352c26.51 0 48 21.49 48 48zm-48 346V86a6 6 0 0 0-6-6H54a6 6 0 0 0-6 6v340a6 6 0 0 0 6 6h340a6 6 0 0 0 6-6zm-68.756-225.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C152.846 131.491 182.94 112 229.794 112c49.071 0 101.45 38.304 101.45 88.8zM266 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z"></path></svg>',
    'mode'      => 'auto',
    'keywords'    => array('quiz', 'questions'),
    'post_types' => array(),
    // 'render_callback' => 'slide_title_block_render_callback'
  ));

  acf_register_block_type(array(
    'name'      => 'mm-video',
    'title'     => __('MM Video'),
    'render_template' => 'inc/blocks/video.php',
    'category'    => 'common',
    'icon'      => 'video-alt2',
    'mode'      => 'auto',
    'keywords'    => array('video', 'embed'),
    'post_types' => array(),
    // 'render_callback' => 'slide_title_block_render_callback'
  ));

  acf_register_block_type(array(
    'name'      => 'mm-insta',
    'title'     => __('MM Instagram'),
    'render_template' => 'inc/blocks/insta.php',
    'category'    => 'formatting',
    'icon'      => '<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" class="svg-inline--fa fa-instagram fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path></svg>',
    'mode'      => 'auto',
    'keywords'    => array('instagram, photos, social, social-media'),
    'post_types' => array(),
    // 'render_callback' => 'slide_title_block_render_callback'
  ));
}
add_action('acf/init', 'mm2020_register_blocks');


function mm_slide_title_block_render_callback($block, $content = '', $is_preview = false, $post_id = 0)
{
  $text = get_field('slide_title') ?: 'Select the slide type…';
  $unselected = get_field('slide_title') ? '' : 'style="opacity: 0.62" ';
  if($is_preview):
?>
  <h1 <?php echo $unselected ?>class="slide-title"><?php echo $text ?></h1>
<?php
  elseif('' === $unselected): ?>
    <h1 class="slide-title"><?php echo $text ?></h1>
  <?php endif;
}

function mm_tool_selector_block_render_callback($block, $content = '', $is_preview = false, $post_id = 0)
{
  $text = '[Optional] Click here to select a tool…';
  $unselected = get_field('consultation_guide_tool') || get_field('freeform') ? '' : 'style="opacity: 0.62" ';
  if('' !== $unselected && $is_preview):
?>
  <h3 <?php echo $unselected ?>class="tool-note"><?php echo $text ?></h3>
<?php
  else:
    one_get_content('blocks', 'cg-tool-selector');
  endif;
}

function movingmedicine2020_block_templates($args, $post_type)
{
  // Only add template to 'post' post type
  // Change for your post type: eg 'page', 'event', 'product'
  if ('condition_slide' === $post_type) {

    // $args['template_lock'] = 'all';

    // Set the template
    $args['template'] = [
      [ 'mm/cg-intro-group', [] ],
      [ 'acf/cg-tool-selector', [] ],
      [ 'mm/cg-insight-group', [] ],
    ];
  }
  elseif ('organisation' === $post_type) {

    $args['template_lock'] = 'all';

    // Set the template
    $args['template'] = [
      [ 'acf/mm-organisation', [] ],
    ];
  }
  elseif ('resource' === $post_type) {

    $args['template_lock'] = 'all';

    // Set the template
    $args['template'] = [
      [ 'acf/mm-resource', [] ],
    ];
  }
  elseif ('patient_information' === $post_type) {

    $args['template_lock'] = 'all';

    // Set the template
    $args['template'] = [
      [ 'acf/mm-patient-information', [] ],
    ];
  }

  return $args;
}
// Hook function into post type arguments filter
add_filter('register_post_type_args', 'movingmedicine2020_block_templates', 20, 2);


function search_inner_blocks($blockGroup) {
  foreach($blockGroup as $block) {
    if(count($block['innerBlocks'])) {
      // $title[] = search_inner_blocks($block['innerBlocks']);
    }
    elseif('acf/cg-slide-title' === $block['blockName']) {
      return $block['attrs']['data']['slide_title'];
    }
  }
  // return $title;
}