(function($) {

  var topNavLinks,
    submenus;

  function init() {
    topNavLinks = $('#menu-top-navigation .menu-item > a');
    if(!topNavLinks) return false;

    submenus = $('#menu-top-navigation .submenu');

    topNavLinks.click(show_submenu);
    topNavLinks.on('focusin', focus_submenu);
    topNavLinks.on('focusout', hide_submenu);
    submenus.on('focusout', hide_submenu);
    $(document).click(hide_submenu);
  }

  function show_submenu(event) {
    var submenu = $(this).closest('.menu-item').find('.submenu');
    if(submenu.length) {
      submenu.addClass('active');
      return false;
    }
  }
  
  function focus_submenu(event) {
    $(event.target).closest('.menu-item').find('.submenu').addClass('active');
  }

  function hide_submenu(event) {
    if(!event.relatedTarget ||
      event.relatedTarget !== null && !$(event.relatedTarget).closest('.top-menu').length ||
      (!$(event.relatedTarget).closest('.submenu').length && !$(event.relatedTarget).find('.submenu').length )) {
      submenus.removeClass('active');
    }
  }

  $(init);
})(jQuery);
