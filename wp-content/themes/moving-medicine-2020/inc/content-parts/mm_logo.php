<?php 

$args["blue"] = "00bfd7";
$args["green"] = "72d44a";

if('condition' === get_post_type() || is_consultation_guides_page()):
  $args["blue"] = "ff5c7e";
  $args["green"] = "ffc200";
elseif(is_active_hospitals_page()):
  $args["blue"] = "6858e9";
  $args["green"] = "6acbe0";
endif;

one_get_content('content-parts', 'mm-logo-svg', $args);