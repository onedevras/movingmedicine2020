<?php if( have_rows( 'questions' ) ): ?>

<!-- Quote Section -->
<section class="section full-width quiz-section <?php the_field('background') ?: 'white' ?>">
  <div class="inner container full">
    <!-- <div class="container thin"> -->
      <div class="quiz">

        <div class="tns-controls" id="custom-controls" aria-label="Use arrow keys to navigate" tabindex="0">
          <button data-controls="prev" aria-label="Previous" tabindex="-1" aria-controls="main" disabled>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"></path></svg>
          </button>
          <button data-controls="next" aria-label="Next" tabindex="-1" aria-controls="main" disabled>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z"></path></svg>
          </button>
        </div>

        <?php //if( get_field('title') ): ?>
        <div class="title">
          <h4><?php echo get_field('title') ?: 'Getting started quiz'; ?></h4>
        </div>
        <?php //endif; ?>

        <div class="questions">
        
          <div class="intro">
            <div class="question-inner">
              <div class="image svg"><?php echo get_svg_from_url( get_field('intro_image')['url'] ) ?></div>
              <div class="content" data-title="<?php echo get_field('title') ?: 'Getting started quiz'; ?>">
                <?php the_field('intro_text'); ?>
                <div class="link">
                  <a href="#" class="button continue">Start</a>
                </div>
              </div>
            </div>
          </div>

          <?php 
          $fields = get_field_object('questions');
          $count = (count($fields['value']));
          $index = 1;
          ?>
          <?php while( have_rows( 'questions' ) ): the_row(); ?>

          <div class="question">
            <div class="question-inner">
              <div class="image svg"><?php echo get_svg_from_url( get_sub_field('image')['url'] ) ?></div>
              <div class="content" data-title="<?php echo 'Question ' . $index++ . ' of ' . $count; ?>">
                <?php the_sub_field('question'); ?>
                <div class="buttons">
                  <a href="#" class="button" data-answer="answer-1"><?php echo get_sub_field('answer_1_button_text') ?: 'Yes' ?></a>
                  <a href="#" class="button" data-answer="answer-2"><?php echo get_sub_field('answer_2_button_text') ?: 'No' ?></a>
                </div>
              </div>
              <div class="answer answer-1">
                <?php the_sub_field('answer_1'); ?>
                <div class="buttons">
                  <a href="#" class="button continue">Continue</a>
                </div>
              </div>
              <div class="answer answer-2">
                <?php the_sub_field('answer_2'); ?>
                <div class="buttons">
                  <a href="#" class="button continue">Continue</a>
                </div>
              </div>
            </div>
          </div>

          <?php endwhile; ?>

          

          <div class="outro">
            <div class="question-inner">
              <div class="content" data-title="<?php echo get_field('outro_title') ?: 'Quiz complete'; ?>">
                <?php the_field('outro_text'); ?>
                <!-- <div class="link"> -->
                  <!-- <a href="<?php the_sub_field('url') ?>" class="button"><?php echo get_sub_field('link_text') ?: 'View' ?></a> -->
                <!-- </div> -->
              </div>
            </div>
          </div>

        </div>

      </div>
    <!-- </div> -->
  </div>
</section>
		
<?php endif ?>