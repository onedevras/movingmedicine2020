<?php

namespace app\wisdmlabs\edwiserBridge\BulkPurchase;

/**
 * The file that defines the woocommerce refund functionality.
 *
 * @since 2.0.1
 * @author     WisdmLabs, India <support@wisdmlabs.com>
 */

if (!class_exists("EbBpRefundManager")) {
    class EbBpRefundManager
    {

        public function __construct()
        {
        }


        /**
         * if the cohort from any order is deleted then the same cohort ids present in the different order id are removed in this function.
         * @param  [type] $orderId [description]
         */
        public function checkIfCohortExists($orderId, $cohorts)
        {
            global $wpdb;
            $newCohortArray = array();
            $tableName = $wpdb->prefix."bp_cohort_info";
            if (is_array($cohorts)) {
                foreach ($cohorts as $cohortId => $quantity) {
                    $query = $wpdb->prepare("SELECT * FROM $tableName WHERE MDL_COHORT_ID = %d", $cohortId);
                    $result = $wpdb->get_row($query);
                    if ($result) {
                        $newCohortArray[$cohortId] = $quantity;
                    }
                }
            }
            update_post_meta($orderId, "eb_bp_mdl_cohort_id", $newCohortArray);
            return $newCohortArray;
        }



        /**
         * dropdown, input box and the checkbox on woocommerce order refund added from this function
         * @return [type]        [description]
         */
        public function refundHtmlContent($order)
        {
            global $wpdb;
            $orderId = $order->get_id();
            $cohort = get_post_meta($orderId, "eb_bp_mdl_cohort_id", 1);
            $cohort = $this->checkIfCohortExists($orderId, $cohort);

            if ($cohort) {
                ob_start();
                ?>
                <div class="bp-refund-wrapper">
                    <div class="bp-refund-heading"><?php _e("Bulk Purchase Refund", EBBP_TD); ?></div>
                    <table class="wc-order-totals">
                        <tr title="<?php _e('You cannot rollback this action!', EBBP_TD); ?>">
                            <td class="label">
                                <label><?php _e('Refund Method:', EBBP_TD); ?></label>
                            </td>
                            <td class="total">
                                <select class="bp-refund-inp-fields" name="bp-refund-type">
                                    <option value="">Select Refund Type</option>
                                    <option value="bp-partial-refund"> Partial Refund</option>
                                    <option value="bp-full-refund">Full Refund</option>
                                </select>
                                <div class="clear"></div>
                            </td>
                        </tr>

                <?php

                foreach ($cohort as $cohortId => $quantity) {
                    $tableName = $wpdb->prefix."bp_cohort_info";
                    $query = $wpdb->prepare("SELECT * FROM $tableName WHERE MDL_COHORT_ID = %d", $cohortId);
                    $result = $wpdb->get_row($query);

                    foreach (maybe_unserialize($result->PRODUCTS) as $key => $value) {
                        $key = $key;
                        $availableQuantity = $value;
                    }
                    $avaialeRefundQuantity = $availableQuantity > $quantity ? $quantity : $availableQuantity;

                    if ($result) {
                        $cohortName = !empty($result->NAME) ? $result->NAME : $result->COHORT_NAME;
                        ?>
                        <tr class="bp-refund-qty-wrapper">
                            <td class="label">
                                <label><?= __('Quantity to be refunded for ', EBBP_TD) . $cohortName . " : "; ?></label>
                            </td>
                            <td class="total">
                                <div class="eb-tooltip">
                                    <input data-availqty= "<?= $avaialeRefundQuantity ?>" step="1" type="number" name="bp-partial-refund-quantity_<?= $cohortId ?>" min="0" max="<?= $avaialeRefundQuantity ?>" class="bp-refund-inp-fields bp-partial-refund-fields">
                                    <span class="eb-tooltiptext"> <?= __("Refund quantity should be less than or equal to the available quantity.") ?></span>
                                </div>
                                <span style="padding-left: 10px; font-weight: 600">
                                    /
                                    <span style="padding-left: 5px">
                                        <?= $avaialeRefundQuantity ?>
                                    </span>
                                </span>
                                <div class="clear"></div>
                            </td>
                        </tr>

                        <?php
                    }
                }
                ?>
                        <tr class="bp-refund-checkbox-wrapper">
                            <td class="label">
                                <label><?php _e('To do Full Refund please tick checkbox (This will unenroll users from group and delete group ):', EBBP_TD); ?></label>
                            </td>
                            <td class="total">
                                <input type="checkbox" class="text" id="bp-full-refund-check" name="bp-full-refund-check" class="bp-refund-inp-fields" />
                                <div class="clear"></div>
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" id="bp_order_id" name="bp_order_id" value="<?= $orderId ?>" />
                    <?php wp_nonce_field('bp_refund_unenrol', 'bp_refund_unenrol'); ?>
                    <div class="clear"></div>
                </div>
                <?php
                $html = ob_get_clean();

                wp_localize_script(
                    'bp_admin_refund_js',
                    'bpRefund',
                    array(
                        'order' => $order,
                        'html'  => $html
                    )
                );

                wp_enqueue_script('bp_admin_refund_js');
            }
        }



        public function refundHandler($orderId, $refundId)
        {
            global $wpdb;
            $refundData = get_post_meta($orderId, "bp-refund-data", 1);
            $refundId = $refundId;
            if (isset($refundData["refund-type"]) && !empty($refundData["refund-type"])) {
                $cohorts = get_post_meta($orderId, "eb_bp_mdl_cohort_id", 1);
                if ("bp-partial-refund" == $refundData["refund-type"]) {
                    foreach ($cohorts as $cohortId => $quantity) {
                        $quantity = $quantity;
                        if (isset($refundData["bp-partial-refund-quantity_".$cohortId]) && !empty($refundData["bp-partial-refund-quantity_".$cohortId])) {
                            $this->decreaseBulkQuantity($cohortId, $refundData["bp-partial-refund-quantity_".$cohortId], $orderId);
                        }
                    }
                }

                if ("bp-full-refund" == $refundData["refund-type"]) {
                    if (isset($refundData["full-refund"]) && !empty($refundData["full-refund"]) && $refundData["full-refund"] == "on") {
                        $emailArgs = array();
                        $tableName = $wpdb->prefix."bp_cohort_info";
                        $cohortIdArray = array_keys($cohorts);
                        //gathering email related data
                        foreach ($cohorts as $cohortId => $quantity) {
                            $query = $wpdb->prepare("SELECT NAME, COHORT_NAME, COHORT_MANAGER, PRODUCTS FROM $tableName WHERE MDL_COHORT_ID = %d;", $cohortId);
                            $row = $wpdb->get_row($query);
                            $cohortManager = get_user_by("ID", $row->COHORT_MANAGER);
                            $products = maybe_unserialize($row->PRODUCTS);
                            $quantity = 0;
                            foreach ($products as $productId => $qty) {
                                $productId = $productId;
                                $quantity = $qty;
                                break;
                            }


                            array_push(
                                $emailArgs,
                                array(
                                    "username"               => $cohortManager->user_login,
                                    "user_email"             => $cohortManager->user_email,
                                    "first_name"             => $cohortManager->first_name,
                                    "last_name"              => $cohortManager->last_name,
                                    "order_id"               => $orderId,
                                    "bulk_refund_type"       => "fully",
                                    "refunded_bulk_quantity" => $quantity,
                                    "group_name"             => $row->NAME ? $row->NAME : $row->COHORT_NAME
                                )
                            );
                        }


                        $cohortManger = new BPManageCohort();
                        $result = $cohortManger->deleteCohort($cohortIdArray);
                        if ($result) {
                            delete_post_meta($orderId, "eb_bp_mdl_cohort_id");

                            foreach ($emailArgs as $args) {
                                do_action("eb_bp_bulk_purchase_refund", $args);
                            }
                        }
                        /*foreach ($cohorts as $cohortId => $quantity) {
                            //DELETE THE BULK PURCHASE ORDER META
                        }*/
                    }
                }
            }

            //DELETE ORDER META OF THE REFUND
            delete_post_meta($orderId, "bp-refund-data");
        }




        /**
         * On partial refund decrese available quantity of the group.
         * @param  [type] $cohortId [description]
         * @param  [type] $quantity [description]
         * @param  [type] $orderId  [description]
         * @return [type]           [description]
         */
        public function decreaseBulkQuantity($cohortId, $quantity, $orderId)
        {
            global $wpdb;
            if ($quantity) {
                $tableName = $wpdb->prefix."bp_cohort_info";
                $query = $wpdb->prepare("SELECT * FROM $tableName WHERE MDL_COHORT_ID = %d", $cohortId);
                $row = $wpdb->get_row($query);
                $products = array();

                foreach (maybe_unserialize($row->PRODUCTS) as $productId => $qty) {
                    // $productId = $productId;
                    if ($qty >= $quantity) {
                        $qty = $qty - $quantity;
                        if ($qty >= 0) {
                            $products[$productId] = $qty;
                            // array_push($products, $qty);
                        }
                    }
                }

                $query = $wpdb->prepare("UPDATE $tableName SET PRODUCTS = %s WHERE MDL_COHORT_ID = %d", maybe_serialize($products), $cohortId);
                $wpdb->get_results($query);

                $cohortManager = get_user_by("ID", $row->COHORT_MANAGER);
                // $groupName = $row->NAME ? $row->NAME : $row->COHORT_NAME;

                $args = array(
                        "username"               => $cohortManager->user_login,
                        "user_email"             => $cohortManager->user_email,
                        "first_name"             => $cohortManager->first_name,
                        "last_name"              => $cohortManager->last_name,
                        "order_id"               => $orderId,
                        "bulk_refund_type"       => "partially",
                        "refunded_bulk_quantity" => $quantity,
                        "group_name"             => $row->NAME ? $row->NAME : $row->COHORT_NAME
                    );

                do_action("eb_bp_bulk_purchase_refund", $args);
            }
        }



        /**
         * Save refund fields data added on the refund settings.
         * this function called ion the ajax as there is no other way to get that settings on the form save
         * @return [type] [description]
         */
        public function saveRefundData()
        {
            if (isset($_POST["refund-type"]) && !empty($_POST["refund-type"]) && isset($_POST["order-id"])) {
                $orderId = $_POST["order-id"];
                $refundData = array(
                    "refund-type" => $_POST['refund-type'],
                    "full-refund" => $_POST['full-refund']
                );

                $cohorts = get_post_meta($orderId, "eb_bp_mdl_cohort_id", 1);
                foreach ($cohorts as $cohortId => $quantity) {
                    $quantity = $quantity;
                    // array_push($refundData, $_POST["bp-partial-refund-quantity_".$cohortId]);
                    $refundData["bp-partial-refund-quantity_".$cohortId] = $_POST["bp-partial-refund-quantity_".$cohortId];
                }
                update_post_meta($orderId, "bp-refund-data", $refundData);
            }
        }
    }
}
