<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="woocommerce-order">

	<?php
	if ( $order ) :

		do_action( 'woocommerce_before_thankyou', $order->get_id() );
		?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

			<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

				<li class="woocommerce-order-overview__order order">
					<?php esc_html_e( 'Order number:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<li class="woocommerce-order-overview__date date">
					<?php esc_html_e( 'Date:', 'woocommerce' ); ?>
					<strong><?php echo wc_format_datetime( $order->get_date_created() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
					<li class="woocommerce-order-overview__email email">
						<?php esc_html_e( 'Email:', 'woocommerce' ); ?>
						<strong><?php echo $order->get_billing_email(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
					</li>
				<?php endif; ?>

				<li class="woocommerce-order-overview__total total">
					<?php esc_html_e( 'Total:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php if ( $order->get_payment_method_title() ) : ?>
					<li class="woocommerce-order-overview__payment-method method">
						<?php esc_html_e( 'Payment method:', 'woocommerce' ); ?>
						<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
					</li>
				<?php endif; ?>

			</ul>

		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
		
		<table class="woocommerce-table woocommerce-table--order-details shop_table order_details address-details">
			<thead>
				<tr>
					<th class="woocommerce-table__product-table billing-address">Billing address</th>
					<th class="woocommerce-table__product-table delivery-address">Delivery address</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="woocommerce-table__product-name billing-address">
						<?php
						$bname1 = $order->get_billing_first_name();
						$bname2 = $order->get_billing_last_name();
						$baddr1 = $order->get_billing_address_1();
						$baddr2 = $order->get_billing_address_2();
						$bcity = $order->get_billing_city();
						$bstate = $order->get_billing_state();
						$bcode = $order->get_billing_postcode();
						$bphone = $order->get_billing_phone();
						$bemail = $order->get_billing_email();
						echo "<p class='woocommerce-customer-details-addr'> <strong>".$bname1." ".$bname2."</strong><br />";
						if($baddr1){
							echo $baddr1."<br />";
						}
						if($baddr2){
							echo $baddr2."<br />";
						}
						if($bcity){
							echo $bcity."<br />";
						}
						if($bstate){
							echo $bstate."<br />";
						}
						if($bcode){
							echo $bcode."<br />";
						}
						echo "</p>";
						if($bphone){
							echo "<p class='woocommerce-customer-details-phone'>".$bphone."</p>";
						}
						if($bemail){
							echo "<p class='woocommerce-customer-details-email'><a href='mailto:".$bemail."'>".$bemail."</a></p>";
						}						
						?>
					</td>
					<td class="woocommerce-table__product-name delivery-address">
						<?php
						$sname1 = $order->get_shipping_first_name();
						$sname2 = $order->get_shipping_last_name();
						$saddr1 = $order->get_shipping_address_1();
						$saddr2 = $order->get_shipping_address_2();
						$scity = $order->get_shipping_city();
						$sstate = $order->get_shipping_state();
						$scode = $order->get_shipping_postcode();
						echo "<strong>".$sname1." ".$sname2."</strong><br />";
						if($saddr1){
							echo $saddr1."<br />";
						}
						if($saddr2){
							echo $saddr2."<br />";
						}
						if($scity){
							echo $scity."<br />";
						}
						if($sstate){
							echo $sstate."<br />";
						}
						if($scode){
							echo $scode."<br />";
						}						
						?>						
					</td>
				</tr>
			</tbody>
		</table>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

	<?php endif; ?>

</div>
