<?php get_header(); ?>
<?php

if( function_exists('is_checkout') && !is_checkout() && !is_cart() && !is_account_page() ) {
	$width = 'thin';
}

?>
<?php one_get_content('content-parts', 'article-header'); ?>
<div class="outer page-outer" id="main">
	<div class="inner container full">
		<div class="container <?php echo $width ?>">
			<?php 
			if (have_posts()):
				while(have_posts()):
					the_post();
					the_content();
				endwhile; 
			endif;
			?>
		</div>
	</div>
</div>
<?php get_footer(); ?>