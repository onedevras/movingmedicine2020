( function( blocks, element, blockEditor ) {
  var el = element.createElement;
  var InnerBlocks = blockEditor.InnerBlocks;

  // TODO 👉 Update hard link to the mm logo with the correct icon
  const INSIGHT_TEMPLATE = [
    [ 'mm/cg-insight-heading', {} ],
    [ 'core/image', { align: 'left', url: '/wp-content/themes/moving-medicine-2020/images/insight.svg', 'alt': 'icon', 'width': 120 } ],
    [ 'core/group', {}, [ 
      [ 'core/freeform', {} ],
    ]]
  ];

  blocks.registerBlockType( 'mm/cg-insight-group', {
      title: 'Insight Group',
      category: 'mm-blocks',

      edit: function( props ) {
          return el(
              'div',
              { className: props.className + ' mm-group insight-group' },
              el(
                'div',
                { className: 'mm-group_inner insight-group_inner thin' },
                el( InnerBlocks,
                  {
                      template: INSIGHT_TEMPLATE,
                      templateLock: "all",
                  }
                )
              )
              
          );
      },

      save: function( props ) {
          return el(
            'div',
            { className: 'mm-group insight-group' },
            el(
              'div',
              { className: 'mm-group_inner insight-group_inner thin' },
              el( InnerBlocks.Content )
            )
          );
      },
  } );
} (
  window.wp.blocks,
  window.wp.element,
  window.wp.blockEditor,
) );