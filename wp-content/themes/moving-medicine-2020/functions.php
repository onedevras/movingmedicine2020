<?php

// Set CENTRAL as the main site
if (is_multisite())
  define('CENTRAL_SITE', get_network()->site_id);

  define('INTERNATIONAL_FIELDS', array(
    // Conditions
    'field_5e677bd8abfa0',
    'field_5e57f38f07d94',
    'field_5e57f790e9c0e',
    'field_5e57f7cee9c11',
    'field_5e57f790e9c0e',
    'field_5e58fbe40ed21',
    'field_5e58fcda0ed35',
    'field_5e58fcda0ed34',
    'field_5e58fd1d0ed3a',
    'field_5e58fd1d0ed3b',
    // Footers
    // Developed
    'field_5ed57153d60fd',
    'field_5e7cf20427347',
    'field_5e7cf20429ecf',
    'field_5e7cf20429edd',
    // Endorsed (condition)
    'field_5ed571822cd92',
    'field_5e7cf19f23888',
    'field_5e7cf19f2c8b9',
    'field_5e7cf19f2c8d2'
  ));

require_once('inc/classes/wp-async-request.php');
require_once('inc/classes/wp-background-process.php');

// template tags used to get content
include('inc/template-tags.php');
include('inc/blocks-functions.php');
include('inc/custom-posts.php');
include('inc/custom-taxonomies.php');
include('inc/custom-shortcodes.php');
include('inc/multisite-functions.php');
include('inc/geo-functions.php');
include('inc/phpqrcode/qrlib.php');
include('woocommerce/woo-functions.php');

// add editor style sheet
add_editor_style('editor-style.css');
add_theme_support('editor-styles');
add_theme_support('responsive-embeds');

remove_filter('the_excerpt', 'wpautop');
remove_filter('the_content', 'wpautop');
remove_filter('lostpassword_url', 'wc_lostpassword_url', 10, 1);

function mm_remove_menus()
{

  remove_menu_page('edit.php');                   //Posts
  remove_menu_page('edit-comments.php');          //Comments
  // remove_menu_page('edit.php?post_type=acf-field-group');
  remove_menu_page( 'customtaxorder' );
  // remove_menu_page('cptui_main_menu');
  remove_menu_page('shortcodes-ultimate');
}
add_action('admin_menu', 'mm_remove_menus');

// Register Options Page
function setup_options()
{

  // Main options page
  if (function_exists('acf_add_options_page')) :
    acf_add_options_page(array(
      'page_title'  => 'Options',
      'menu_title'  => 'Options',
      'menu_slug'   => 'options',
      'capability'  => is_multisite() ? 'manage_options_international' : 'manage_options',
      'redirect'    => true
    ));
  endif;

    if (function_exists('acf_add_options_sub_page')) :
      acf_add_options_sub_page(array(
        'title' => 'International Options',
        'parent' => 'options',
        'capability' => 'administrator'
      ));
    endif;

  // Site options sub page
  if (function_exists('acf_add_options_sub_page')) :
    acf_add_options_sub_page(array(
      'title' => 'Site Options',
      'parent' => 'options',
      'capability' => is_multisite() ? 'manage_options_international' : 'manage_options'
    ));
  endif;

  // Theme options sub page
  if (function_exists('acf_add_options_sub_page')) :
    acf_add_options_sub_page(array(
      'title' => 'Footer Options',
      'parent' => 'options',
      'capability' => is_multisite() ? 'manage_options_international' : 'manage_options'
    ));
  endif;

  if (function_exists('acf_add_options_sub_page')) :
    acf_add_options_sub_page(array(
      'title' => 'Header Options',
      'parent' => 'options',
      'capability' => is_multisite() ? 'manage_options_international' : 'manage_options'
    ));
  endif;
}
add_action('acf/init', 'setup_options');

// Enqueue Extra Styles
function mm_enqueue_styles()
{
  if (!is_admin()) {
    // wp_deregister_style('Edwiser Bridge Single Sign On-public-style');
    // wp_deregister_style('edwiserbridge_font_awesome');
    // wp_deregister_style('wdmdatatablecss');
    // wp_deregister_style('eb-public-jquery-ui-css');
    // wp_deregister_style('WooCommerce Integration');
    // wp_deregister_style('woocommerce_integration');
  }
  wp_enqueue_style('typekit', 'https://use.typekit.net/iok1hqp.css', false, false);
  wp_enqueue_style('google_fonts', 'https://fonts.googleapis.com/css?family=Schoolbell&display=swap', false, false);
}
add_action('oneltd_enqueue_styles', 'mm_enqueue_styles');


// Enqueue extra scripts
function mm_enqueue_scripts()
{

  wp_register_script('fa', 'https://kit.fontawesome.com/efe8ea0450.js', array(), NULL, false);
  wp_enqueue_script('fa');
  wp_register_script('tippy-polyfill', 'https://polyfill.io/v3/polyfill.min.js?features=Array.prototype.find,Promise,Object.assign', array(), NULL, true);
  wp_enqueue_script('tippy-polyfill');
  wp_register_script('popper', 'https://unpkg.com/@popperjs/core@2', array(), NULL, true);
  wp_enqueue_script('popper');
  wp_register_script('tippy', 'https://unpkg.com/tippy.js@6', array(), NULL, true);
  wp_enqueue_script('tippy');

  if (!is_singular('condition')) {
    wp_register_script('vimeo', 'https://player.vimeo.com/api/player.js#asyncload', array(), NULL, true);
    wp_enqueue_script('vimeo');
    wp_register_script('youtube', 'https://www.youtube.com/iframe_api#asyncload', array(), NULL, true);
    wp_enqueue_script('youtube');
  }
}

add_action('oneltd_enqueue_scripts', 'mm_enqueue_scripts');

function add_async_forscript($url)
{
  if (strpos($url, '#asyncload') === false)
    return $url;
  else if (is_admin())
    return str_replace('#asyncload', '', $url);
  else
    return str_replace('#asyncload', '', $url) . "' async='async";
}
add_filter('clean_url', 'add_async_forscript', 11, 1);


// Register the navigation menus
function mm_register_nav_menu()
{
  register_nav_menus(array(
    'main-navigation' => __('Main navigation', 'text_domain'),
    'top-navigation'  => __('Top navigation', 'text_domain'),
    'footer-navigation'  => __('Footer navigation', 'text_domain'),
  ));
}
add_action('after_setup_theme', 'mm_register_nav_menu');

// Set default color palettes for blocks
function mm_block_color_setup()
{
  // Disable Custom Colors
  add_theme_support('disable-custom-colors');

  // Editor Color Palette
  add_theme_support('editor-color-palette', array(
    array(
      'name'  => __('Blue', 'mm-starter'),
      'slug'  => 'blue',
      'color'  => '#00bfd7',
    ),
    array(
      'name'  => __('Green', 'mm-starter'),
      'slug'  => 'green',
      'color'  => '#72d44a',
    ),
    array(
      'name'  => __('Yellow', 'mm-starter'),
      'slug'  => 'yellow',
      'color' => '#ffc200',
    ),
    array(
      'name'  => __('Pink}', 'mm-starter'),
      'slug'  => 'pink',
      'color' => '#ff5c7e',
    ),
    array(
      'name'  => __('AH Blue', 'mm-starter'),
      'slug'  => 'ah-blue',
      'color'  => '#6acbe0',
    ),
    array(
      'name'  => __('AH Purple', 'mm-starter'),
      'slug'  => 'ah-purple',
      'color' => '#6858e9',
    ),
    array(
      'name'  => __('Black', 'mm-starter'),
      'slug'  => 'black',
      'color' => '#333',
    ),
    array(
      'name'  => __('White', 'mm-starter'),
      'slug'  => 'white',
      'color'  => '#FFF',
    ),
    array(
      'name'  => __('Grey', 'mm-starter'),
      'slug'  => 'grey',
      'color' => '#989898',
    ),
    array(
      'name'  => __('Light Grey', 'mm-starter'),
      'slug'  => 'lightgrey',
      'color'  => '#f2efef',
    ),
  ));
}
add_action('after_setup_theme', 'mm_block_color_setup');


function mm_add_woocommerce_support()
{
  add_theme_support('woocommerce');
}
add_action('after_setup_theme', 'mm_add_woocommerce_support');

add_action('pre_post_update', 'wp_save_post_revision');

// Get a template file
function one_get_content($type, $slug, $args = null)
{
  $url = '/inc/' . $type . '/' . $slug . '.php';
  if (locate_template($url)) {
    include(locate_template($url));
  } else {
    echo '<p>missing template: ' . $url . '</p>';
  }
}

// Function for calling in ACF flexible content loop. Note: stripname.php file names MUST be the same as ACF slug
function get_strips()
{
  if (have_rows('strips')) :
    while (have_rows('strips')) : the_row();
      $slug = get_row_layout();
      one_get_content('strips', $slug); // Our content part
    endwhile;
  endif;
}

// Get and print out an SVG file
function get_svg($slug)
{
  $url = '/images/' . $slug . '.svg';
  if (locate_template($url)) {
    include(locate_template($url));
  } else {
    echo '<p>missing template: ' . $url . '</p>';
  }
}

function get_svg_from_url($url)
{
  if ($url) {
    // Load and return the contents of the file
    $svg = false;

    $svg = file_get_contents($url, false, stream_context_create(
      array(
        'ssl' => array(
          'verify_peer' => false,
          'verify_peer_name' => false
        )
      )
    ));

    return $svg;
  }
}


// Remove meta boxes
function my_remove_wp_seo_meta_box()
{
  remove_meta_box('wpseo_meta', 'condition_slide', 'normal');
  remove_meta_box('wpseo_meta', 'condition', 'normal');
  remove_meta_box('wpseo_meta', 'evidence', 'normal');
  remove_meta_box('wpseo_meta', 'slide_extra', 'normal');
  remove_meta_box('wpseo_meta', 'organisation', 'normal');
  remove_meta_box('wpseo_meta', 'resource', 'normal');
  remove_meta_box('wpseo_meta', 'patient_information', 'normal');
}
add_action('add_meta_boxes', 'my_remove_wp_seo_meta_box', 100);

add_filter('use_block_editor_for_post_type', 'prefix_disable_gutenberg', 10, 2);
function prefix_disable_gutenberg($current_status, $post_type)
{
  if ($post_type === 'condition')
    return false;

  return $current_status;
}

function mm_query_vars($query_vars)
{
  $query_vars[] = 'guide';
  return $query_vars;
}
add_filter('query_vars', 'mm_query_vars');


function mm_rewrite_rules()
{
  add_rewrite_rule('^condition/([^/]+)/([^/]+)/?', 'index.php?name=$matches[1]&post_type=condition&guide=$matches[2]', 'top');
  add_rewrite_rule('^conditions/([^/]+)/([^/]+)/?', 'index.php?name=$matches[1]&post_type=condition&guide=$matches[2]', 'top');
  add_rewrite_rule('^consultation-guide/condition/([^/]+)/([^/]+)/([^/]+)/?', 'index.php?name=$matches[2]&post_type=condition&taxonomy=matches[1]&guide=$matches[3]', 'top');
  add_rewrite_rule('^consultation-guides/condition/([^/]+)/([^/]+)/([^/]+)/?', 'index.php?name=$matches[2]&post_type=condition&taxonomy=matches[1]&guide=$matches[3]', 'top');
  add_rewrite_rule('^condition/?', 'index.php?pagename=consultation-guides/find-the-right-consultation/', 'top');
  add_rewrite_rule('^conditions/?', 'index.php?pagename=consultation-guides/find-the-right-consultation/', 'top');
}
add_action('init', 'mm_rewrite_rules', 10, 0);

function mm_get_my_vars()
{
  global $wp_query;

  if (isset($wp_query->query_vars['guide'])) {
    $guide = get_query_var('guide');
    var_dump($guide);
  }
}

// Enqueue js for bocks
function mm_register_blocks()
{

  if (!function_exists('register_block_type')) {
    // Gutenberg is not active.
    return;
  }

  wp_register_script(
    'intro-group',
    get_stylesheet_directory_uri() . '/inc/blocks/js/intro-group/block.js',
    array('wp-blocks', 'wp-editor', 'wp-element')
  );

  register_block_type('mm/cg-intro-group', array(
    'editor_script' => 'intro-group',
  ));

  wp_register_script(
    'insight-heading',
    get_stylesheet_directory_uri() . '/inc/blocks/js/insight-heading/block.js',
    array('wp-blocks', 'wp-element')
  );

  register_block_type('mm/cg-insight-heading', array(
    'editor_script' => 'insight-heading',
  ));

  wp_register_script(
    'tool-group',
    get_stylesheet_directory_uri() . '/inc/blocks/js/tool-group/block.js',
    array('wp-blocks', 'wp-editor', 'wp-element')
  );

  register_block_type('mm/cg-tool-group', array(
    'editor_script' => 'tool-group',
  ));

  wp_register_script(
    'insight-group',
    get_stylesheet_directory_uri() . '/inc/blocks/js/insight-group/block.js',
    array('wp-blocks', 'wp-editor', 'wp-element')
  );

  register_block_type('mm/cg-insight-group', array(
    'editor_script' => 'insight-group',
  ));
}
add_action('init', 'mm_register_blocks');

function mm_blocks_js()
{
  wp_enqueue_script(
    'gdt-plugin-blacklist-blocks',
    get_stylesheet_directory_uri() . '/inc/blocks/js/customise-blocks.js',
    array('wp-blocks')
  );
}
add_action('enqueue_block_editor_assets', 'mm_blocks_js');

function mm_body_classes($classes)
{
  global $post;
  // Add Classes to body if the post type archive is 'publikasjoner'
  if (is_consultation_guides_page()) {
    $classes[] = 'consultation-guide';
  } elseif ('condition' === get_post_type()) {
    $classes[] = 'consultation-guide';
    if (get_query_var('guide')) {
      $classes[] = 'consultation-guide-conversation ' . str_replace("_", "-", get_query_var('guide'));
    } else {
      $classes[] = 'consultation-guide-landing';
    }
    $patient_types = get_the_terms(get_the_ID(), 'patient');
    // There should be only one of patient type
    foreach ($patient_types as $patient_type) {
      $classes[] = $patient_type->slug;
    }
  } elseif (is_active_hospitals_page() || 'active-hospitals' === $post->post_name) {
    $classes[] = 'active-hospitals';
  } else {
    $classes[] = 'core';
  }

  return $classes;
}
add_filter('body_class', 'mm_body_classes');

add_filter(
  'acf/load_field/name=evidence_shortcode',
  function ($field) {
    $field['default_value'] = '[ev id=' . get_the_ID() . ']';
    $field['disabled'] = 1;
    return $field;
  }
);

// add_filter('acf/prepare_field', 'my_acf_prepare_field');

// Disable fields that aren't in the list of active fields
add_filter('acf/prepare_field', 'mm_acf_prepare_condition_fields');
function mm_acf_prepare_condition_fields($field)
{
  $user = wp_get_current_user();

  if ( !in_array( 'administrator', (array) $user->roles ) && // Not admin
       !in_array($field['key'], INTERNATIONAL_FIELDS) &&  // Not in whitelist
       ( !is_array($field['taxonomy']) || strpos($field['taxonomy'][0], 'slide_extra_type') < 0) ) // Not a slide_extra
  {
    if ('condition' === get_post_type() ) {
      // $field['disabled'] = 1;
      $field['instructions'] = 'This field cannot be edited. Any changes will not be approved.';
      if('image' == $field['type']) {
        return;
      }
    }

    return $field;
  }
  if ('condition' === get_post_type() && isset($field['value'])) {
    if('post_object' == $field['type']) {
      $field['instructions'] = '<a href="/wp-admin/post.php?post=' . $field['value'] . '&action=edit" target="_blank">Link to ' . get_the_title($field['value']) . '</a>';
    }
  }

  return $field;
}


add_filter('manage_evidence_posts_columns', 'mm_filter_evidence_posts_columns');
function mm_filter_evidence_posts_columns($columns)
{
  $columns['shortcode'] = __('Shortcode', 'mm');
  return $columns;
}

add_action('manage_evidence_posts_custom_column', 'mm_evidence_shortcode_column', 10, 2);
function mm_evidence_shortcode_column($column, $post_id)
{
  // Shortcode column
  if ('shortcode' === $column) {
    $shortcode = '[ev id=' . get_the_ID() . ']';
  }
  echo $shortcode;
}

add_filter('manage_condition_posts_columns', 'mm_filter_condition_posts_columns');
function mm_filter_condition_posts_columns($columns)
{
  $columns['patient'] = __('Patient Type', 'mm');
  return $columns;
}

add_action('manage_condition_posts_custom_column', 'mm_condition_column', 10, 2);
function mm_condition_column($column, $post_id)
{
  // Patient Type column
  if ('patient' === $column) {
    $patient = get_the_terms($post_id, 'patient');
  }
  echo $patient[0]->name;
}

// Custom rule for targeting a specific depth of menu
function acf_location_rules_types($choices)
{
  $choices['Menu']['menu_level'] = 'Menu Depth';
  return $choices;
}

add_filter('acf/location/rule_types', 'acf_location_rules_types');

add_filter('acf/location/rule_values/menu_level', 'acf_location_rule_values_level');
function acf_location_rule_values_level($choices)
{
  $choices[0] = '0';
  $choices[1] = '1';

  return $choices;
}

// Check if depth of menu is equal to the value specified in Menu Depth
add_filter('acf/location/rule_match/menu_level', 'acf_location_rule_match_level', 10, 4);
function acf_location_rule_match_level($match, $rule, $options, $field_group)
{
  $current_screen = get_current_screen();
  if ('nav-menus' == $current_screen->base) {
    if ('==' == $rule['operator']) {
      $match = ($options['nav_menu_item_depth'] == $rule['value']);
    }
  }
  return $match;
}

// Convert checkboxes to radios in the sidebar for conditions
add_action('add_meta_boxes', 'mm_add_meta_boxes', 10, 2);
function mm_add_meta_boxes($post_type, $post)
{
  if ('condition' !== $post_type)
    return;
  ob_start();
}
add_action('dbx_post_sidebar', 'mm_dbx_post_sidebar');
function mm_dbx_post_sidebar()
{
  if ('condition' !== get_post_type())
    return;
  $html = ob_get_clean();
  $html = str_replace('"checkbox"', '"radio"', $html);
  echo $html;
}

// Save post functions
add_action('save_post', 'trigger_build_active_hospital_menu', 10, 3);
function trigger_build_active_hospital_menu($post_id, $post, $update)
{

  // Only set for post_type = active-hospitals!
  if (!is_active_hospitals_page($post)) {
    return;
  }
  build_active_hospital_menu();
}

// Recursively look for evidence in condition
function find_evidence_in_condition($arr, $condition)
{
  if (is_array($arr)) {
    foreach ($arr as $item) {
      // If the item is an array then recurse
      if (is_array($item)) {
        find_evidence_in_condition($item, $condition);
      } else if ($item instanceof WP_Post) {
        set_condition_tax_in_evidence($item->post_content, array($condition));
      }
    }
  }
}

function set_condition_tax_in_evidence($content, $conditions)
{
  preg_match_all('/\[ev id=([^\]]+)\]/', $content, $matches);
  if (array_key_exists('1', $matches)) {
    foreach ($matches[1] as $evidence_id) {
      // For each bit of evidence that appears on the slide, set its terms
      // for the conditions this slide appears on
      $conditions_string = implode(', ', $conditions);
      // error_log("🔥🔥🔥 Setting {$conditions_string} on {$evidence_id}", 0);
      wp_set_object_terms($evidence_id, $conditions, 'condition-tax', true);
    }
  }
}

// Check for evidence used in conditions and find conditions for
// condition_slides and slide_extras and set the terms in any evidence found
add_action('save_post', 'set_evidence_terms_after_save', 10, 3);
function set_evidence_terms_after_save($post_id, $post, $update)
{
  $content = '';
  $conditions = array();

  // Only set for post_type = condition_slide or condition!
  if (
    'condition_slide' !== $post->post_type &&
    'condition' !== $post->post_type &&
    'slide_extra' !== $post->post_type
  ) {
    return;
  }

  // We're looking at a condition which is a group of components
  if ('condition' === $post->post_type) {

    $condition = $post->post_name;

    // Convert content to string for searching
    $fields = get_fields($post_id);
    find_evidence_in_condition($fields, $condition);
  }
  // Otherwise we're looking at an individual component of 1-N conditions
  else {
    // Get the conditions this post is used on
    $query_args = array(
      'numberposts' => -1,
      'post_type' => 'condition',
      'meta_query' => array(
        array(
          'value' => $post->ID,
        )
      ),
      'orderby' => 'title',
      'order' => 'ASC'
    );

    $conditions = array_map('get_slugs', get_posts($query_args));
    $content = $post->post_content;
    // If the post is a condition_slide then search the post content for an
    // instance of the evidence shortcode
    set_condition_tax_in_evidence($content, $conditions);
  }
}


// Update all conditions to populate evidence condition_tax terms
// add_action( 'wp_loaded', 'update_all_posts' );
function update_all_posts()
{
  $args = array(
    'post_type' => 'condition',
    'numberposts' => -1
  );
  $all_posts = get_posts($args);
  foreach ($all_posts as $single_post) {
    $single_post->post_title = $single_post->post_title . '';
    wp_update_post($single_post);
  }
}

add_action('save_post', 'trigger_build_evidence_finder_list', 10, 3);
function trigger_build_evidence_finder_list($post_id, $post, $update)
{
  // Only set for post_type = condition_slide or condition!
  if (
    'slide_extra' !== $post->post_type &&
    'condition_slide' !== $post->post_type &&
    'condition' !== $post->post_type &&
    'evidence' !== $post->post_type
  ) {
    return;
  }
  // _one_log("Building evidence finder");
  build_evidence_finder_list();
}

add_action('save_post', 'trigger_build_activity_finder_list', 10, 3);
function trigger_build_activity_finder_list($post_id, $post, $update)
{

  // Only set for post_type = condition_slide or condition!
  if ('organisation' !== $post->post_type) {
    return;
  }
  build_activity_finder_list();
}

add_action('save_post', 'trigger_build_patient_information_finder_list', 10, 3);

function trigger_build_patient_information_finder_list($post_id, $post, $update)
{

  // Only set for post_type = condition_slide or condition!
  if ('patient_information' !== $post->post_type) {
    return;
  }
  build_patient_information_finder_list();
}

add_action('save_post', 'trigger_build_consultation_finder_list', 10, 3);

function trigger_build_consultation_finder_list($post_id, $post, $update)
{

  // Only set for post_type = condition_slide or condition!
  if ('condition' !== $post->post_type) {
    return;
  }
  $force = true;
  build_nav_consultation_finder($force);
  build_consultation_finder_list();
}

function mm_add_default_terms()
{
  // Patients
  wp_insert_term(
    'Adult', // the term
    'patient', // the taxonomy
    array(
      'slug' => 'adult'
    )
  );
  wp_insert_term(
    'Young Person', // the term
    'patient', // the taxonomy
    array(
      'slug' => 'young-person'
    )
  );
  wp_insert_term(
    'Child', // the term
    'patient', // the taxonomy
    array(
      'slug' => 'child'
    )
  );

  // Slide Extras
  wp_insert_term(
    'Did you know', // the term
    'slide_extra_type', // the taxonomy
    array(
      'slug' => 'did_you_know'
    )
  );
  wp_insert_term(
    'Real impact', // the term
    'slide_extra_type', // the taxonomy
    array(
      'slug' => 'real_impact'
    )
  );

  // Activities/Organisations
  wp_insert_term(
    'Support', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'support'
    )
  );

  $parent_term = term_exists('support', 'organisation_type'); // array is returned if taxonomy is given
  $parent_term_id = $parent_term['term_id'];

  wp_insert_term(
    'Disadvantaged communities', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'disadvantaged-communities',
      'parent' => $parent_term_id
    )
  );

  wp_insert_term(
    'Family', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'family',
      'parent' => $parent_term_id
    )
  );

  wp_insert_term(
    'Activity', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'activity'
    )
  );

  $parent_term = term_exists('activity', 'organisation_type'); // array is returned if taxonomy is given
  $parent_term_id = $parent_term['term_id'];

  wp_insert_term(
    'Being active with a disability', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'being-active-with-a-disability',
      'parent' => $parent_term_id
    )
  );

  wp_insert_term(
    'Walking', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'walking',
      'parent' => $parent_term_id
    )
  );

  wp_insert_term(
    'Cycling', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'cycling',
      'parent' => $parent_term_id
    )
  );

  wp_insert_term(
    'Jogging', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'jogging',
      'parent' => $parent_term_id
    )
  );

  wp_insert_term(
    'Home exercise', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'home-exercise',
      'parent' => $parent_term_id
    )
  );

  wp_insert_term(
    'Apps', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'apps',
      'parent' => $parent_term_id
    )
  );

  wp_insert_term(
    'Dance and fitness', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'dance-and-fitness',
      'parent' => $parent_term_id
    )
  );

  wp_insert_term(
    'Sport', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'sport',
      'parent' => $parent_term_id
    )
  );

  wp_insert_term(
    'Conservation and volunteering', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'conservation-and-vounteering',
      'parent' => $parent_term_id
    )
  );

  wp_insert_term(
    'At work', // the term
    'organisation_type', // the taxonomy
    array(
      'slug' => 'at-work',
      'parent' => $parent_term_id
    )
  );
}
add_action('init', 'mm_add_default_terms');

function condition_tax_init()
{

  $labels = [
    "name" => __('Conditions'),
    "singular_name" => __('Condition'),
    "search_items" => __('Search Conditions'),
  ];

  //set some options for our new custom taxonomy
  $args = array(
    'label' => __('Conditions'),
    'labels' => $labels,
    "public" => true,
    "publicly_queryable" => true,
    "hierarchical" => true,
    "show_ui" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "query_var" => true,
    "rewrite" => ['slug' => 'condition-tax', 'with_front' => true,],
    "show_admin_column" => false,
    "show_in_rest" => true,
    "rest_base" => "condition-tax",
    "rest_controller_class" => "WP_REST_Terms_Controller",
    "show_in_quick_edit" => true,
    'capabilities' => array(
      // allow anyone editing posts to assign terms
      'assign_terms' => 'edit_posts',
      /*
      * but you probably don't want anyone except
      * admins messing with what gets auto-generated!
      */
      'edit_terms' => 'administrator'
    )
  );

  /*
  * attach it to custom post organisation - we want to be
  * able to select the conditions the org relates to
  */
  register_taxonomy('condition-tax', ['organisation', 'patient_information', 'evidence'], $args);
}

add_action('init', 'condition_tax_init');

function update_condition_tax_terms($post_id)
{
  $taxonomy = 'condition-tax';

  // only update terms if it's a condition post
  if ('condition' !== get_post_type($post_id)) {
    return;
  }

  // don't create or update terms for system generated posts
  if ('publish' !== get_post_status($post_id)) {
    return;
  }

  /*
  * Grab the post title and slug to use as the new
  * or updated term name and slug
  */
  $term_title = get_the_title($post_id);
  $term_slug = get_post($post_id)->post_name;

  /**
   * Get the patient type to use as the parent term
   */
  $parent = get_the_terms($post_id, 'patient');
  // There should only be one set, so check it has a value and grab the first one
  $parent_slug = isset($parent[0]) ? $parent[0]->slug : false;

  // There's no patient set on the post so return
  if (!$parent_slug)
    return;

  $parent_name = $parent[0]->name;
  $parent_term = term_exists($parent_slug, $taxonomy);

  // We need to create the parent term
  if (!$parent_term) {
    $parent_term = wp_insert_term(
      $parent_name,
      $taxonomy,
      array(
        'slug' => $parent_slug,
      )
    );
  }

  $parent_term_id = $parent_term['term_id'];

  /*
  * Check if a corresponding term already exists by comparing
  * the post ID to all existing term descriptions.
  */
  $existing_terms = get_terms(
    $taxonomy,
    array(
      'hide_empty' => false
    )
  );

  foreach ($existing_terms as $term) {
    if ($term->description == $post_id) {
      // term already exists so update it and we're done
      wp_update_term(
        $term->term_id,
        $taxonomy,
        array(
          'name' => $term_title,
          'slug' => $term_slug,
          'parent' => $parent_term_id
        )
      );
      return;
    } else if ($term->slug === $term_slug && '' === $term->description) {
      // For some reason the post ID isn't in the description
      wp_update_term(
        $term->term_id,
        $taxonomy,
        array(
          'name' => $term_title,
          'description' => $post_id,
          'parent' => $parent_term_id
        )
      );
      return;
    }
  }

  /*
  * If we didn't find a match above, this is a new post,
  * so create a new term.
  */
  wp_insert_term(
    $term_title,
    $taxonomy,
    array(
      'slug' => $term_slug,
      'description' => $post_id,
      'parent' => $parent_term_id
    )
  );
}

//run the update function whenever a post is created or edited
add_action('save_post', 'update_condition_tax_terms');


function mm_footer_has_logos($types)
{
  global $post;
  $post_id = $post->ID;

  if (is_page()) {
    if ($post->post_parent) {
      $ancestors = get_post_ancestors($post->ID);
      $root = count($ancestors) - 1;
      $post_id = $ancestors[$root];
    }
  }

  if (!is_array($types)) {
    if (have_rows($types . '_logos', $post_id))
      return true;
  } else {
    foreach ($types as $type) {
      if (have_rows($type . '_logos', $post_id))
        return true;
    }
  }
  return false;
}

function is_active_hospitals_page($post = false)
{
  global $post;
  $post_slug = get_post_field('post_name');
  if ($post_slug == 'active-hospitals')
    return true;
  if ($post->post_parent) {
    $ancestors = get_post_ancestors($post->ID);
    $root = count($ancestors) - 1;
    $post_id = $ancestors[$root];
    $post_slug = get_post_field('post_name', $post_id);
    if ($post_slug == 'active-hospitals')
      return true;
    else
      return false;
  } else {
    return false;
  }
}

function is_consultation_guides_page($post = false)
{
  global $post;
  $post_slug = get_post_field('post_name');
  if ($post_slug == 'consultation-guides')
    return true;
  if ($post->post_parent) {
    $ancestors = get_post_ancestors($post->ID);
    $root = count($ancestors) - 1;
    $post_id = $ancestors[$root];
    $post_slug = get_post_field('post_name', $post_id);
    if ($post_slug == 'consultation-guides')
      return true;
    else
      return false;
  } else {
    return false;
  }
}



// Add featured section to main nav menus
class MM_Main_Nav_Walker extends Walker_Nav_Menu
{

  private $curItem;

  function start_lvl(&$output, $depth = 0, $args = array())
  {

    $indent = str_repeat("\t", $depth);
    $featuredContent = "";
    $featuredClass = "";

    // Top level only
    if (0 === $depth) {
      $featured = get_field('featured_page', $this->curItem);
      if ($featured) {

        // We need to run some checks to make sure sections are enabled
        // if('Active Hospitals' === $this->curItem->title && get_field('disable_active_hospitals', 'options'))
        //   return;

        // if('Supporting you' === $this->curItem->title && get_field('disable_online_course', 'options'))
        //   return;

        $featuredClass = ' has-featured';
        $featuredContent .= "\n$indent\t<div class='feature'>";
        $featuredContent .= "\n$indent\t\t<a href='" . get_permalink($featured) . "'><h4>" . $featured->post_title . "</h4>";
        $featuredContent .= "\n$indent\t\t" . get_the_post_thumbnail($featured, 'icon') . "</a>";
        $featuredContent .= "\n$indent\t</div>\n$indent\t";
      } elseif ('Consultation Guides' === $this->curItem->title) {

        // if(get_field('disable_consultation_guides', 'options'))
        //   return;

        $featuredClass = ' has-finder';
        $featuredContent .=  build_nav_consultation_finder();
      }
    }
    $output .= "\n$indent<div class='sub-menu-wrap$featuredClass'>$featuredContent<ul class='sub-menu'>\n";
  }

  function end_lvl(&$output, $depth = 0, $args = array())
  {

    $indent = str_repeat("\t", $depth);
    $output .= "$indent\t</ul>\n$indent</div>";
  }

  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
  {
    // Record the current item for use in the lvl functions
    $this->curItem = $item;
    parent::start_el($output, $item, $depth, $args, $id);
  }
}

function mm_handle_nav_consultation_finder()
{

  $patient = $_POST['patients'];
  $condition = $_POST[$patient];
  $duration = $_POST['duration'];

  wp_redirect(site_url("consultation-guides/condition/$patient/$condition/$duration/"));
}
add_action('admin_post_nopriv_nav_consultation_finder', 'mm_handle_nav_consultation_finder');
add_action('admin_post_nav_consultation_finder', 'mm_handle_nav_consultation_finder');

function mm_show_permalinks($post_link, $post)
{
  if (is_object($post) && $post->post_type == 'condition') {
    $terms = wp_get_object_terms($post->ID, 'patient');
    if ($terms) {
      return str_replace('%patient%', $terms[0]->slug, $post_link);
    }
  }
  return $post_link;
}
add_filter('post_type_link', 'mm_show_permalinks', 1, 2);

// Add icons to menu items
function mm_add_icon_to_nav_menu_objects($items, $args)
{
  foreach ($items as &$item) {
    $icon = get_field('icon', $item);

    if ($icon) {
      $item->title = '<i class="menu-icon">' . get_svg_from_url($icon['url']) . '</i>' . $item->title;
    }
  }

  return $items;
}
add_filter('wp_nav_menu_objects', 'mm_add_icon_to_nav_menu_objects', 10, 2);

// Add international sites section to top nav menu
class MM_Top_Nav_Walker extends Walker_Nav_Menu
{

  function end_el(&$output, $item, $depth = 0, $args = null)
  {
    $add_dropdown = get_field('add_international_dropdown', $item);

    // We're assuming there isn't already a dropdown in place
    if ($add_dropdown) {

      $sites = get_sites();
      $count = 0;

      uasort( $sites, function( $a, $b ) {
          // Compare site names alphabetically for sorting purposes.
          return strcmp( $a->__get( 'blogname' ), $b->__get( 'blogname' ) );
        }
      );

      if ($sites) {
        $html = "\n\t<ul class='submenu'>";
        foreach ($sites as $blog) {
          // We want to skip Central
          if (CENTRAL_SITE == $blog->blog_id)
            continue;

          switch_to_blog($blog->blog_id);

          // We want to skip sites that aren't live
          if( !get_field('make_live', 'options') ) {
            restore_current_blog();
            continue;
          }

          $count++;
          $url = get_site_url($blog->blog_id);
          $name = get_bloginfo('name');
          $html .= "\n\t\t<li><a href='$url'>$name</a></li>";
          restore_current_blog();
        }
        $html .= "\n\t</ul>";
      }
    }
    if($count)
      $output .= $html;
    parent::end_el($output, $item, $depth, $args);
  }
}

if (!function_exists('_one_log')) {
  function _one_log($message)
  {
    $error_path = ini_get('error_log');
    $error_path_as_array = explode("/", $error_path);
    $error_path_as_array[count($error_path_as_array) - 1] = 'debug.log';
    $time_stamp = '[' . date('d\-F\-Y H:i:s e') . ']: ';
    if (is_array($message) || is_object($message)) {
      file_put_contents(implode('/', $error_path_as_array), $time_stamp . print_r($message, true) . PHP_EOL, FILE_APPEND);
	  file_put_contents(getcwd().'/debug.log', $time_stamp . print_r($message, true) . PHP_EOL, FILE_APPEND);
    } else {
      file_put_contents(implode('/', $error_path_as_array), $time_stamp . $message . PHP_EOL, FILE_APPEND);
	  file_put_contents(getcwd().'/debug.log', $time_stamp . $message . PHP_EOL, FILE_APPEND);
    }
  }
}

function mm_instagram_posts()
{

  $transient = get_transient('instagram');

  if ($transient) return $transient;

  $token = "6040346991.7be7077.8bf5479e367945bab689b5f395fc6e87"; // need to get this.

  $access_url = "https://api.instagram.com/v1/users/self/media/recent/?access_token=" . $token;
  $result = json_decode(mm_query_instagram($access_url));

  $insta_posts = array();
  foreach ($result->data as $key => $value) {
    $insta_posts[$key] = array(
      'type'  =>  'instagram',
      'date'  =>  date('Ymd', $value->created_time),
      'title' =>  $value->caption->text,
      'image' =>  $value->images->standard_resolution->url,
      'link'  =>  $value->link,
      'name'  =>  $value->user->username,
      'likes' =>  $value->likes->count,
      'comments'  =>  $value->comments->count
    );
  }

  set_transient($site . 'instagram', $insta_posts, HOUR_IN_SECONDS);

  return $insta_posts;
}

function mm_query_instagram($url)
{
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, 20);
  $result = curl_exec($ch);
  curl_close($ch);
  return $result;
}


/******************************************************************************
 *
 * International
 */

function country_geo_redirect()
{

  $city = getenv('HTTP_GEOIP_CITY');

  // if ($country == "US") {

  //   wp_redirect('https://en.wikipedia.org/wiki/United_States', 301);

  //   exit;
  // } else if ($country == "BR") {

  //   wp_redirect('https://en.wikipedia.org/wiki/Brazil', 301);

  //   exit;
  // }
}

// add_action('init', 'country_geo_redirect');




/**
 * See documentation of base theme at
 * http://git.oneltd.co.uk/one/oneltd-base/blob/master/README.md
 *
 * The base theme does some important bits while enqueuing scripts and styles,
 * you therefore need to load your extra ones a little differently to usual.
 *
 * To enqueue styles:
 *
 * add_action('oneltd_enqueue_styles', 'SITENAME_enqueue_styles');
 * function SITENAME_enqueue_styles() {
 *   wp_enqueue_style(...);
 * }
 *
 * To enqueue scripts:
 *
 * add_action('oneltd_enqueue_scripts', 'SITENAME_enqueue_scripts');
 * function SITENAME_enqueue_scripts() {
 *   wp_register_script(...);
 *   // etc
 * }
 */
