<?php get_header(); ?>

<!-- Billboard --> 
<section class="section billboard-section">
	<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
	<div class="video">
		<video autoplay muted loop>
		  <source src="https://www.w3schools.com/howto/rain.mp4" type="video/mp4">
		</video>
	</div>
	<div class="content">
		<h1><strong>Moving Medicine</strong></h1>
		<p>The ultimate resource to support health care
		professionals to integrate physical activity into
		routine clinical care</p>
		<p><a href="#" class="button dark">Optional link</a></p>
	</div>
</section>

<!-- accordions -->
<section class="section content-section">
	<div class="container thin">

		<!-- accordion / with images -->
		<div class="accordion">
			<div class="title">
				<h4><strong>accordion</strong> with images</h4>
			</div>

			<div class="accordion-item">
				<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
				<div class="content">
					<header><h5>accordion heading</h5></header>
					<main>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</main>
				</div>
				<div class="expander">
					<a href="#">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"></path></svg>
					</a>
				</div>
			</div>

			<div class="accordion-item">
				<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
				<div class="content">
					<header><h5>accordion heading</h5></header>
					<main>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</main>
				</div>
				<div class="expander">
					<a href="#">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"></path></svg>
					</a>
				</div>
			</div>

			<div class="accordion-item">
				<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
				<div class="content">
					<header><h5>accordion heading</h5></header>
					<main>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</main>
				</div>
				<div class="expander">
					<a href="#">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"></path></svg>
					</a>
				</div>
			</div>
		</div>

		<!-- accordion / without images -->
		<div class="accordion">
			<div class="title">
				<h4><strong>accordion</strong> without images</h4>
			</div>

			<div class="accordion-item">
				<div class="content">
					<header><h5>accordion with table as content</h5></header>
					<main>
						<div class="table">
							<div class="table-container">
								<ul class="table-categories">
									<li><a href="#" data-category="home" class="active">Home</a></li>
									<li><a href="#" data-category="travel">Travel</a></li>
									<li><a href="#" data-category="work">Work</a></li>
								</ul>
								<div class="table-items">

									<!-- "home" items -->
									<div data-category="home" class="active">
										<div class="table-item">
											<a href="#">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
												<span>Gardening</span>
											</a>
										</div>
										<div class="table-item">
											<a href="#">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
												<span>Walk the dog</span>
											</a>
										</div>
										<div class="table-item">
											<a href="#">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
												<span>Long thing that takes up two lines</span>
											</a>
										</div>
									</div>

									<!-- "travel" items -->
									<div data-category="travel">
										<div class="table-item">
											<a href="#">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
												<span>Travel Activity 1</span>
											</a>
										</div>
										<div class="table-item">
											<a href="#">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
												<span>Travel Activity 2</span>
											</a>
										</div>
										<div class="table-item">
											<a href="#">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
												<span>Long thing that takes up two lines</span>
											</a>
										</div>
									</div>

									<!-- "travel" items -->
									<div data-category="work">
										<div class="table-item">
											<a href="#">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
												<span>Work Activity 1</span>
											</a>
										</div>
										<div class="table-item">
											<a href="#">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
												<span>Work Activity 2</span>
											</a>
										</div>
										<div class="table-item">
											<a href="#">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
												<span>Long thing that takes up two lines</span>
											</a>
										</div>
									</div>

								</div>
							</div>
						</div>
					</main>
				</div>
				<div class="expander">
					<a href="#">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"></path></svg>
					</a>
				</div>
			</div>

			<div class="accordion-item">
				<div class="content">
					<header><h5>accordion heading</h5></header>
					<main>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</main>
				</div>
				<div class="expander">
					<a href="#">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"></path></svg>
					</a>
				</div>
			</div>

			<div class="accordion-item">
				<div class="content">
					<header><h5>accordion heading</h5></header>
					<main>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</main>
				</div>
				<div class="expander">
					<a href="#">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"></path></svg>
					</a>
				</div>
			</div>
		</div>

		<!-- List / with images -->
		<div class="list">

			<div class="title">
				<h4><strong>List</strong> with images</h4>
			</div>

			<div class="list-item">
				<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
				<div class="content">
					<header>
						<h6>List heading</h6>
					</header>
					<main>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</main>
				</div>
				<div class="link">
					<a class="button" href="#">
						Read more
					</a>
				</div>
			</div>

			<div class="list-item">
				<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
				<div class="content">
					<header>
						<h6>List heading</h6>
					</header>
					<main>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</main>
				</div>
				<div class="link">
					<a class="button" href="#">
						Read more
					</a>
				</div>
			</div>

			<div class="list-item">
				<div class="image round" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
				<div class="content">
					<header>
						<h6>List heading</h6>
					</header>
					<main>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</main>
				</div>
				<div class="link">
					<a class="button" href="#">
						Read more
					</a>
				</div>
			</div>
		</div>

		<!-- List / with numbers -->
		<div class="list">

			<div class="title">
				<h4><strong>List with numbers</strong></h4>
			</div>

			<div class="list-item">
				<div class="number">1</div>
				<div class="content">
					<header>
						<h6>This is an example of numbers</h6>
					</header>
				</div>
			</div>

			<div class="list-item">
				<div class="number">2</div>
				<div class="content">
					<header>
						<h6>This is an example of numbers</h6>
					</header>
				</div>
			</div>

			<div class="list-item">
				<div class="number">3</div>
				<div class="content">
					<header>
						<h6>This is an example of numbers</h6>
					</header>
				</div>
			</div>
		</div>

		<!-- List / without images -->
		<div class="list">

			<div class="title">
				<h4><strong>List</strong> without images</h4>
			</div>

			<div class="list-item">
				<div class="content">
					<header>
						<h6>List heading</h6>
					</header>
					<main>
						<p>This is the content</p>
					</main>
				</div>
				<div class="link">
					<a class="button" href="#">
						Read more
					</a>
				</div>
			</div>

			<div class="list-item">
				<div class="content">
					<header>
						<h6>List heading</h6>
					</header>
					<main>
						<p>This is the content</p>
					</main>
				</div>
				<div class="link">
					<a class="button" href="#">
						Read more
					</a>
				</div>
			</div>

			<div class="list-item">
				<div class="content">
					<header>
						<h6>List heading</h6>
					</header>
					<main>
						<p>This is the content</p>
					</main>
				</div>
				<div class="link">
					<a class="button" href="#">
						Read more
					</a>
				</div>
			</div>
		</div>

	</div>
</section>

<!-- Pathways -->
<section class="section content-section">
	<div class="container thin">
		<div class="pathway">

			<div class="title">
				<h4><strong>Medical Pathway Overview</strong></h4>
			</div>

			<div class="pathway-container">

				<div class="pathway-row">
					<div class="pathway-col">
						<strong>Pathway</strong>
					</div>
					<div class="pathway-col">
						<strong>Admission</strong>
					</div>
					<div class="pathway-col">
						<strong>Inpatient on ward</strong>
					</div>
					<div class="pathway-col">
						<strong>Discharge</strong>
					</div>
				</div>

				<div class="pathway-row">
					<div class="pathway-col">
						<strong>Direct</strong>
					</div>
					<div class="pathway-col">
						<ul>
							<li><a href="#">Physical Activity Calculator</a></li>
							<li><a href="#">1 minute conversation</a></li>
						</ul>
					</div>
					<div class="pathway-col">
						<ul>
							<li><a href="#">Physical Activity Calculator</a></li>
							<li><a href="#">1 minute conversation</a></li>
						</ul>
					</div>
					<div class="pathway-col">
						<ul>
							<li><a href="#">Physical Activity Calculator</a></li>
							<li><a href="#">1 minute conversation</a></li>
						</ul>
					</div>
				</div>

				<div class="pathway-row">
					<div class="pathway-col">
						<strong>Indirect</strong>
					</div>
					<div class="pathway-col">
						<ul>
							<li><a href="#">Physical Activity Calculator</a></li>
							<li><a href="#">1 minute conversation</a></li>
						</ul>
					</div>
				</div>

			</div>

		</div>
	</div>
</section>

<section class="section content-section">
	<div class="container thin">
		<div class="pathway">

			<div class="title">
				<h4><strong>Medical Pathway Overview</strong></h4>
			</div>

			<div class="pathway-container">

				<div class="pathway-row">
					<div class="pathway-col">
						<strong>Pathway</strong>
					</div>
					<div class="pathway-col">
						<strong>Admission</strong>
					</div>
					<div class="pathway-col">
						<strong>Inpatient on ward</strong>
					</div>
					<div class="pathway-col">
						<strong>Discharge</strong>
					</div>
					<div class="pathway-col">
						<strong>Extra</strong>
					</div>
				</div>

				<div class="pathway-row">
					<div class="pathway-col">
						<strong>Direct</strong>
					</div>
					<div class="pathway-col">
						<ul>
							<li><a href="#">Physical Activity Calculator</a></li>
							<li><a href="#">1 minute conversation</a></li>
						</ul>
					</div>
					<div class="pathway-col">
						<ul>
							<li><a href="#">Physical Activity Calculator</a></li>
							<li><a href="#">1 minute conversation</a></li>
						</ul>
					</div>
					<div class="pathway-col">
						<ul>
							<li><a href="#">Physical Activity Calculator</a></li>
							<li><a href="#">1 minute conversation</a></li>
						</ul>
					</div>
					<div class="pathway-col">
						<ul>
							<li><a href="#">Physical Activity Calculator</a></li>
							<li><a href="#">1 minute conversation</a></li>
						</ul>
					</div>
				</div>

				<div class="pathway-row">
					<div class="pathway-col">
						<strong>Indirect</strong>
					</div>
					<div class="pathway-col">
						<ul>
							<li><a href="#">Physical Activity Calculator</a></li>
							<li><a href="#">1 minute conversation</a></li>
						</ul>
					</div>
				</div>

			</div>

		</div>
	</div>
</section>

<!-- Table -->
<section class="section content-section">
	<div class="container thin">

		<div class="table">

			<div class="title">
				<h4><strong>Title of the thing goes here</strong></h4>
			</div>

			<div class="table-container">

				<ul class="table-categories">
					<li><a href="#" data-category="home" class="active">Home</a></li>
					<li><a href="#" data-category="travel">Travel</a></li>
					<li><a href="#" data-category="work">Work</a></li>
				</ul>

				<div class="table-items">

					<!-- "home" items -->
					<div data-category="home" class="active">
						<div class="table-item">
							<a href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
								<span>Gardening</span>
							</a>
						</div>
						<div class="table-item">
							<a href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
								<span>Walk the dog</span>
							</a>
						</div>
						<div class="table-item">
							<a href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
								<span>Long thing that takes up two lines</span>
							</a>
						</div>
					</div>

					<!-- "travel" items -->
					<div data-category="travel">
						<div class="table-item">
							<a href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
								<span>Travel Activity 1</span>
							</a>
						</div>
						<div class="table-item">
							<a href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
								<span>Travel Activity 2</span>
							</a>
						</div>
						<div class="table-item">
							<a href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
								<span>Long thing that takes up two lines</span>
							</a>
						</div>
					</div>

					<!-- "travel" items -->
					<div data-category="work">
						<div class="table-item">
							<a href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
								<span>Work Activity 1</span>
							</a>
						</div>
						<div class="table-item">
							<a href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
								<span>Work Activity 2</span>
							</a>
						</div>
						<div class="table-item">
							<a href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M1.004 5.998l10.996-5.998 10.99 6.06-10.985 5.86-11.001-5.922zm11.996 7.675v10.327l10-5.362v-10.326l-10 5.361zm-2 0l-10-5.411v10.376l10 5.362v-10.327z"/></svg>
								<span>Long thing that takes up two lines</span>
							</a>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
</section>

<!-- Tiles / Full Container -->
<section class="section content-section">
	<div class="container">

		<div class="tiles" data-items="4">

			<div class="title">
				<h4><strong>Optional title</strong></h4>
			</div>

			<div class="items">
				<div class="tile">
					<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
					<div class="content">
						<h6>The title goes here like this</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<div class="link">
						<a href="#" class="button">Optional link</a>
					</div>
				</div>

				<div class="tile">
					<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
					<div class="content">
						<h6>The title goes here like this</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<div class="link">
						<a href="#" class="button">Optional link</a>
					</div>
				</div>

				<div class="tile">
					<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
					<div class="content">
						<h6>The title goes here like this</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<div class="link">
						<a href="#" class="button">Optional link</a>
					</div>
				</div>

				<div class="tile">
					<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
					<div class="content">
						<h6>The title goes here like this</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<div class="link">
						<a href="#" class="button">Optional link</a>
					</div>
				</div>

			</div>

		</div>

	</div>
</section>


<!-- Tiles / Thin Container -->
<section class="section content-section">
	<div class="container thin">

		<div class="tiles" data-items="4">

			<div class="title">
				<h4><strong>Optional title</strong></h4>
			</div>

			<div class="items">
				<div class="tile">
					<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
					<div class="content">
						<h6>The title goes here like this</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<div class="link">
						<a href="#" class="button">Optional link</a>
					</div>
				</div>

				<div class="tile">
					<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
					<div class="content">
						<h6>The title goes here like this</h6>
						<p>Lorem ipsum dolor sit amet!</p>
					</div>
					<div class="link">
						<a href="#" class="button">Optional link</a>
					</div>
				</div>

				<div class="tile">
					<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
					<div class="content">
						<h6>The title goes here like this</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua magna aliqua.</p>
					</div>
					<div class="link">
						<a href="#" class="button">Optional link</a>
					</div>
				</div>

				<div class="tile">
					<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
					<div class="content">
						<h6>The title goes here like this</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<div class="link">
						<a href="#" class="button">Optional link</a>
					</div>
				</div>

			</div>

		</div>

	</div>
</section>

<!-- Tiles / 5 / Full Container -->
<section class="section content-section">
	<div class="container">

		<div class="tiles" data-items="5">

			<div class="title">
				<h4><strong>Optional title</strong></h4>
			</div>

			<div class="items">
				<div class="tile">
					<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
					<div class="content">
						<h6>The title goes here like this</h6>
					</div>
					<div class="link">
						<a href="#" class="button">Optional link</a>
					</div>
				</div>
				<div class="tile">
					<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
					<div class="content">
						<h6>The title goes here like this</h6>
					</div>
					<div class="link">
						<a href="#" class="button">Optional link</a>
					</div>
				</div>
				<div class="tile">
					<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
					<div class="content">
						<h6>The title goes here like this</h6>
					</div>
					<div class="link">
						<a href="#" class="button">Optional link</a>
					</div>
				</div>
				<div class="tile">
					<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
					<div class="content">
						<h6>The title goes here like this</h6>
					</div>
					<div class="link">
						<a href="#" class="button">Optional link</a>
					</div>
				</div>
				<div class="tile">
					<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
					<div class="content">
						<h6>The title goes here like this</h6>
					</div>
					<div class="link">
						<a href="#" class="button">Optional link</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>

<!-- Quote Section -->
<section class="section quote-section">
	<div class="container">

		<div class="quotes">
			<div class="quote">
				<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
				<div class="content">
					<h4><strong>“Moving Medicine is the single most important advance in
					therapeutics in my 50 years in medicine. Every drug or psychological
					prescription for a long-term condition should be partnered by a
					Moving Medicine prescription of activity.”</strong></h4>
					<h4>Muir Gray Kt CBE DSc MD</h4>
				</div>
			</div>
			<div class="quote">
				<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
				<div class="content">
					<h4><strong>“Moving Medicine is the single most important advance in
					therapeutics in my 50 years in medicine.”</strong></h4>
					<h4>Muir Gray Kt CBE DSc MD</h4>
				</div>
			</div>
		</div>

	</div>
</section>

<!-- Split Section -->
<section class="section split-section">
	<div class="split">
		<div class="content">
			<h2><strong>This is a test</strong></h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dapibus dictum lorem sed convallis. Donec feugiat urna id tortor lobortis, ac efficitur massa cursus. Pellentesque non leo augue.</p>
			<p><a href="#" class="button">Find out more</a></p>
		</div>
	</div>
	<div class="split">
		<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
	</div>
</section>

<section class="section split-section wide-content image-left">
	<div class="split">
		<div class="image" style="background-image: url(/wp-content/uploads/2020/03/horse-galloping-in-grass-688899769-587673275f9b584db3a44cdf-scaled.jpg)"></div>
	</div>
	<div class="split">
		<div class="content">
			<h2><strong>This is a test</strong></h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dapibus dictum lorem sed convallis. Donec feugiat urna id tortor lobortis, ac efficitur massa cursus. Pellentesque non leo augue.</p>
			<p><a href="#" class="button">Find out more</a></p>
		</div>
	</div>
</section>

<section class="section split-section split-content">
	<div class="split">
		<div class="content">
			<h2><strong>This is a test</strong></h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dapibus dictum lorem sed convallis. Donec feugiat urna id tortor lobortis, ac efficitur massa cursus. Pellentesque non leo augue.</p>
			<p>Donec feugiat urna id tortor lobortis, ac efficitur massa cursus. Pellentesque non leo augue.</p>
			<p><a href="#" class="button">Find out more</a></p>
		</div>
	</div>
	<div class="split">
		<div class="content">
			<h2><strong>This is a test</strong></h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dapibus dictum lorem sed convallis. Donec feugiat urna id tortor lobortis, ac efficitur massa cursus. Pellentesque non leo augue.</p>
			<p><a href="#" class="button">Find out more</a></p>
		</div>
	</div>
</section>

<!-- WYSIWYG Section -->
<section class="section content-section">
	<div class="container thin">
		<h1>This is H1 title</h1>
		<h2>This is H2 Medical Ward Pathway</h2>
		<h3>This is H3 Outcomes</h3>
		<h4>This is H4 than a third of the UK population now lives to the age of 85, with
			up to half of adults in that group estimated to become frail in the last 10
			years of their lives, and the numbers keep increasing. Frailty is an important
			prognostic indicator for hospital admission, higher care needs, poor quality
			of life and mortality rates.
		</h4>
		<h5>This is H5 Independently living frail patients who are admitted to hospital are
			less likely to recover their mobility, and more likely to need life in the care of
			nursing homes after discharge. Improvements in mobility and balance during
			the first 48 hrs of admission have been associated with greater overall
			functional improvement and shorter recovery times.
		</h5>
		<p>
			<img class="alignleft" src="https://pbs.twimg.com/profile_images/720950127645601793/Vi8FiEuW_400x400.jpg"/>
			This is body text a snapshot analysis over a
			five day period was conducted to identify the
			number of patients who were suitable to
			receive a physical activity intervention (i.e.
			active conversation, goal setting, exercise
			advice with bed/seated/or standing leaflet
			and I CAN tool) and those who actually
			received one. Exclusion criteria were as
			follows; a ‘track & trigger’ score greater than
			1, cognitive impairment, end of life care and patient refused intervention.
		</p>
		<p>We found that the exclusion criteria were sensitive but not specific (excluded suitable patients)</p>

		<p><a href="#" class="button" data-modal="test">
			Click for Modal Window
		</a></p>

		<ul>
			<li>These are bullets. Data also demonstrated that commonly only the Physical Activity Champion was encouraging patients to be physically active.</li>
			<li>Activity was significantly higher on days when the Physical Activity champion was present.</li>
			<li>This suggested that further support was needed to increase the frequency of physica activity conversations on the ward.</li>
		</ul>

		<blockquote>
			85% of patients felt physical activity was important in recovery,
			76% felt that they were encouraged by sta to be more active on
			the ward, with 84% feeling that they wanted to be active on
			discharge following the Active Hospital interventions.
		</blockquote>

		<table>
			<thead>
				<tr>
					<th>Column title</th>
					<th>Column title</th>
					<th>Column title</th>
					<th>Column title</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</td>
					<td>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</td>
					<td>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</td>
					<td>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</td>
				</tr>
				<tr>
					<td>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</td>
					<td>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</td>
					<td>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</td>
					<td>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</td>
				</tr>
			</tbody>
		</table>

	</div>
</section>

<!-- Modal Windows -->
<section class="modal" data-modal-id="test">
	<div class="content">
		<a href="#" data-modal-action="close">
			Close
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"/></svg>
		</a>
		<div class="qrcode">
			<div class="info">
				<h1>Scan this code with your mobile device to download this resource.</h1>
				<p>Or download PDF directly</p>
				<p><a href="#" class="button">Download PDF</a></p>
			</div>
			<div class="code">
				<h5>Inflammatory Rheumatic Disease Workbook for Adults</h5>
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M11 8h-1v-2h1v1h2v1h-1v1h-1v-1zm2 12v-1h-1v1h1zm-1-15v-1h-2v1h1v1h1v-1zm8-1v6h-1v-1h-4v-5h5zm-1 4v-3h-3v3h3zm-14 2h-1v1h2v-1h-1zm0 3h1v1h1v-3h-1v1h-2v2h1v-1zm5 1v2h1v-2h-1zm4-10h-1v3h1v-3zm0 5v-1h-1v1h1zm3-2h1v-1h-1v1zm-10-1h-1v1h1v-1zm2-2v5h-5v-5h5zm-1 1h-3v3h3v-3zm9 5v1h-1v-1h-2v1h-1v-1h-3v-1h-1v1h-1v1h1v2h1v-1h1v2h1v-2h3v1h-2v1h2v1h1v-3h1v1h1v2h1v-1h1v-1h-1v-1h-1v-1h1v-1h-2zm-11 8h1v-1h-1v1zm-2-3h5v5h-5v-5zm1 4h3v-3h-3v3zm12-3v-1h-1v1h1zm0 1h-1v1h-1v-1h-1v-1h1v-1h-2v-1h-1v2h-1v1h-1v3h1v-1h1v-1h2v2h1v-1h1v1h2v-1h1v-1h-2v-1zm-9-3h1v-1h-1v1zm10 2v1h1v1h1v-3h-1v1h-1zm2 4v-1h-1v1h1zm0-8v-1h-1v1h1zm-2-10h4v4h2v-6h-6v2zm-16 4v-4h4v-2h-6v6h2zm4 16h-4v-4h-2v6h6v-2zm16-4v4h-4v2h6v-6h-2z"/></svg>
			</div>
		</div>

	</div>
</section>

<?php get_footer(); ?>