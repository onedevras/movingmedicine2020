<section class="cg-tool content-section cycles-tool">
	<div class="container thin center">

		<div class="cycles-outer">

			<div class="title">
				<h5><?php echo get_field('cycles_title') ?></h5>
			</div>

			<div class="description">
			<?php echo get_field('cycles_description') ?>
			</div>

			<?php if( have_rows('cycles_symptoms_content') ): ?>

			<div class="cycles-container">

				<div class="cycles">

					<div class="cycles-inner-container">

						<div class="cycles-row cycles-impacts">
							<div class="cycles-col">
								<p>Impact of</p>
							</div>
							<div class="cycles-col">
								<a href="#" data-impact="more-activity" class="tool-link active">More activity</a>
							</div>
							<div class="cycles-col">
								<a href="#" data-impact="less-activity" class="tool-link">Less activity</a>
							</div>
						</div>

						<div class="cycles-row">
							<div class="cycles-col">
								<p>on</p>
							</div>
							<div class="cycles-col">
								<div class="cycles-row nested">
								<?php $active = 'active'; ?>
								<?php while ( have_rows('cycles_symptoms_content') ) : the_row(); ?>
									<div class="cycles-col">
										<?php 
											$symptom_field = get_sub_field('cycle_symptom');
											// _one_log($symptom_field);
											// preg_match('/^(.*?) \[ev/', $symptom_field, $matches); 
											$symptom_name = slugify($symptom_field);
										?>
										<a href="#" class="tool-link <?php echo $active; ?>" data-symptom="<?php echo $symptom_name ?>"><?php the_sub_field('cycle_symptom'); ?></a></li>
										<?php $active = ''; ?>
									</div>
								<?php endwhile; ?>
								</div>
							</div>
						</div>

						<div class="cycles-row">
							<div class="cycles-col">
								<p>is</p>
							</div>

							<div class="cycles-col steps">
						<?php $active = 'class="active"'; ?>
						<?php while ( have_rows('cycles_symptoms_content') ) : the_row(); ?>
							<?php 
								$more_activity = get_sub_field('more_activity');
								$less_activity = get_sub_field('less_activity');
								$symptom_field = get_sub_field('cycle_symptom');
								// preg_match('/^(.*?) \[ev/', $symptom_field, $matches); 
								$symptom_name = slugify($symptom_field);
								// Make sure there's no empty repeaters (probab;y a better way of doing this)
								if( $more_activity && !$more_activity[ count($more_activity )-1]['step'] )
									array_pop($more_activity);
								if( $less_activity && !$less_activity[ count($less_activity)-1 ]['step'] )
									array_pop($less_activity);
							?>
								<ul <?php echo $active; ?> data-symptom="<?php echo $symptom_name; ?>" data-impact="more-activity">
								<?php if ( $more_activity ) :?>
								<?php foreach ( $more_activity as $step): ?>
									<li class="tool-item step"><?php echo $step['step']; ?></li>
								<?php endforeach; ?>
								<?php $active = ''; ?>
								<?php endif; ?>
								</ul>

								<ul data-symptom="<?php echo $symptom_name ?>" data-impact="less-activity">
								<?php if ( $less_activity ) :?>
								<?php foreach ( $less_activity as $step): ?>
									<li class="tool-item step"><?php echo $step['step']; ?></li>
								<?php endforeach; ?>
								<?php endif; ?>
								</ul>

						<?php endwhile; ?>
							</div>
						</div>

					</div>

				</div>

			</div>
		</div>

		<?php endif; ?>

	</div>
</section>