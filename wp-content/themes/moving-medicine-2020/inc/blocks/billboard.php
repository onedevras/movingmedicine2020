<?php 

$bg_video = get_field('video');
$modal_video = get_field('modal_video'); 
if ( $modal_video ) {
	$guid = uniqid();
	$params = array(
		'controls' => 1,
		'hd' => 1,
		'autoplay' => 0,
		'loop' => 0,
		'mute' => 0,
	);
	$modal_video = get_video($modal_video, $params, $guid);
}

?>

<!-- Billboard --> 
<section class="section billboard-section full-width <?php echo $bg_video ? 'has-video' : 'no-video' ?>">
	<?php if( get_field('image') ): ?>
	<div class="image" style="background-image: url(<?php echo get_field('image')['url']; ?>)"></div>
	<?php endif; ?>
	<?php 
	// Get the background video
	if ( $bg_video ) {
		$params = array(
			'controls' => 0,
			'hd' => 1,
			'autoplay' => 1,
			'loop' => 1,
			'mute' => 1,
			'muted' => 1,
			'title' => get_field('title') ?: false
		);
		the_video($bg_video, $params);
	}

	?>
	<?php if( get_field('title') || get_field('description') || get_field('link') ): ?>
	<div class="content">
		<?php if( get_field('title') ): ?>
		<div class="heading">
			<h1><?php the_field('title'); ?></h1>
		</div>
		<?php endif; ?>
		<?php if( get_field('description') ): ?>
			<?php the_field('description') ?>
		<?php endif; ?>
		<?php if( ($the_link = get_field('link')) || $modal_video ): ?>
		<p><a href="<?= $the_link ?: '#'; ?>" class="button dark" <?php if($modal_video) echo "data-modal='qr-$guid'";?>><?php the_field('link_text'); ?></a></p>
		<?php endif; ?>
	</div>
<?php endif; ?>
	<?php if (get_field('social_buttons', $post_id)) : ?>
		<div class="share">
			<span>Share this</span>
			<?php get_social_share_buttons($post_id); ?>
		</div>
	<?php endif; ?>
</section>
<?php 
if ( $modal_video ):
	$args = array(
		'guid' => $guid,
		'modal_video' => $modal_video,
		'title' => get_field('title') ?: false
	);
	one_get_content('content-parts', 'video-modal', $args);
endif; 
?>