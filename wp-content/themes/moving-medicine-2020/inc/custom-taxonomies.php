<?php

function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Patient Types.
	 */

	$labels = [
		"name" => __( "Patient Types", "custom-post-type-ui" ),
		"singular_name" => __( "Patient Type", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Patient Types", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'consultation-guides/conditions', 'with_front' => false, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "patient",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		];
	register_taxonomy( "patient", [ "condition" ], $args );

	/**
	 * Taxonomy: Types.
	 */

	$labels = [
		"name" => __( "Types", "custom-post-type-ui" ),
		"singular_name" => __( "Type", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Types", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'slide_extra_type', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "slide_extra_type",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		];
	register_taxonomy( "slide_extra_type", [ "slide_extra" ], $args );

	/**
	 * Taxonomy: Subjects.
	 */

	$labels = [
		"name" => __( "Subjects", "custom-post-type-ui" ),
		"singular_name" => __( "Subject", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Subjects", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'subject', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "subject",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		];
	register_taxonomy( "subject", [ "resource" ], $args );

	/**
	 * Taxonomy: Types.
	 */

	$labels = [
		"name" => __( "Types", "custom-post-type-ui" ),
		"singular_name" => __( "Type", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Types", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'organisation_type', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "organisation_type",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		];
	register_taxonomy( "organisation_type", [ "organisation" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes' );

