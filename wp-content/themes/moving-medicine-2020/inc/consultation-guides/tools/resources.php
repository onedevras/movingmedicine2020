<section class="cg-tool content-section resources-tool">
	<div class="container thin center">
		<div class="resources-container">

			<div class="title">
				<h5><?php echo get_field('resources_title'); ?></h5>
			</div>

			<div class="description">
				<?php echo get_field('resources_description'); ?>
			</div>

			<div class="resources-items">

				<?php $resources = get_field('resources'); ?>
				<?php foreach($resources as $resource): ?>

					<div class="resources-item">
					<?php
						$guid = uniqid();
						$blocks = parse_blocks($resource->post_content);
						foreach($blocks as $block):
							if( 'acf/mm-patient-information' === $block['blockName'] ):
								$block_data = $block['attrs']['data'];
								$title = get_the_title( $resource );
								$description = autop($block_data['description']);
								$link_text = '';
								if( '1' === $block_data['is_external'] ) {
									$url = $block_data['url'];
									$hide_link = true;
								}
								else {
									$file_id = $block_data['file'];
									$url = wp_get_attachment_url( $file_id );
									$link_text = strtoupper( wp_check_filetype( $url )['ext'] );
								}
							endif;
						endforeach;
					?>
						<h5><?php echo $title; ?></h5>
						
						<div class="content">
							<?php echo $description; ?>
						</div>
						<?php if($url): ?>
						<div class="link-row">
							<?php if( !$hide_link ): ?>
							<div class="link">
								<a class="button" target="_blank" href="<?php echo $url; ?>"><?php echo $link_text ?: 'Visit'; ?></a>
							</div>
							<? endif; ?>
							<div class="link">
								<a class="button" href="#" data-modal="qr-<?php echo $guid; ?>">QR code</a>
							</div>
						</div>
						<section class="modal resources-modal" data-modal-id="qr-<?php echo $guid; ?>">
							<div class="content">
								<a href="#" data-modal-action="close">
									Close
									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"></path></svg>
								</a>
								<div class="inner">
									<div class="info">
										<h1>Scan this code with your mobile device to get the resource.</h1>
										<p>Or click below to access it directly</p>
										<p><a href="<?php echo $url; ?>" class="button"><?php echo $link_text ?: 'Visit'; ?></a></p>
									</div>
									<div class="code">
										<h5><?php echo $title; ?></h5>
										<?= do_shortcode("[su_qrcode data='$url' title='' size='1000' margin='0' align='none' link='$url' target='blank' color='#333333' background='#ffffff' class='qrcode-image']"); ?>
									</div>
								</div>
							</div>
						</section>
						<?php endif; ?>
					</div>

				<?php endforeach; ?>

			</div>
		</div>
	</div>
</section>