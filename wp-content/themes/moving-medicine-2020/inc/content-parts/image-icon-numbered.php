<?php
if( !isset($args['icon_url']) && $args['icon'] ) {
  $args['icon_url'] = $args['icon']['url'];
}

?>
<?php if( $args['numbered'] ): ?>
  <div class="number"><?php echo $args['count'] ?></div>
<?php elseif( $args['icon_url'] && 'svg' === wp_check_filetype( $args['icon_url'] )['ext'] ): ?>
  <div class="image svg"><?php echo get_svg_from_url($args['icon_url']); ?></div>
<?php  elseif( $args['icon_url'] ): ?>
  <div class="image <?php echo $args['icon_round'] ?: ''; ?>" style="background-image: url('<?php echo $args['icon_url']; ?>'); background-color: transparent;"></div>
<?php  elseif( $args['placeholder'] ): ?>
  <div class="image <?php echo $args['icon_round'] ?: ''; ?>"></div>
<?php endif; ?>