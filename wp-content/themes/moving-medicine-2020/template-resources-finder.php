<?php /* Template Name: Finder: Resource */ ?>
<?php get_header(); ?>

<?php 
function mm_body_classes_for_resources_finder($classes) {

  $classes[] = 'consultation-guide';

  return $classes;
}
add_filter('body_class', 'mm_body_classes_for_resources_finder');
?>

<?php one_get_content('content-parts', 'article-header'); ?>

  <div class="finder_outer-container resource-finder" id="main" data-levels="2">
    <div class="container">
      <div class="finder_inner">
        <?php 
        if( false === ( $finder = get_transient('resource_finder_list') ) ):
          $finder = build_resource_finder_list();
        endif;
        if($finder): ?>
          <dl>
            <?php array_walk($finder, 'finder_from_list'); ?>
          </dl>
        <?php else: ?>
          <p>No resources were found.</p>
        <?php endif; ?>
      </div>
    </div>
  </div>

<?php get_footer(); ?>