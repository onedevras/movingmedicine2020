<?php

$text_1 = get_field('text_1');
$text_2 = get_field('text_2');
$container_classes = array();

if ($text_1['text']) :
  $type = get_field('type');
  if ('text-image' === $type) {
    $container_classes[] = get_field('size');
    $container_classes[] = get_field('image-right') ? 'image-right' : 'image-left';
    $image_classes = array("image");
    $image_settings = get_field('image_settings');
    if($image_settings['image']) {
      $image = $image_settings['image']['url'];
      $image_classes[] = $image_settings['image_size'] ? 'cover' : '';
      $container_classes[] = $image_settings['image_size'] ? 'full-bleed-image' : '';
      $image_classes[] = $image_settings['image_size'] ? $image_settings['image_align'] : '';
    }
    
  } else {
    $container_classes[] = 'split-content';
  }

  $container_classes[] = get_field('background') ?: 'white';

?>

  <!-- Split Section -->
  <section class="section full-width split-section <?php echo implode(' ', $container_classes ) ?>">
    <?php if ('text-image' === $type) : ?>
      <div class="split">
        <div class="<?php echo implode(' ', $image_classes); ?>" style="background-image: url('<?php echo $image; ?>')"></div>
      </div>
    <?php endif; ?>
    <div class="split">
      <div class="content">
        <?php if( $text_1['title'] ): ?>
          <h2><?php echo $text_1['title']; ?></h2>
        <?php endif; ?>
        <?php echo $text_1['text']; ?>
        <?php if($text_1['buttons']):
          foreach ($text_1['buttons'] as $button) : ?>
          <?php if ($button['url']) : ?>
            <?php $classes = array('button'); ?>
            <?php if( $button['cta'] ) $classes[] = 'cta'; ?>
            <p><a href="<?php echo $button['url'] ?>" class="<?= implode(' ', $classes); ?>"><?php echo $button['text'] ?: 'Read more' ?></a></p>
          <?php endif; ?>
        <?php endforeach;
        endif; ?>
      </div>
    </div>
    <?php if ('text-image' !== $type && $text_2['text']) : ?>
      <div class="split">
        <div class="content">
          <?php if( $text_2['title'] ): ?>  
            <h2><?php echo $text_2['title']; ?></h2>
          <?php endif; ?>
          <?php echo $text_2['text']; ?>
          <?php if($text_2['buttons']):
            foreach ($text_2['buttons'] as $button) : ?>
            <?php if ($button['url']) : ?>
              <?php $classes = array('button'); ?>
              <?php if( $button['cta'] ) $classes[] = 'cta'; ?>
              <p><a href="<?php echo $button['url'] ?>" class="<?= implode(' ', $classes); ?>"><?php echo $button['text'] ?: 'Read more' ?></a></p>
            <?php endif; ?>
          <?php endforeach;
          endif; ?>
        </div>
      </div>
    <?php endif; ?>
  </section>

<?php endif; ?>