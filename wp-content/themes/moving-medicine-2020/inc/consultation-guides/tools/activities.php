<section class="cg-tool content-section activities-tool">
	<div class="container thin center">

		<div class="table activities-table">

			<div class="title">
				<h5><?php echo get_field('activities_title') ?></h5>
			</div>

			<?php
			if (have_rows('activities_content')) : the_row();
				$active = 'class="active"';
				$areas = get_field('activities_content');
			?>

				<div class="table-container activities-container">
					<ul class="table-categories activities-list">

						<?php foreach ($areas as $area) : ?>
							<li class="tool-item"><a href="#" data-category="<?php echo slugify($area['title']); ?>" <?php echo $active; ?>><?php echo $area['title']; ?></a></li>
							<?php $active = ''; ?>
						<?php endforeach; ?>

					</ul>
					<div class="table-items">

						<?php $content = get_field('activities_content') ?>
						<?php $active = 'class="active"'; ?>

						<?php foreach ($areas as $area) : ?>
							<?php $activities = $area['activities']; ?>

							<div data-category="<?php echo slugify($area['title']); ?>" <?php echo $active; ?>>

								<?php foreach ($activities as $activity) : ?>

									<div class="table-item">
										<?php if($activity['icon']): ?>
										<?php echo get_svg_from_url($activity['icon']['url']); ?>
										<?php endif; ?>
										<span><?php echo $activity['activity']; ?></span>
									</div>

									<?php $active = ''; ?>
								<?php endforeach; ?>

							</div>

						<?php endforeach; ?>

					</div>
				</div>

			<?php endif; ?>

		</div>

	</div>
</section>