<?php

$list_item['icon_type'] = wp_check_filetype( get_field('icon')['url'] )['ext'];
$list_item['icon_round'] = get_field('icon_round') ? 'circular' : '';
$list_item['icon_url'] = get_field('icon')['url'];
$list_item['title'] = get_the_title( $post_id );
$list_item['description'] = get_field('description');
$list_item['url'] = get_field('url');

one_get_content('content-parts', 'list', [ $list_item ]);

?>
