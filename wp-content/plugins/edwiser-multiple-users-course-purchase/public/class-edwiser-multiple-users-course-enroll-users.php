<?php

namespace app\wisdmlabs\edwiserBridge\BulkPurchase;

use app\wisdmlabs\edwiserBridge as edwiserBridge;

/**
 * The public-facing functionality of enroll user.
 * @author     WisdmLabs, India <support@wisdmlabs.com>
 */
class EdwiserMultipleCourseEnrollUsers
{

    public function __construct()
    {
        /*add_action('wp_ajax_get_user_bulk_course_details', array($this, 'wdmEnrolledUsers'));
        add_action('wp_ajax_get_enrol_user_details', array($this, 'wdmEnrollUserDetails'));
        add_action('wp_ajax_get_enrol_user_course', array($this, 'wdmEnrollUserCourse'));
        add_action('wp_ajax_edit_user', array($this, "wdmEditUser"));
        add_action('wp_ajax_ebbp_add_to_cart', array($this, "wdmAddToCart"));
        add_action('wp_ajax_ebbp_add_quantity', array($this, 'wdmAddMoreQuantity'));
        add_action('wp_ajax_ebbp_add_new_product', array($this, 'wdmAddNewProductToGroup'));
        add_action('wp_ajax_ebbp_edit_cohort_name', array($this, 'ebEditCohortNameFromCohortId'));
        */
    }



    /**
     * functionality to change the coohort name from enroll-student page.
     * @return [type] [description]
     */
    public function ebEditCohortNameFromCohortId()
    {
        global $wpdb;
        if (isset($_POST["mdl_cohort_id"]) && !empty($_POST["mdl_cohort_id"]) && isset($_POST["mdl_cohort_name"]) && !empty($_POST["mdl_cohort_name"])) {
            $cohortId = $_POST["mdl_cohort_id"];
            $name = $_POST["mdl_cohort_name"];

            $tblCohortInfo = $wpdb->prefix . 'bp_cohort_info';

            $wpdb->update(
                $tblCohortInfo,
                array(
                    "NAME" => $name
                ),
                array(
                    "MDL_COHORT_ID" => $cohortId
                )
            );
            wp_send_json_success(__("Group name successfully changed.", "ebbp-textdomain"));
        } else {
            wp_send_json_error(__("Unable to update group name.", "ebbp-textdomain"));
        }
    }




    /**
     * returns the enrolled users list
     * @return [type] [description]
     */
    public function wdmEnrolledUsers()
    {
        if (isset($_POST['mdl_cohort_id']) && !empty($_POST['mdl_cohort_id'])) {
            $mdlCohortId = $_POST['mdl_cohort_id'];
            $cuser_id = get_current_user_id();
            $avail_seats = 0;
            global $wpdb;
            //v1.1.1
            $tableName = $wpdb->prefix . "bp_cohort_info";
            $query = $wpdb->prepare("SELECT PRODUCTS FROM $tableName WHERE mdl_cohort_id = %d", $mdlCohortId);
            $result = $wpdb->get_var($query);
            $products = unserialize($result);
            $avail_seats = min($products);
            if ($avail_seats==null) {
                $avail_seats=0;
            }
            $tbl_name = $wpdb->prefix . 'moodle_enrollment';

            /**
             * Fixed #32173 - Backward compatibility v1.0.0
             * @author Pandurang
             * @since 1.0.1
             */
            $query = $wpdb->prepare(
                "SELECT DISTINCT `user_id` FROM `{$tbl_name}` WHERE `enrolled_by` = %d AND `mdl_cohort_id` = '%d'",
                $cuser_id,
                $mdlCohortId
            );

            $enrolled_users = $wpdb->get_results($query);

            $tableColumns = array(
                // "id"        => __("ID", 'ebbp-textdomain'),
                'cb'        => '<input type="checkbox" id="ebbp_enroll_student_cb_head">', 
                "firstname" => __("FirstName", 'ebbp-textdomain'),
                "lastname"  => __("LastName", 'ebbp-textdomain'),
                "email"     => __("Email Id", 'ebbp-textdomain'),
                "role"      => __("Role", 'ebbp-textdomain'),
                "progress"  => __("Progress", 'ebbp-textdomain'),
                "actions"   => __("Actions", 'ebbp-textdomain')
            );

            $tableColumns = apply_filters("eb_bp_enroll_user_tbl_header", $tableColumns);

            ob_start();
            ?>
            <!-- <label>
                /*_e('Enrolled Users:', 'ebbp-textdomain');
                echo " <span> ".count($enrolled_users). "</span>";*/
            </label> -->



            <table id='enroll-user-table'>
                <thead>
                    <tr>
                    <?php

                    foreach ($tableColumns as $tableHeader) {
                        echo "<th>" . $tableHeader . "</th>";
                    }

                    ?>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($enrolled_users)) {
                        foreach ($enrolled_users as $userId) {
                            $user_data = get_userdata($userId->user_id);
                            $userRole = $this->getUserRole($user_data);


                            $tableData = array(
                                // "id"        => $userId->user_id,
                                'cb'        => '<input data-id="'.$userId->user_id.'" type="checkbox" class="ebbp_ebroll-students_cb">',
                                "firstname" => $user_data->first_name,
                                "lastname"  => $user_data->last_name,
                                "email"     => $user_data->user_email,
                                "role"      => $userRole,
                                "progress"  => '<a> <span data-cohortid = '.$mdlCohortId.' data-userid = '.$userId->user_id.' class="ebbp_course_progress">' . __("View Progress", "ebbp-custom-field-textdomain") . "</span> </a>",
                                "actions"   => '<i id="'.$userId->user_id.'" class="fa fa-pencil-square-o edit-enrolled-user"></i>
                                    <i id="'.$userId->user_id.'" class="fa fa-trash bp-delete-enrolled-user"></i>'
                            );

                            $tableData = apply_filters("eb_bp_enroll_user_tbl_data", $tableData, $mdlCohortId, $userId->user_id);

                            ?>
                            <tr class ="<?php echo $userId->user_id; ?>">

                            <?php
                            //Here foreach is on table columns because table header should get displayed according to the array values.
                            foreach ($tableColumns as $key => $value) {
                                $value = $value;
                                echo "<td>" . $tableData[$key] . "</td>";
                            }

                            ?>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <?php
                    $form = ob_get_clean();
                    $associatedCourses = $this->wdmEnrollUserCourse($mdlCohortId);
                    $responce = array('seats' => $avail_seats, "enrolled_users" => count($enrolled_users), 'html' => $form, "asso_courses" => $associatedCourses);
                    wp_send_json_success($responce);
        } else {
            wp_send_json_error(__("Invalid request data"));
        }
    }


    public function set_enroll_students_removed_users_data($cohort_id, $enrollmentErr, $enrollmentSuc, $enrolled_user)
    {

        $existing_data = get_option('removed_users_'.$cohort_id);

        if (isset($existing_data) && !empty($existing_data)) {
            $enrollmentErr = $existing_data['enrollment_err'] . $enrollmentErr;
            $enrollmentSuc = $existing_data['enrollment_suc'] . $enrollmentSuc;

            $existing_data['enrolled_user'] = isset($existing_data['enrolled_user']) && !empty($existing_data['enrolled_user']) ? $existing_data['enrolled_user'] : array();

            $enrolled_user = array_merge($existing_data['enrolled_user'], $enrolled_user);
        }

        update_option('removed_users_'.$cohort_id, array('enrollment_err' => $enrollmentErr, 'enrollment_suc' => $enrollmentSuc, 'enrolled_user' => $enrolled_user));
    }

    public function get_enroll_students_removed_users_data($cohort_id)
    {

        // return get_option($user_id .'_'.$cohort_id);
        $data = get_option('removed_users_'.$cohort_id);

        delete_option('removed_users_'.$cohort_id);

        return $data;

    }



    public function bp_delete_multiple_enrolled_user()
    {
        if (isset($_POST["action"]) && !empty($_POST["action"]) == "bp_delete_enrolled_user" && isset($_POST["userId"]) && !empty($_POST["userId"]) && isset($_POST["cohortId"]) && !empty($_POST["cohortId"])) {
            $users_arr      = $_POST['userId'];
            $cohort_id      = $_POST['cohortId'];
            $success_arr    = '';
            $error_arr      = '';
            $msg            = '';
            $enrolled_user  = array();
            $processed_users     = count($users_arr);


            foreach ($users_arr as $single_user_id) {
                $result = $this->bp_remove_user_from_cohort($single_user_id, $cohort_id);
                $user_info = get_userdata($single_user_id);

                if ($result["status"]) {
                    array_push($enrolled_user, $single_user_id);
                    $success_arr .=  '<li>'.$user_info->user_email.'</li>' ;

                } else {
                    $error_arr .= '<li>'.$user_info->user_email .'</li>';
                }
            }


            if ($_POST['total'] == ($_POST['processed_users'] + $processed_users)) {
                $this->set_enroll_students_removed_users_data( $cohort_id, $error_arr, $success_arr, $enrolled_user);
                $saved_response = $this->get_enroll_students_removed_users_data($cohort_id);

                $error_arr    = $saved_response['enrollment_err'];
                $success_arr  = $saved_response['enrollment_suc'];
                $enrolled_user = $saved_response['enrolled_user'];

                if (!empty($success_arr)) {
                    $msg .= '<div class="wdm_success_message">
                                <i class="fa fa-times-circle wdm_success_msg_dismiss"></i>
                                    <span class="wdm_enroll_warning_message_lable">
                                    '.__("User with email id  ", "ebbp-textdomain").'
                                        <ul>
                                            '. $success_arr .'
                                        </ul>
                                        '.__(" have been removed successfully", "ebbp-textdomain").'
                                    </span>
                            </div>';
                }

                if (!empty($error_arr)) {
                    $msg .= '<div class="wdm_error_message">
                                <i class="fa fa-times-circle wdm_success_msg_dismiss"></i>
                                    <span class="wdm_enroll_warning_message_lable">
                                        '.__("Sorry, unable to delete user with email id ", "ebbp-textdomain").'
                                        <ul>
                                            '. $error_arr .'
                                        </ul>
                                    </span>
                            </div>';
                }
            } else{

                $this->set_enroll_students_removed_users_data($cohort_id, $error_arr, $success_arr, $enrolled_user);
            }


            wp_send_json_success(
                array(
                    "status"         => 1,
                    "qty"            => count($enrolled_user),
                    "msg"            => $msg,
                    'enrolled_user'  => $enrolled_user,
                    'processed_users' => $processed_users,
                )
            );


        }


    }






    public function bp_remove_user_from_cohort($user_id, $cohort_id)
    {
        $current_user_id = get_current_user_id();
        $cohortManager = new BPCohortManageUser();
        $result = $cohortManager->deleteUserFromCohort($user_id, $cohort_id, $current_user_id);
        return $result;
        // $user_info = get_userdata($_POST["userId"]);
    }





    public function bp_delete_single_enrolled_user()
    {
        if (isset($_POST["action"]) && !empty($_POST["action"]) == "bp_delete_enrolled_user" && isset($_POST["userId"]) && !empty($_POST["userId"]) && isset($_POST["cohortId"]) && !empty($_POST["cohortId"])) {

            $user_id = $_POST['userId'];
            $cohort_id = $_POST['cohortId'];

            //Call the user remove function.
            $result = $this->bp_remove_user_from_cohort($user_id, $cohort_id);

            $user_info = get_userdata($user_id);

            if ($result["status"]) {
                $msg = '<div class="wdm_success_message">
                            <i class="fa fa-times-circle wdm_success_msg_dismiss"></i>
                            <span class="wdm_enroll_warning_message_lable">
                            '.__("User with email id  ", "ebbp-textdomain"). $user_info->user_email .__(" have been removed successfully", "ebbp-textdomain").'
                            </span>
                        </div>';

                wp_send_json_success(
                    array(
                        "status" => $result["status"],
                        "qty"    => $result["qty"],
                        "msg"    => $msg
                    )
                );
            } else {
                $msg = '<div class="wdm_error_message">
                            <i class="fa fa-times-circle wdm_success_msg_dismiss"></i>
                            <span class="wdm_enroll_warning_message_lable">
                            '.__("Sorry, unable to delete user with email id ", "ebbp-textdomain"). $user_info->user_email .'
                            </span>
                        </div>';
                wp_send_json_error(array("msg" => $msg));
            }
        }
    }



    public function bpDeleteCohortFromFrontend()
    {
        if (isset($_POST["action"]) && !empty($_POST["action"]) == "bp_delete_cohort" && isset($_POST["cohortId"]) && !empty($_POST["cohortId"])) {
            $cohortId = $_POST["cohortId"];

            // unenroll all the users from the cohort.
            $cohortUserManager = new BPCohortManageUser();
            $result = $cohortUserManager->deleteAllUsersFromCohort($cohortId);

            //Delete Cohort from wordopress as well as moodle
            $cohortManger = new BPManageCohort();
            $result = $cohortManger->deleteCohort(array($cohortId));

            if ($result) {
                wp_send_json_success($result);
            } else {
                wp_send_json_error(__("Invalid request data"));
            }
        }
    }




    /**
     * function to show all users
     * @param  [type] $user [description]
     * @return [type]       [description]
     */
    public function getUserRole($user)
    {
        $userRole = $user->roles;
        $str = "";
        $count = count($userRole);
        for ($i = 0; $i < $count; $i++) {
            if ($i == 0) {
                if ($userRole[$i] == 'non_editing_teacher') {
                    $str .= "Non Editing Teacher";
                } else {
                    $str .= $userRole[$i];
                }
            } else {
                if ($userRole[$i] == 'non_editing_teacher') {
                    $str .= ", Non Editing Teacher";
                } else {
                    $str .= ", " . $userRole[$i];
                }
            }
        }
        return $str;
    }

    public function wdmEnrollUserDetails()
    {
        $uid = trim($_POST["uid"]);
        $user_data = get_userdata($uid);
        $first_name = $user_data->first_name;
        $last_name = $user_data->last_name;
        $Email = $user_data->user_email;
        $roles = $user_data->roles;

        foreach ($roles as $role) {
            if ($role = "subscriber") {
                $userRole = "student";
                break;
            } else {
                $userRole = "Manager";
            }
        }

        echo json_encode(array("FirstName" => $first_name, "lastname" => $last_name, "email" => $Email, "role" => $userRole));
        die();
    }

    public function wdmEnrollUserCourse($mdlCOhortId)
    {
        // $mdlCOhortId = $_POST['mdl_cohort_id'];
        if (isset($mdlCOhortId) && !empty($mdlCOhortId)) {
            global $wpdb;
            $tableName = $wpdb->prefix . "bp_cohort_info";
            $query = $wpdb->prepare("SELECT PRODUCTS, NAME, COHORT_NAME FROM $tableName WHERE MDL_COHORT_ID = %d;", $mdlCOhortId);
            $result = $wpdb->get_row($query, ARRAY_A);
            $product = $result['PRODUCTS'];
            // $cohortName = $result['COHORT_NAME'];
            $product = unserialize($product);
            ob_start();
            ?>
            <div id="wdm-asso-course-accordian" class="wdm-coho-asso-corses wdm-dialog-scroll">
                <ol type="1">
                    <?php
                    foreach ($product as $key => $value) {
                        $value = $value;
                        $tbl_name = $wpdb->prefix . "woo_moodle_course";
                        $query = $wpdb->prepare(
                            "SELECT DISTINCT `moodle_post_id` FROM `{$tbl_name}` WHERE `product_id` = %d ",
                            $key
                        );
                        $courses = $wpdb->get_col($query);
                        $productName = get_the_title($key);
                        ?>

                            <li class="wdm_enrol-studnets_products"> <?php echo $productName; ?></li>
                                <div class = "wdm_productwise_course">
                                    <ol type="a">
                                    <?php
                                    foreach ($courses as $course) {
                                        $courseInfo = get_post($course);
                                        $title = $courseInfo->post_title;
                                        ?>
                                            <li><?php echo $title; ?></li>
                                        <?php
                                    }
                                    ?>
                                    </ol>
                                </div>
                        <?php
                    }
                    ?>
                </ol>
            </div>
            <?php
            $responce = ob_get_clean();
            // $current_user =  wp_get_current_user();
            // $cohortName =  isset($result["NAME"]) ? $result["NAME"] : str_replace($current_user->user_login."_", "", html_entity_decode($cohortName));
            return $responce;
            // wp_send_json_success(array('html' => $responce, 'cohortName' => $cohortName));
        } else {
            return __("Invalid request parameters", "ebbp-textdomain");
            // wp_send_json_error(__("Invalid request parameters", 'ebbp-textdomain'));
        }
    }

    /**
     * Callback to update the user on wordpress and moodle.
     */
    public function wdmEditUser()
    {
        $uid = $this->checkData($_POST, "uid");
        $fnane = $this->checkData($_POST, "firstname");
        $lname = $this->checkData($_POST, "lastname");
        $email = $this->checkData($_POST, "email");
        if ($uid && $fnane && $lname && $email) {
            $userData = array(
                "ID" => $uid,
                "first_name" => $fnane,
                "last_name" => $lname,
                "user_email" => $email,
            );

            /**
             * Update user on wordpress.
             */
            $updateUser = wp_update_user($userData);
            if (is_wp_error($updateUser)) {
                wp_send_json_error($this->prepareResMsg($updateUser->get_error_message(), true));
            } else {
                $moodleUserId = get_user_meta(trim($_POST["uid"]), 'moodle_user_id', true);
                $user_data = array(
                    "id" => $moodleUserId,
                    "firstname" => $fnane,
                    "lastname" => $lname,
                    "email" => $email
                );
                /**
                 * Update user data on moodle.
                 */
                $mdlUserUpdated = edwiserBridge\edwiserBridgeInstance()->userManager()->createMoodleUser($user_data, 1);
                if ($mdlUserUpdated['user_updated'] == 1) {
                    wp_send_json_success($this->prepareResMsg(__("User data has been updated successfully.", 'ebbp-textdomain'), false));
                } else {
                    wp_send_json_error($this->prepareResMsg(__("Failed to update user on moodle.", 'ebbp-textdomain'), true));
                }
            }
        } else {
            wp_send_json_error($this->prepareResMsg(__("User data is inappropriate.", 'ebbp-textdomain'), true));
        }
    }

    private function prepareResMsg($msg, $isError = false)
    {
        ob_start();
        if (!$isError) {
            ?>
            <div class="wdm_success_message wdm_user_list">
                <i class="fa fa-times-circle wdm_success_msg_dismiss"></i>
                <span class="wdm_enroll_warning_message_lable">
                    <?php echo $msg; ?>
                </span>
            </div>
            <?php
        } else {
            ?>
            <div class="wdm_error_message wdm_user_list">
                <i class="fa fa-times-circle wdm_error_msg_dismiss"></i>
                <span class="wdm_enroll_warning_message_lable">
                    <?php echo $msg; ?>
                </span>
            </div>
            <?php
        }
        return ob_get_clean();
    }

    private function checkData($array, $key)
    {
        if (isset($array[$key]) && !empty($array[$key])) {
            return $array[$key];
        }
        return false;
    }

    public function wdmAddToCart()
    {
        $session_data = array();
        if (WC()->session->get('add_product_from_enroll_page')) {
            $session_data = WC()->session->get('add_product_from_enroll_page');
        }

        if (isset($_REQUEST["mdl_cohort_id"]) && !empty($_REQUEST["mdl_cohort_id"]) && isset($_REQUEST['productQuantity']) && !empty($_REQUEST['productQuantity'])) {
            global $woocommerce;
            $checkoutUrl = "#";
            $cohortId = $_REQUEST['mdl_cohort_id'];

            $cohortDetails = array("cohort_id" => $_REQUEST['mdl_cohort_id']);

            // $session_data['cohortId'] = $_REQUEST['mdl_cohort_id'];
            $flag = 0;
            foreach ($_REQUEST['productQuantity'] as $value) {
                if ($value <= 0) {
                    $flag = 1;
                }
            }
/*            $wcSession = [];
            $wcSession ["cohortId"] = $cohortId;*/
            if (!$flag) {
                foreach ($_REQUEST['productQuantity'] as $prodId => $qty) {
                    $cohortDetails["product_id"] = $prodId;
                    $cohortDetails["quantity"] = $qty;
                    // $session_data[$prodId] = $qty;
                    $woocommerce->cart->add_to_cart($prodId, $qty, "", array(), array('cohort_id'=>$cohortId,'wdm_edwiser_self_enroll'=>'on','Group Enrollment'=> 'yes', 'enroll-students' => 'yes'));
                    array_push($session_data, $cohortDetails);
                }


                // Commenting for testig purpose.
                // Setting the order details in session.

                // WC()->session->set('add_product_from_enroll_page', $session_data);


                // WC()->session->set('add_product_from_enroll_page', $session_data);
                $checkoutUrl = $woocommerce->cart->get_checkout_url();
            }
        }
        if (empty($checkoutUrl)) {
            wp_send_json_error(__("Checkout page not found, Please contact admin", 'ebbp-textdomain'));
        }
        wp_send_json_success($checkoutUrl);
    }

    public function wdmAddMoreQuantity()
    {
        // wp_send_json_error("Error occured while adding cou??rses");
        if (WC()->session->get('eb-bp-create-same-product')) {
            WC()->session->set('eb-bp-create-same-product', 0);
        }
        //v1.1.1
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION["addQuantity"] = 1;
        $currency = get_woocommerce_currency_symbol();
        $cohortId = $_POST['mdl_cohort_id'];
        global $wpdb;
        $tableName = $wpdb->prefix . "bp_cohort_info";
        $query = $wpdb->prepare("SELECT PRODUCTS FROM $tableName WHERE MDL_COHORT_ID = %d", $cohortId);
        $result = $wpdb->get_var($query);
        $product = @unserialize($result);
        ob_start();
        ?>
        <div id='add-quantity'>
            <div class="wdm-add-prod_qty">
                <div style="display: table-row;">
                    <label for="wdm_new_prod_qty" style="display: table-cell;padding-right:10px;">Enter Quantity:</label>
                    <input type="number" min="0" maxlength="5" name="wdm_new_prod_qty" value="0" id="wdm_new_prod_qty" style="display: table-cell;">
                </div>
            </div>
            <table id ='add-quantity-table' class="wdm-more-qty-tbl" border="0" data-cohortid='<?php echo $cohortId; ?>'>
                <thead>
                    <tr>
                        <th class="eb_add_qty_tbl_sr_no"><?php _e("Sr. No.", 'ebbp-textdomain'); ?></th>
                        <th class="eb_add_qty_tbl_prod_name"><?php _e("Product Name", 'ebbp-textdomain'); ?></th>
                        <th class="eb_add_qty_tbl_price"><?php _e("Price", 'ebbp-textdomain'); ?></th>
                        <th class="eb_add_qty_tbl_x"></th>
                        <th class="eb_add_qty_tbl_qty"><?php _e("Quantity", 'ebbp-textdomain'); ?></th>
                        <th class="eb_add_qty_tbl_equal"></th>
                        <th class="eb_add_qty_tbl_total_price"><?php _e("Price", 'ebbp-textdomain'); ?></th>
                    </tr>
                </thead>
                <tbody class="wdm-dialog-scroll">
                    <?php
                    $cnt = 0;
                    foreach ($product as $pid => $quantity) {
                        $cnt++;
                        $quantity = $quantity;
                        $courses = $this->getCoursesAssociatedWithProduct($pid);
                        ?>
                        <tr>
                            <td class="eb_add_qty_tbl_sr_no"><?php echo $cnt; ?></td>
                            <td class='wdmProductNameContainer eb_add_qty_tbl_prod_name'>
                                <ul class='wdmProductName'>
                                    <li class = 'product_title'><?php echo get_the_title($pid); ?><ul>
                                            <?php
                                            foreach ($courses as $course) {
                                                ?>
                                                <li> - <?php echo get_the_title($course); ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                            <?php
                            //v1.1.1
                            $productPrice = $this->getProductPrice($pid);
                            ?>
                            <td class="eb_add_qty_tbl_price">
                                <span><?php echo $currency; ?></span>
                                <span id = '<?php echo $pid; ?>-per-product-price'><?php echo $productPrice; ?></span>
                            </td>
                            <td class="eb_add_qty_tbl_x"> x </td>
                            <td class="eb_add_qty_tbl_qty">
                                <span class="wdm_new_qty_per_prod" id="<?php echo $pid; ?>">0</span></td>
                            </td>
                            <td class="eb_add_qty_tbl_equal"> = </td>
                            <td class="eb_add_qty_tbl_total_price">
                                <div class="wdm-item-price">
                                    <span><?php echo $currency; ?></span>
                                    <span class = 'wdm-quantity-total add-more-quantity' id='<?php echo $pid; ?>-total-price'>0</span>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td><strong>TOTAL</strong></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td >
                            <span><?php echo $currency; ?></span>
                            <span id='add-quantity-total-price'>0</span>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>

        <?php
        if (count($product) <= 0) {
            wp_send_json_error(__("'Sorry, currently there are no group products available '", 'ebbp-textdomain'));
        }
        $responce = ob_get_clean();
        wp_send_json_success($responce);
    }

    public function wdmAddNewProductToGroup()
    {
        if (WC()->session->get('eb-bp-create-same-product')) {
            WC()->session->set('eb-bp-create-same-product', 0);
        }
        $currency = get_woocommerce_currency_symbol();
        $mdlCohortId = $_POST['mdl_cohort_id'];
        global $wpdb;
        $tblCohoInfo = $wpdb->prefix . "bp_cohort_info";
        $stmtSelecCohoInfo = $wpdb->prepare("SELECT PRODUCTS, NAME, COHORT_NAME FROM $tblCohoInfo WHERE MDL_COHORT_ID = %d", $mdlCohortId);
        $result = $wpdb->get_row($stmtSelecCohoInfo, ARRAY_A);
        $cohortProducts = unserialize($result['PRODUCTS']);
        $avaQty = max($cohortProducts);
        $cohortName = $result['COHORT_NAME'];
        $cntCohortMemb = $this->getTotalMembers($mdlCohortId);
        $minProductQuantity = $avaQty + $cntCohortMemb;
        $tblMoodleEnroll = $wpdb->prefix . "woo_moodle_course";
        $stmtSeleMdlRec = "SELECT DISTINCT `product_id` FROM `{$tblMoodleEnroll}`";
        $allProduct = $wpdb->get_col($stmtSeleMdlRec);
        ob_start();
        ?>
            <div id ='add-quantity-table' class="wdm-more-prod-tbl wdm-dialog-scroll" data-cohortid='<?php echo $mdlCohortId; ?>'>
            </div>
        <div id ='bp-new-product'>
            <table id ='bp-new-product-table' class="wdm-more-prod-tbl wdm-dialog-scroll" data-cohortid='<?php echo $mdlCohortId; ?>'>

                <thead>
                    <tr>
                        <th></th>
                        <th><?php _e("Product Name", 'ebbp-textdomain'); ?></th>
                        <th><?php _e("Price", 'ebbp-textdomain'); ?></th>
                        <th></th>
                        <th><?php _e("Quantity", 'ebbp-textdomain'); ?></th>
                        <th></th>
                        <th><?php _e("Total", 'ebbp-textdomain'); ?></th>
                    </tr>
                </thead>
                <tbody class="wdm-dialog-scroll">
                    <?php
                    foreach ($allProduct as $product) {
                        $price = get_post_meta($product, "_regular_price", 1);

                        if ('publish' === get_post_status($product) && isset($price) && isset($price) && $price != "") {
                            $postMeta = get_post_meta($product, "product_options");
                            // v1.1.1
                            //$postMeta = unserialize($postmeta[0]);
                            if (isset($postMeta[0]['moodle_course_group_purchase']) && $postMeta[0]['moodle_course_group_purchase'] == 'on') {
                                if (array_key_exists($product, $cohortProducts)) {
                                    continue;
                                } else {
                                    ?>
                                    <tr>
                                        <td class = 'box'>
                                            <input class='wdm_selected_products' id="<?php echo $product; ?>-wdm-sele-prod" type = 'checkbox' />
                                        </td>
                                        <td class='wdmProductNameContainer'>
                                            <ul class='wdmProductName'>
                                                <li class = 'product_title' data-id = "<?php echo $product; ?>">
                                                        <?php echo get_the_title($product); ?>
                                                    <ul>
                                                        <?php
                                                        $courses = $this->getCoursesAssociatedWithProduct($product);
                                                        foreach ($courses as $course) {
                                                            ?>
                                                            <li> - <?php echo get_the_title($course); ?></li>
                                                            <?php
                                                        }
                                                        ?>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                        <?php
                                        $productPrice = $this->getProductPrice($product);
                                        ?>
                                        <td>
                                            <span><?php echo $currency ?></span>
                                            <span id="<?php echo $product; ?>-per-product-price"><?php echo $productPrice; ?></span>
                                        </td>
                                        <td> x </td>
                                        <td style = 'text-align:center;'>
                                            <span class="wdm_new_qty_per_new_prod" id="<?php echo $product; ?>"><?php echo $minProductQuantity; ?></span></td>
                                        </td>
                                        <td> = </td>
                                        <td>
                                            <span><?php echo $currency; ?> </span>
                                            <span class = 'wdm-quantity-total add-more-product' id='<?php echo $product; ?>-total-price'>0</span>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                    }

                    if (count($allProduct) <= 0) {
                        wp_send_json_error(__("'Sorry, currently there are no group products available '", 'ebbp-textdomain'));
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td><strong>TOTAL</strong></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <span><?php echo $currency; ?></span>
                            <span id='add-quantity-total-price'>0</span>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <?php
        $current_user =  wp_get_current_user();
        $cohortName = isset($result["NAME"]) && !empty($result["NAME"]) ? $result["NAME"] : str_replace($current_user->user_login."_", "", $cohortName);
        $responce = array("data" => ob_get_clean(), "cohort" => $cohortName);
        wp_send_json_success($responce);
    }

    public function getProductPrice($productId)
    {
        $product = wc_get_product($productId);
        return $product->get_price();
    }

    public function getTotalMembers($mdlCohortId)
    {
        global $wpdb;
        $tableName = $wpdb->prefix . "moodle_enrollment";
        $query = $wpdb->prepare("SELECT DISTINCT  user_id FROM $tableName WHERE mdl_cohort_id = %d", $mdlCohortId);
        $result = $wpdb->get_results($query, ARRAY_A);
        return count($result);
    }

    public function getCoursesAssociatedWithProduct($productId)
    {
        global $wpdb;
        $tbl_name = $wpdb->prefix . "woo_moodle_course";
        $query = $wpdb->prepare("SELECT DISTINCT `moodle_post_id` FROM `{$tbl_name}` WHERE `product_id` = %d ", $productId);
        $courses = $wpdb->get_col($query);
        return $courses;
    }
}

new EdwiserMultipleCourseEnrollUsers();


