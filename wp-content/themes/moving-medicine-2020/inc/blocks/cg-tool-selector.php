<?php

/**
 * Consultation Guide Tool Selector Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$args = array(
	'block' => $block,
	'is_preview' => $is_preview,
	'post_id' => $post_id
);

$selection = get_field('consultation_guide_tool'); ?>

<?php if( 'decision' !== $selection && null !== $selection ): ?>

	<div class="triangle-for-tools"></div>

<?php endif;

switch($selection) {

	case 'activity_calculator':
		one_get_content('consultation-guides/tools', 'activity-calculator', $args);
	break;

	case 'share_benefits':
		one_get_content('consultation-guides/tools', 'share-benefits', $args);
	break;

	case 'common_concerns':
		one_get_content('consultation-guides/tools', 'common-concerns', $args);
	break;

	case 'cycles':
		one_get_content('consultation-guides/tools', 'cycles', $args);
	break;

	case 'decision':
		one_get_content('consultation-guides/tools', 'decision', $args);
	break;

	case 'activities':
		one_get_content('consultation-guides/tools', 'activities', $args);
	break;

	case 'resources':
		one_get_content('consultation-guides/tools', 'resources', $args);
	break;

	case 'signposts':
		one_get_content('consultation-guides/tools', 'signposts', $args);
	break;

	default:
		one_get_content('consultation-guides/tools', 'content', $args);
}

?>