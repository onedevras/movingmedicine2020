<?php

namespace app\wisdmlabs\edwiserBridge\BulkPurchase;

use app\wisdmlabs\edwiserBridge as edwiserBridge;

if (!class_exists("EbBpSendEmailer")) {

    class EbBpSendEmailer
    {

        public function sendBulkPurchaseEmail($args)
        {
            $emailTmplData = edwiserBridge\EBAdminEmailTemplate::getEmailTmplContent("eb_emailtmpl_bulk_prod_purchase_notifn");
            $allowNotify = get_option("eb_emailtmpl_bulk_prod_purchase_notifn_notify_allow");
            if ($emailTmplData && $allowNotify == "ON") {
                $emailTmplObj = new edwiserBridge\EBAdminEmailTemplate();
                return $emailTmplObj->sendEmail($args['user_email'], $args, $emailTmplData);
            }
        }
        public function sendCohortEnrollmentEmail($args)
        {
            $emailTmplData = edwiserBridge\EBAdminEmailTemplate::getEmailTmplContent("eb_emailtmpl_student_enroll_in_cohort_notifn");
            $allowNotify = get_option("eb_emailtmpl_student_enroll_in_cohort_notifn_notify_allow");
            if ($emailTmplData && $allowNotify == "ON") {
                $emailTmplObj = new edwiserBridge\EBAdminEmailTemplate();
                return $emailTmplObj->sendEmail($args['user_email'], $args, $emailTmplData);
            }
        }
        public function sendCohortUnEnrollmentEmail($args)
        {
            $emailTmplData = edwiserBridge\EBAdminEmailTemplate::getEmailTmplContent("eb_emailtmpl_student_unenroll_in_cohort_notifn");
            $allowNotify = get_option("eb_emailtmpl_student_unenroll_in_cohort_notifn_notify_allow");
            if ($emailTmplData && $allowNotify == "ON") {
                $emailTmplObj = new edwiserBridge\EBAdminEmailTemplate();
                return $emailTmplObj->sendEmail($args['user_email'], $args, $emailTmplData);
            }
        }


        public function bpSendCohortDeleteEmail($args)
        {
            $emailTmplData = edwiserBridge\EBAdminEmailTemplate::getEmailTmplContent("eb_emailtmpl_cohort_deletion");
            $allowNotify = get_option("eb_emailtmpl_cohort_deletion_notify_allow");
            if ($emailTmplData && $allowNotify == "ON") {
                $emailTmplObj = new edwiserBridge\EBAdminEmailTemplate();
                return $emailTmplObj->sendEmail($args['user_email'], $args, $emailTmplData);
            }
        }


        public function bpSendGroupRefundEmail($args)
        {
            $emailTmplData = edwiserBridge\EBAdminEmailTemplate::getEmailTmplContent("eb_emailtmpl_bulk_refund");
            $allowNotify = get_option("eb_emailtmpl_bulk_refund_notify_allow");
            if ($emailTmplData && $allowNotify == "ON") {
                $emailTmplObj = new edwiserBridge\EBAdminEmailTemplate();
                return $emailTmplObj->sendEmail($args['user_email'], $args, $emailTmplData);
            }
        }



       /* public function bpSendFullRefundEmail($args)
        {
            $emailTmplData = edwiserBridge\EBAdminEmailTemplate::getEmailTmplContent("eb_emailtmpl_student_unenroll_in_cohort_notifn");
            $allowNotify = get_option("eb_emailtmpl_student_unenroll_in_cohort_notifn_notify_allow");
            if ($emailTmplData && $allowNotify == "ON") {
                $emailTmplObj = new edwiserBridge\EBAdminEmailTemplate();
                return $emailTmplObj->sendEmail($args['user_email'], $args, $emailTmplData);
            }
        }*/


        /************************************************
         - When bulk purchase product is purchased without checking bulk purchase product checkbox then the normal woo-int mail should go.
         - but the mail added in woo-int don't have any hook added to send it.
         - so temporarily addin hook for only bulk purchase.

        IMP
        remove it when Hookis added in woo-int for mail eb_emailtmpl_woocommerce_moodle_course_notifn
         ***********************************************/
        public function sendCourseEnrollmentEmail($args)
        {
            $emailTmplData = edwiserBridge\EBAdminEmailTemplate::getEmailTmplContent("eb_emailtmpl_woocommerce_moodle_course_notifn");
            $allowNotify = get_option("eb_emailtmpl_woocommerce_moodle_course_notifn_notify_allow");
            if ($emailTmplData && $allowNotify == "ON") {
                $emailTmplObj = new edwiserBridge\EBAdminEmailTemplate();
                return $emailTmplObj->sendEmail($args['user_email'], $args, $emailTmplData);
            }
        }

    }
}
