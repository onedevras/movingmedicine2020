(function($) {

  var quizSlider;

  function init() {

    if(!document.querySelectorAll(".quiz-section").length)
      return;

    quizSlider = tns({
      "container": ".questions",
      "items": 1,
      "nested": "inner",
      "loop": false,
      "slideBy": "page",
      "swipeAngle": false,
      "speed": 400,
      "controls": true,
      "controlsContainer": "#custom-controls",
      "nav": false,
      "touch": true,
      "mouseDrag": false,
      "autoHeight": false,
    });

    $("[data-answer]").click(showAnswer);
    $(".button.continue").click(goToNextQuestion);

    quizSlider.events.on("transitionStart", updateTitle);
    quizSlider.events.on("indexChanged", hideAnswer);

  }

  function showAnswer(event) {
    var container = $(this).parents(".question-inner"),
      answer = $(this).attr("data-answer");

    container.find('.content').hide();
    container.find('.'+answer).show();
    return false;
  }

  function hideAnswer(event) {
    var slide = event.slideItems[event.indexCached];

    $(slide).find('.content').show();
    $(slide).find('.answer').hide();
    return false;
  }

  function goToNextQuestion(event) {
    quizSlider.goTo("next");
    return false;
  }

  function updateTitle(event) {
    var index = quizSlider.getInfo().index,
      content = $(".questions > div").eq(index).find("[data-title]"),
      title = content.attr("data-title");

    if(title) {
      var quiz = content.parents(".quiz");
      quiz.find(".title h4").text(title);
    }

    return false;
  }
  
  $(init);
})(jQuery);
