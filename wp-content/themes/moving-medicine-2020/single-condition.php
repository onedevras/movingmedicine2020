<?php 
// We want a different header depending on the branching


// Check querystring to see if we need to display a guide
if( $_GET['guide'] ) {
  $guide = $_GET['guide'];
}
elseif(isset($wp_query->query_vars['guide'])) {
  $guide = get_query_var('guide');
}

$args = array(
  'guide' => $guide
);

// Display one of the three guides or the landing page
one_get_content('consultation-guides', $guide ? 'guide' : 'landing-page', $args);
?>

<?php get_footer(); ?>