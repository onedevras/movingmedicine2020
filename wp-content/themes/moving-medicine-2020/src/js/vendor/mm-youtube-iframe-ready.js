// This needs to be in global scope to work
function onYouTubeIframeAPIReady() {

  var mm_iframes = document.querySelectorAll('iframe');
  for(var i = 0; i < mm_iframes.length; i++) {
    var mm_iframe = mm_iframes[i];
    try {
      var player = new YT.Player(mm_iframe.id, {
        playerVars: { 'rel': 0, 'modestbranding': 1, 'origin': 'https://movingmedicine.ac.uk' },
        events: {
          'onReady': onPlayerReady
        }
      });

      function onPlayerReady(event) {
         if(event.target && event.target.f && event.target.f.id) {
            window[event.target.f.id] = event.target;
         }
      }
    }
    catch(e) {
    }

  };

}

