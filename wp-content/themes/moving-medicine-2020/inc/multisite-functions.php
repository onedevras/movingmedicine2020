<?php

include('classes/copy-uploads-process.php');

require_once('libraries/action-scheduler/action-scheduler.php');


require_once ABSPATH . 'wp-admin/includes/translation-install.php';

/******************************************************************************
 *
 * Multisite code – Grand Central
 */

add_action('network_admin_menu', 'mm_add_network_cg_page');
function mm_add_network_cg_page() {
	add_menu_page(
		'Create Int. Site',
		'Create Int. Site',
		'administrator',
		'create-international-site',
		'add_new_international_site',
		'dashicons-admin-site-alt',
		8
	);
}

function add_new_international_site() {

	if (!current_user_can('create_sites')) {
		wp_die(__('Sorry, you are not allowed to add sites to this network.'));
	}

	if (isset($_REQUEST['action']) && 'add-international-site' === $_REQUEST['action']) {
		check_admin_referer('add-int-site', '_wpnonce_add-int-site');

		if (!is_array($_POST['blog'])) {
			wp_die(__('Can&#8217;t create an empty site.'));
		}

		$blog   = $_POST['blog'];
		$domain = '';

		$blog['domain'] = trim($blog['domain']);
		if (preg_match('|^([a-zA-Z0-9-])+$|', $blog['domain'])) {
			$domain = strtolower($blog['domain']);
		}

		// If not a subdomain installation, make sure the domain isn't a reserved word.
		if (!is_subdomain_install()) {
			$subdirectory_reserved_names = get_subdirectory_reserved_names();

			if (in_array($domain, $subdirectory_reserved_names, true)) {
				wp_die(
					sprintf(
						/* translators: %s: Reserved names list. */
						__('The following words are reserved for use by WordPress functions and cannot be used as blog names: %s'),
						'<code>' . implode('</code>, <code>', $subdirectory_reserved_names) . '</code>'
					)
				);
			}
		}

		$title = $blog['title'];

		$meta = array(
			'public' => 1,
		);

		// Handle translation installation for the new site.
		if (isset($_POST['WPLANG'])) {
			if ('' === $_POST['WPLANG']) {
				$meta['WPLANG'] = ''; // en_US
			} elseif (in_array($_POST['WPLANG'], get_available_languages(), true)) {
				$meta['WPLANG'] = $_POST['WPLANG'];
			} elseif (current_user_can('install_languages') && wp_can_install_language_pack()) {
				$language = wp_download_language_pack(wp_unslash($_POST['WPLANG']));
				if ($language) {
					$meta['WPLANG'] = $language;
				}
			}
		}

		if (empty($domain)) {
			wp_die(__('Missing or invalid site address.'));
		}

		if (is_subdomain_install()) {
			$newdomain = $domain . '.' . preg_replace('|^central\.|', '', get_network()->domain);
			$path      = get_network()->path;
		} else {
			$newdomain = get_network()->domain;
			$path      = get_network()->path . $domain . '/';
		}

		$password = 'N/A';

		$email = 'admin-backend@oneltd.co.uk';

		$user_id  = email_exists($email);
		if (!$user_id) { // Create a new user with a random password.

			$user_id = username_exists($domain);
			if ($user_id) {
				wp_die(__('The domain or path entered conflicts with an existing username.'));
			}
			$password = wp_generate_password(12, false);
			$user_id  = wpmu_create_user($domain, $password, $email);
			if (false === $user_id) {
				wp_die(__('There was an error creating the user.'));
			}
		}

		$data = array(
			'domain' => $newdomain,
			'path' => $path,
			'title' => $title,
			'user_id' => $user_id,
		);

		$id = wp_insert_site($data);

		if (!is_wp_error($id)) {
			if (!is_super_admin($user_id) && !get_user_option('primary_blog', $user_id)) {
				update_user_option($user_id, 'primary_blog', $id, true);
			}

			wp_mail(
				get_site_option('admin_email'),
				sprintf(
					/* translators: New site notification email subject. %s: Network title. */
					__('[%s] New Site Created'),
					get_network()->site_name
				),
				sprintf(
					/* translators: New site notification email. 1: User login, 2: Site URL, 3: Site title. */
					__(
						'New site created by %1$s
							Address: %2$s
							Name: %3$s'
					),
					$current_user->user_login,
					get_site_url($id),
					wp_unslash($title)
				),
				sprintf(
					'From: "%1$s" <%2$s>',
					_x('Site Admin', 'email "From" field'),
					get_site_option('admin_email')
				)
			);
			wpmu_welcome_notification($id, $user_id, $password, $title, array('public' => 1));
			wp_redirect(
				add_query_arg(
					array(
						'page' => 'create-international-site',
						'update' => 'added',
						'id'     => $id,
					),
					'admin.php'
				)
			);
			exit;
		} else {
			wp_die($id->get_error_message());
		}
	}

	if (isset($_GET['update'])) {
		$messages = array();
		if ('added' === $_GET['update']) {
			$messages[] = sprintf(
				/* translators: 1: Dashboard URL, 2: Network admin edit URL. */
				__('Site added. <a href="%1$s">Visit Dashboard</a> or <a href="%2$s">Edit Site</a>'),
				esc_url(get_admin_url(absint($_GET['id']))),
				network_admin_url('site-info.php?id=' . absint($_GET['id']))
			);
		}
	}

?>

	<div class='wrap'>
		<h1 class='wp-heading-inline'>Create International Site</h1>
		<p class='description'>Fill out the fields below and click Add to create a new International site.</p>
		<?php
		if (!empty($messages)) {
			foreach ($messages as $msg) {
				echo '<div id="message" class="updated notice is-dismissible"><p>' . $msg . '</p></div>';
			}
		}
		?>
		<form method="post" action="<?php echo network_admin_url('admin.php?page=create-international-site&action=add-international-site'); ?>" novalidate="novalidate">
			<?php wp_nonce_field('add-int-site', '_wpnonce_add-int-site'); ?>
			<table class="form-table" role="presentation" style="margin-top: 32px;">
				<tr class="form-field form-required">
					<th scope="row"><label for="site-address"><?php _e('Site Address (URL)'); ?> <span class="required">*</span></label></th>
					<td>
						<?php if (is_subdomain_install()) { ?>
							<input name="blog[domain]" type="text" class="regular-text ltr" id="site-address" aria-describedby="site-address-desc" autocapitalize="none" autocorrect="off" required /><span class="no-break">.<?php echo preg_replace('|^central\.|', '', get_network()->domain); ?></span>
						<?php
						} else {
							echo get_network()->domain . get_network()->path
						?>
							<input name="blog[domain]" type="text" class="regular-text ltr" id="site-address" aria-describedby="site-address-desc" autocapitalize="none" autocorrect="off" required />
						<?php
						}
						echo '<p class="description" id="site-address-desc">' . __('Only lowercase letters (a-z), numbers, and hyphens are allowed.') . '</p>';
						?>
					</td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row"><label for="site-title"><?php _e('Site Title'); ?> <span class="required">*</span></label></th>
					<td><input name="blog[title]" type="text" class="regular-text" id="site-title" required /></td>
				</tr>
				<?php
				$languages    = get_available_languages();
				if (!empty($languages)) :
				?>

					<tr class="form-field form-required">
						<th scope="row"><label for="site-language"><?php _e('Site Language'); ?></label></th>
						<td>
							<?php
							// Network default.
							$lang = get_site_option('WPLANG');

							// Use English if the default isn't available.
							if (!in_array($lang, $languages, true)) {
								$lang = '';
							}

							wp_dropdown_languages(
								array(
									'name'                        => 'WPLANG',
									'id'                          => 'site-language',
									'selected'                    => $lang,
									'languages'                   => $languages,
									'translations'                => []
								)
							);
							?>
						</td>
					</tr>
				<?php endif; // Languages.
				?>
			</table>
			<? submit_button( __( 'Add Site' ), 'primary', 'add-site' ); ?>
		</form>
	</div>

<?php }

function one_gc__add_cap() {
	if (!is_multisite())
		return;

	$custom_cap = 'manage_options_international';
	$min_cap    = 'administrator';
	$grant      = true;

	foreach ($GLOBALS['wp_roles'] as $role_obj) {
		if (
			!$role_obj->has_cap($custom_cap)
			and $role_obj->has_cap($min_cap)
		)
			$role_obj->add_cap($custom_cap, $grant);
	}
}

function add_cap_to_revisor() {
	// get the the role object
	$revisor_role = get_role('revisor');
	$revisor_role->add_cap('manage_options_international');
	$revisor_role->remove_cap('manage_options');
	$revisor_role->add_cap('upload_files');
}
add_action('init', 'add_cap_to_revisor');

// Hide certain fields in the backend from non admins
function my_acf_prepare_field($field) {

	// Lock-in the value "Example".
	if ($field['value'] === 'Example') {
		$field['readonly'] = true;
	};
	return $field;
}


function one_gc_remove_int_editor_role() {
	remove_role('int_editor');
}
add_action('init', 'one_gc_remove_int_editor_role');

function add_role_filter_to_posts_query($query) {
	// if (!is_admin()) {
	// 	return;
	// }

	$user = wp_get_current_user();
	if (in_array('administrator', (array) $user->roles)) {
		return;
	}

	global $pagenow;

	// Conditions
	if ('edit.php' === $pagenow && 'condition' === $query->query['post_type']) {

		// Get disabled patient types
		$excluded_patients = get_field('disable_patient_types', 'options');

		// Exclude them from the query
		$query->set('tax_query', array(
			array(
				'taxonomy' => 'patient',
				'field'    => 'term_id',
				'terms'    => $excluded_patients,
				'operator' => 'NOT IN',
			),
		));

		// Get disabled conditions
		$excluded_conditions = get_field('disable_conditions', 'options');
		$query->set('exclude', $excluded_conditions);
	}

	// Remove non international pages from everywhere else
	elseif ('edit.php' === $pagenow) {
		// We only want to see posts that are non admins or have is_international set
		$query->set('meta_key', 'is_international');
		$query->set('meta_value', 1);
	}
}
add_action('pre_get_posts', 'add_role_filter_to_posts_query');

function mm_disable_sections() {
	$user = wp_get_current_user();
	if (in_array('administrator', (array) $user->roles)) {
		return;
	}

	$disable_consultation_guides = get_field('disable_consultation_guides', 'options');
	$disable_active_hospitals = get_field('disable_active_hospitals', 'options');
	$disable_online_course = get_field('disable_online_course', 'options');

	if ($disable_consultation_guides) {
		remove_menu_page('edit.php?post_type=condition');
		remove_menu_page('edit.php?post_type=organisation');
	}

	if ($disable_active_hospitals) {
		remove_menu_page('edit.php?post_type=resource');
	}

	if ($disable_online_course) {
	}
}
add_action('admin_menu', 'mm_disable_sections');

add_action('acf/save_post', 'mm_acf_save_post');
function mm_acf_save_post($post_id) {

	if ('options' !== $post_id)
		return;

	// Trigger a rebuild of all finders
	build_evidence_finder_list();
	build_activity_finder_list();
	build_patient_information_finder_list();
	$force = true;
	build_nav_consultation_finder($force);
	build_consultation_finder_list();

	build_list_of_supported_locations($force);
}

// Add icons to menu items
add_filter('wp_nav_menu_objects', 'mm_remove_disabled_menu_objects', 10, 2);
function mm_remove_disabled_menu_objects($items, $args) {

	$disable_consultation_guides = get_field('disable_consultation_guides', 'options');
	$disable_active_hospitals = get_field('disable_active_hospitals', 'options');
	$disable_online_course = get_field('disable_online_course', 'options');
	$parents = array();

	foreach ($items as $key => $item) {

		if ('Consultation Guides' === $item->title && $disable_consultation_guides) {
			$parents[] = $item->object_id;
			unset($items[$key]);
		}

		if ('Active Hospitals' === $item->title && $disable_active_hospitals) {
			unset($items[$key]);

			$parents[] = $item->object_id;
		}

		if ($disable_online_course) {

			if ('Course log-in' === $item->title || 'Supporting you' === $item->title || 'Online course' === $item->title) {
				unset($items[$key]);
				$parents[] = $item->object_id;
			}
		}

		if (in_array($item->post_parent, $parents) || ($disable_active_hospitals && strpos($item->url, '/active-hospitals/') > -1)) {
			unset($items[$key]);
		}
	}

	return $items;
}

add_action('wp_trash_post', 'one_gc_trash_attached_posts');
function one_gc_trash_attached_posts($post_id) {

	if (CENTRAL_SITE !== get_current_blog_id()) {
		return;
	}

	$gc_router = get_post_meta($post_id, '_grand_central_router', true);

	foreach ($gc_router as $branch_id => $branch_post) {
		switch_to_blog($branch_id);
		wp_trash_post($branch_post['ID']);
		restore_current_blog();
	}
}

add_action('untrash_post', 'one_gc_untrash_attached_posts');
function one_gc_untrash_attached_posts($post_id) {

	if (CENTRAL_SITE !== get_current_blog_id()) {
		return;
	}

	$gc_router = get_post_meta($post_id, '_grand_central_router', true);

	foreach ($gc_router as $branch_id => $branch_post) {
		switch_to_blog($branch_id);
		wp_untrash_post($branch_post['ID']);
		restore_current_blog();
	}
}

add_action('before_delete_post', 'one_gc_delete_attached_posts');
function one_gc_delete_attached_posts($post_id) {

	// Clean up the router in central
	if (CENTRAL_SITE !== get_current_blog_id()) {
		// Get the original post id
		$original_post_id = get_post_meta($post_id, '_gc_original_post', true);
		$branch_id = get_current_blog_id();
		switch_to_blog(CENTRAL_SITE);
		// Get the router and remove this branch ID as we're about to delete it
		$gc_router = get_post_meta($original_post_id, '_grand_central_router', true);
		unset($gc_router[$branch_id]);
		update_post_meta($original_post_id, '_grand_central_router', $gc_router);
		restore_current_blog();
		return;
	}

	$gc_router = get_post_meta($post_id, '_grand_central_router', true);

	foreach ($gc_router as $site_id => $post) {
		switch_to_blog($site_id);
		wp_delete_post($post);
		restore_current_blog();
	}
}

add_filter(
	'acf/update_value/type=post_object',
	function ($value, $post_id, $field) {
		if ('condition' !== get_post_type($post_id)) {
			return $value;
		}

		$selected_value_gc_router = get_post_meta($value, '_grand_central_router', true);
		if ($selected_value_gc_router) {
			update_post_meta($post_id, '_gc_' . $field['name'] . '_router', $selected_value_gc_router);
		}

		return $value;
	},
	5,
	3
);

add_action('save_post', 'one_gc_insert_post_wrapper', 20, 2);
function one_gc_insert_post_wrapper($original_post_id, $original_post) {

	if (!is_multisite())
		return;

	// Check for _is_gutenberg meta value before proceeding
	$post_meta = get_post_custom($original_post_id);
	if (1 === (int) $post_meta['_is_gutenberg'][0]) {
		// We don't want to continue if the saved post is a gutenberg page
		return;
	}
	one_gc_insert_post_on_all_sites($original_post_id, $original_post);
}

add_action('rest_after_insert_page', 'one_gc_update_terms_in_all_sites', 10, 2);
add_action('rest_after_insert_resource', 'one_gc_update_terms_in_all_sites', 10, 2);
add_action('rest_after_insert_organisation', 'one_gc_update_terms_in_all_sites', 10, 2);
add_action('rest_after_insert_condition_slide', 'one_gc_update_terms_in_all_sites', 10, 2);
add_action('rest_after_insert_evidence', 'one_gc_update_terms_in_all_sites', 10, 2);
add_action('rest_after_insert_patient_information', 'one_gc_update_terms_in_all_sites', 10, 2);
add_action('rest_after_insert_slide_extra', 'one_gc_update_terms_in_all_sites', 10, 2);
function one_gc_update_terms_in_all_sites($post, $request) {

	one_gc_insert_post_on_all_sites($post->ID, $post);
}

// One of the things we need to do in here is
function one_gc_check_if_post_changed($post_id, $post_after, $post_before) {

	if (!is_multisite())
		return;

	$user = wp_get_current_user();
	$userRole = $user->roles ? $user->roles[0] : false;

	// We only want to run this for the int_editor role
	if ('int_editor' !== $userRole) {
		return;
	}

	// If the original post is already stored then don't overwrite it
	if (get_post_meta($post_id, 'original_post', true)) {
		// _one_log("Original post already exists so I'm not storing it again");
		return;
	}

	// _one_log("Storing the original post");
	add_post_meta($post_id, 'original_post', $post_before, true);

	// Create a new post with the new data


}
// add_action('post_updated', 'one_gc_check_if_post_changed', 10, 3);

function prepare_post_meta_and_terms_for_insert($original_post_id, $original_post) {
	if (!is_multisite())
		return;

	// let's get this post data as an array
	$post_data = array(
		'post_author' => $original_post->post_author,
		'post_date' => $original_post->post_date,
		'post_modified' => $original_post->post_modified,
		'post_content' => $original_post->post_content,
		'post_title' => $original_post->post_title,
		'post_excerpt' => $original_post->post_excerpt,
		'post_status' => 'publish',
		'post_name' => $original_post->post_name,
		'post_type' => $original_post->post_type,
		'post_parent' => $original_post->post_parent,
	);

	// terms and post meta as well
	$post_terms = array(
		'category' => wp_get_object_terms($original_post_id, 'category', array('fields' => 'slugs')),
		'condition-tax' => wp_get_object_terms($original_post_id, 'condition-tax', array('fields' => 'slugs')),
		'patient' => wp_get_object_terms($original_post_id, 'patient', array('fields' => 'slugs')),
		'slide_extra_type' => wp_get_object_terms($original_post_id, 'slide_extra_type', array('fields' => 'slugs')),
		'subject' => wp_get_object_terms($original_post_id, 'subject', array('fields' => 'slugs')),
		'organisation_type' => wp_get_object_terms($original_post_id, 'organisation_type', array('fields' => 'slugs'))
	);

	return array(
		'post_data' => $post_data,
		'post_terms' => $post_terms
	);
}

// https://rudrastyh.com/wordpress-multisite/post-to-all-sites.html#wordpress_plugin
function one_gc_insert_post_on_all_sites($original_post_id, $original_post) {

	if (!is_multisite())
		return;

	// We only want to broadcast from Central
	if (get_current_blog_id() !== CENTRAL_SITE) {
		// Update the meta on the post befoer bombing out
		// Check if we know the orignal post and update that if we do
		$central_post_id = get_post_meta($original_post_id, '_gc_original_post', true);
		if (isset($central_post_id)) {
			// cache the current site id
			$site_id = get_current_blog_id();
			switch_to_blog(CENTRAL_SITE);
			// Get the gc router or create new one if it doesn't exist
			$gc_router = get_post_meta($central_post_id, '_grand_central_router', true) ?: array();

			if (!isset($gc_router[$site_id]) && !isset($gc_router[$site_id][$original_post_id])) {
				// Add the post we're editing to it
				$gc_router[$site_id]['ID'] = $original_post_id;
			}
			// Set it to detached so we know not to overwrite it later
			$gc_router[$site_id]['detached'] = 1;
			update_post_meta($central_post_id, '_grand_central_router', $gc_router);
			restore_current_blog();
		}
		update_post_meta($original_post_id, '_gc_detached', 1);
		return;
	}


	// do not publish revisions
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return $original_post_id;

	// actually we need only "publish" status
	if (get_post_status($original_post) !== 'publish') return $original_post_id;

	// prevent "Fatal error: Maximum function nesting level reached"
	remove_action('save_post', __FUNCTION__, 20);

	// Save the url for the current blog
	$central_url = get_bloginfo('url');
	$central_url_for_blocks = str_replace('//', '\/\/', $central_url);

	// look through all sites EXCEPT central
	$sites = get_sites(array(
		'site__not_in' => array(CENTRAL_SITE)
	));

	// Get the original post data and terms
	$post_complete = prepare_post_meta_and_terms_for_insert($original_post_id, $original_post);

	// _one_log($post_complete);

	foreach ($sites as $site) {

		$site_id = $site->blog_id;

		$post_data = $post_complete['post_data'];
		$post_terms = $post_complete['post_terms'];
		$post_meta = get_post_custom($original_post_id);

		$gc_router = get_post_meta($original_post_id, '_grand_central_router', true) ?: array();

		// read post_content and swap attachment ids from central to the child site attachment ids
		$blocks = parse_blocks($post_data['post_content']);
		foreach ($blocks as $block) {
			if (isset($block['attrs']) && isset($block['attrs']['data'])) {
				foreach ($block['attrs']['data'] as $dkey => $datablock_value) {
					//image attribute found, checked by ACF?
					if (preg_match("/image/i", $dkey) || preg_match("/icon/i", $dkey) || preg_match("/logo/i", $dkey) || preg_match("/file/i", $dkey)) {
						//look for an attachement id?
						if (is_numeric($datablock_value)) {
							$central_attachment_id = $datablock_value;

							$gc_attachment_router = get_post_meta($central_attachment_id, '_grand_central_router', true) ?: array();

							// If the blog is already in the gc_router then we already know the post id
							if (isset($gc_attachment_router[$site_id])) {
								// Set the id of the post so we can update it
								$child_attachment_id = $gc_attachment_router[$site_id]['ID'];
								$post_data['post_content'] = str_replace($central_attachment_id, $child_attachment_id, $post_data['post_content']);
								_one_log('Site '.$site_id.' , change central attachment ('.$dkey.') with ID ' .$central_attachment_id . ' to chilid site ID '. $child_attachment_id.PHP_EOL );
							} else {
								//try to copy old attachments over if they dont exist
								copy_across_to_multisite($central_attachment_id);

								$gc_attachment_router = get_post_meta($central_attachment_id, '_grand_central_router', true) ?: array();

								// If the blog is already in the gc_router then we already know the post id
								if (isset($gc_attachment_router[$site_id])) {
									// Set the id of the post so we can update it
									$child_attachment_id = $gc_attachment_router[$site_id]['ID'];
									$post_data['post_content'] = str_replace($central_attachment_id, $child_attachment_id, $post_data['post_content']);
									_one_log('Site '.$site_id.' , change central attachment('.$dkey.') with ID ' .$central_attachment_id . ' to chilid site ID '. $child_attachment_id.PHP_EOL );
								}
							}
						}
					}
				}
			} else {
            _one_log('WARNING! UNEXPECTED BLOCK STRUCTURE FOUND IN ACF BLOCKS FOR POST ID '. $original_post_id.PHP_EOL);
				_one_log(json_encode($block));
			}
		}

      // find the parent gc router and find the equivelant from that site and swap it
      $gc_router_parent = array();
      if($post_data['post_parent'] && !empty($post_data['post_parent'])) {
         $gc_router_parent = get_post_meta($post_data['post_parent'], '_grand_central_router', true) ?: array();
				if($gc_router_parent[$site_id] && $gc_router_parent[$site_id]['ID']) {
               $post_data['post_parent'] = $gc_router_parent[$site_id]['ID'];
            }
      }

		switch_to_blog($site_id);

		$site_url = get_bloginfo('url');
		$site_url_for_blocks = str_replace('//', '\/\/', $site_url);

		// Search and replace the links
		$post_data['post_content'] = str_replace($central_url, $site_url, $post_data['post_content']);
		$post_data['post_content'] = str_replace($central_url_for_blocks, $site_url_for_blocks, $post_data['post_content']);

		// If the blog is already in the gc_router then we already know the post id
		if (isset($gc_router[$site_id])) {
			if (isset($gc_router[$site_id]['detached']) && 1 == $gc_router[$site_id]['detached']) {
				// This post is detached so don't update
				restore_current_blog();
				_one_log('This post on site ' .$site_id .' is detached so dont update. ' . $original_post_id);
				_one_log($gc_router);
				continue;
			}

			// Need to correct any old gc_routers that just contain the id number
			if (!is_array($gc_router[$site_id])) {
				$gc_router[$site_id]['ID'] = $gc_router[$site_id];
				unset($gc_router[$site_id]);
				update_post_meta($original_post_id, '_grand_central_router', $gc_router);
			}

			// Set the id of the post so we can update it
			$post_data['ID'] = $gc_router[$site_id]['ID'];
		}
		// Otherwise, if a post exists in the target site and has the same slug, type and status, let's assume it's the same thing and update it
		elseif ($destination_post = get_posts(array('name' => $post_data['post_name'], 'post_type' => $post_data['post_type'], 'post_status' => 'publish'))) {
			// If the ID is the same then we know it's the same post that would have been created during the clone
			if ($original_post_id == $destination_post[0]->ID && !get_post_meta($destination_post[0]->ID, '_gc_detached', true)) {
				$post_data['ID'] = $destination_post[0]->ID;
			}
		}
		// If none of the above, the post doesn't exist so we want to create a new one. To do this we need to make sure there's no post id set in the data
		else {
			unset($post_data['ID']);
		}

		// addslashes as wp_insert_post will escape one layer of slashes
		$post_data['post_content'] = wp_slash($post_data['post_content']);
		// Add the post to the branch
		$inserted_post_id = (int) wp_insert_post($post_data);

		if ($inserted_post_id > 0) {
			$gc_router[$site_id]['ID'] = $inserted_post_id;
		}

		// Add the terms
		foreach ($post_terms as $term => $value) {
			wp_set_object_terms($inserted_post_id, $value, $term, false);
		}

		foreach ($post_meta as $meta_key => $meta_values) {
			// we do not need these redirects
			if ('_wp_old_slug' === $meta_key) {
				continue;
			}

			// Get rid of the edit lock
			if ('_edit_lock' === $meta_key) {
				continue;
			}
			foreach ($meta_values as $meta_value) {
				if (
					'_grand_central_router' === $meta_key ||
					preg_match("/^_gc_.*_router$/", $meta_key)
				) {
					continue;
				}

				if ('condition' === get_post_type($inserted_post_id)) {
					// We need to check and see if the value should be replaced with a branch specific value
					// For example, if the linked  post has a different ID on the branch site
					// This is stored in the post meta as _cg_{field_name}_router
					$gc_post_router_raw = $post_meta["_gc_{$meta_key}_router"];

					if ($gc_post_router_raw[0]) {
						$gc_post_router = unserialize($gc_post_router_raw[0]);
						$meta_value = $gc_post_router[$site_id];
					}
				}
				if (is_serialized($meta_value)) {
					$meta_value = unserialize($meta_value);
				}
				update_post_meta($inserted_post_id, $meta_key, $meta_value);
			}
		}
		// Add a new meta value pointing back to the original post
		update_post_meta($inserted_post_id, '_gc_original_post', $original_post_id);

		switch_to_blog(CENTRAL_SITE);

		update_post_meta($original_post_id, '_grand_central_router', $gc_router);
	}
}

add_filter('rest_pre_insert_page', 'one_gc_check_if_gutenberg_before_save', 10, 2);
add_filter('rest_pre_insert_resource', 'one_gc_check_if_gutenberg_before_save', 10, 2);
add_filter('rest_pre_insert_organisation', 'one_gc_check_if_gutenberg_before_save', 10, 2);
add_filter('rest_pre_insert_condition_slide', 'one_gc_check_if_gutenberg_before_save', 10, 2);
add_filter('rest_pre_insert_evidence', 'one_gc_check_if_gutenberg_before_save', 10, 2);
add_filter('rest_pre_insert_patient_information', 'one_gc_check_if_gutenberg_before_save', 10, 2);
add_filter('rest_pre_insert_slide_extra', 'one_gc_check_if_gutenberg_before_save', 10, 2);
function one_gc_check_if_gutenberg_before_save($prepared_post, $request) {

	if (!is_multisite())
		return $prepared_post;

	// Mark the post as gutenberg so we don't run the post.php request in save_post
	add_post_meta($prepared_post->ID, '_is_gutenberg', '1', true);

	return $prepared_post;
}

/**
 * When an international site publishes a new page or post, we need to make sure it's
 * status is set to pending make sure it can bee seen ib Central for verification.
 * Shall we also send an email to all super admins?
 */
function one_gc_mark_for_verification($data) {
	if (!is_multisite())
		return $data;

	// If the user's an int_editor then make sure we're not unpublishing a published post

	// If it's publish then we need to stop the post from

	// $user = wp_get_current_user();
	// $userRole = $user->roles ? $user->roles[0] : false;

	// if ('int_editor' !== $userRole) {
	//   $data['post_status'] = 'publish';
	// }

	// _one_log($data);

	return $data;
}
// add_filter('wp_insert_post_data', 'one_gc_mark_for_verification', '99');

function one_gc_clone_on_insert_site($new_site) {
	if (!is_multisite())
		return;

	//clone the specified blog to the newly registered blog
	one_gc_clone_site(CENTRAL_SITE, $new_site);
}
add_action('wp_initialize_site', 'one_gc_clone_on_insert_site', 11);

function one_gc_clone_site($existing_site_id, $new_site) {
	global $wpdb;

	$new_site_id = $new_site->blog_id;
	$db_name = $wpdb->dbname;

	$new_site->domain = str_replace('central.', '', $new_site->domain);

	// Get site upload dirs
	switch_to_blog($new_site_id);
	$destination_upload_dir = wp_upload_dir();
	$dst_site_url = get_bloginfo('url');
	restore_current_blog();
	$source_upload_dir = wp_upload_dir();
	$src_site_url = get_bloginfo('url');

	$copy_uploads_process = new WP_Copy_Uploads_Process();
	recursive_queue_for_copy($source_upload_dir['basedir'], $destination_upload_dir['basedir'], $copy_uploads_process);
	$copy_uploads_process->save()->dispatch();

	// The table prefix for the blog we want to clone
	$src_table_prefix = $wpdb->get_blog_prefix(CENTRAL_SITE);

	// the table prefix for the target blog ubin which we are coding
	$dst_table_prefix = $wpdb->get_blog_prefix($new_site_id);

	// exclude these tables 👇
	$excluded_tables = array(
		'wp_blogmeta',
		'wp_blogs',
		'wp_registration_log',
		'wp_signups',
		'wp_site',
		'wp_sitemeta',
		'wp_usermeta',
		'wp_users'
	);

	// We're just going to copy all the tables from the master (excluding a few)
	$query_get_tables = "SHOW TABLES FROM {$db_name}
		WHERE Tables_in_{$db_name}
		LIKE '{$src_table_prefix}%' AND Tables_in_{$db_name}
		NOT REGEXP '{$src_table_prefix}[0-9]+'";

	foreach ($excluded_tables as $excluded_table) {
		$query_get_tables .= " AND Tables_in_{$db_name}
			NOT LIKE '{$excluded_table}'";
	}

	// Get the tables to clone
	$tables = $wpdb->get_col($query_get_tables);

	$tables = preg_replace("|^$src_table_prefix|", '', $tables);

	// The options that we don't want to alter in the target blog
	// We will preserve the values for these in the options table of the newly created blog
	$excluded_options = array(
		'siteurl',
		'home',
		'blogname',
		'admin_email',
		'upload_path',
		'upload_url_path',
		$dst_table_prefix . 'user_roles', //preserve the roles
		// add keys to preserve here
	);

	$excluded_options = esc_sql($excluded_options);

	// We're going to use || clause to fetch everything in a single query.
	// For this to work we need to quote the string
	$excluded_option_list = "('" . join("','", $excluded_options) . "')";

	//the options table name for the new blog in which we are going to clone in next few seconds
	$dst_blog_options_table = $dst_table_prefix . 'options';

	$excluded_options_query = "SELECT option_name, option_value FROM {$dst_blog_options_table} WHERE option_name IN {$excluded_option_list}";

	//let us fetch the data
	$excluded_options_data = $wpdb->get_results($excluded_options_query);

	// $search_replace_process = new WP_Search_Replace_Process();

	// Make sure sql_mode doesn't contain NO_ZERO_IN_DATE,NO_ZERO_DATE
	// https://stackoverflow.com/questions/36882149/error-1067-42000-invalid-default-value-for-created-at/37696251#37696251
	$show_variables = "show variables like 'sql_mode';";
	// _one_log($excluded_options_data);

	$variables = $wpdb->get_var($show_variables, 1);
	$new_variables = preg_replace(array('/NO_ZERO_IN_DATE,?/', '/NO_ZERO_DATE,?/'), '', $variables);

	// Set the variables
	$query_set_variables = "set global sql_mode = '{$new_variables}';";

	$wpdb->query($query_set_variables);

	$args = array(
		'tables' => $tables,
		'src_table_prefix' => $src_table_prefix,
		'dst_table_prefix' => $dst_table_prefix,
		'src_site_url' => $src_site_url,
		'dst_site_url' => $dst_site_url,
		'excluded_options' => $excluded_options_data
	);

	// We need to get each row
	queue_rows($args);

	// Be responsible and reset the variables to their previous state
	$query_set_variables = "set global sql_mode = '{$variables}';";
	$wpdb->query($query_set_variables);

	$supers = get_super_admins();
	foreach ($supers as $super) {
		$wp_user = get_user_by('login', $super);
		add_user_to_blog($new_site_id, $wp_user->ID, 'administrator');
	}
}

// Parse src file tree, generate folders in dest and queue files for copying
function recursive_queue_for_copy($src, $dst, $process) {
	$dir = opendir($src);
	while (false !== ($file = readdir($dir))) {
		// Ignore ., .., and sites.
		if (($file != '.') && ($file != '..') && ($file !== 'sites')) {
			if (is_dir($src . '/' . $file)) {
				recursive_queue_for_copy($src . '/' . $file, $dst . '/' . $file, $process);
			} else {
				$data = array(
					'src' => $src . '/' . $file,
					'dst' => $dst . '/' . $file
				);
				$process->push_to_queue($data);
			}
		}
	}
	closedir($dir);
}

function queue_tables($tables, $src_table_prefix, $dst_table_prefix, $process) {
	// global $wpdb;
	foreach ($tables as $table) {
		$data = array(
			'table' => $table,
			'src_prefix' => $src_table_prefix,
			'dst_prefix' => $dst_table_prefix,
		);
		// Queue the row
		$process->push_to_queue($data);
	}
}

// Get a bunch of rows and push them in to the background processor
function queue_rows($args) {
	global $wpdb;

	$tables = $args['tables'];
	$src_table_prefix = $args['src_table_prefix'];
	$dst_table_prefix = $args['dst_table_prefix'];
	$src_site_url = $args['src_site_url'];
	$dst_site_url = $args['dst_site_url'];
	$excluded_options = $args['excluded_options'];

	foreach ($tables as $table) {
		$limit = 200;
		$offset = 0;

		$data = array(
			'table' => $table,
			'src_prefix' => $src_table_prefix,
			'dst_prefix' => $dst_table_prefix,
			'src_url' => preg_replace('/^https?:\/\//', '', $src_site_url),
			'dst_url' => preg_replace('/^https?:\/\//', '', $dst_site_url),
			'rows' => $rows,
			'offset' => $offset,
			'excluded_options' => $excluded_options
		);

		$rows = $data['rows'] = [];

		// If $rows is empty then just push to queue
		if (!count($rows)) {
			as_enqueue_async_action('copy_data_to_destination_site', array($data));
		}

		while ($rows) {
			// Queue the row
			as_enqueue_async_action('copy_data_to_destination_site', array($data));

			// Update offset
			$offset += $limit;

			// Get the next set of rows
			$query = "SELECT * FROM {$src_table_prefix}{$table} LIMIT {$limit} OFFSET {$offset};";

			$rows = $wpdb->get_results($query);
			$data['rows'] = $rows;
			$data['offset'] = $offset;
		}
	}
}

add_action('add_attachment', 'copy_across_to_multisite', 1000, 1);
add_action('attachment_updated', 'copy_across_to_multisite', 1000, 1);

function copy_across_to_multisite($attachment_id) {
	// Insert media file into each site in the network.
	remove_action('add_attachment', 'copy_across_to_multisite', 1000);
	remove_action('attachment_updated', 'copy_across_to_multisite', 1000);

	insert_media_file_into_sites($attachment_id);
}

function insert_media_file_into_sites($original_attachment_id) {


	if (!is_multisite())
		return;

	if (!$original_attachment_id) {
		return;
	}

	// We only want to broadcast from Central
	if (get_current_blog_id() !== CENTRAL_SITE) {
		// Update the meta on the post befoer bombing out
		// Check if we know the orignal post and update that if we do
		$central_post_id = get_post_meta($original_attachment_id, '_gc_original_post', true);
		if (isset($central_post_id)) {
			// cache the current site id
			$site_id = get_current_blog_id();
			switch_to_blog(CENTRAL_SITE);
			// Get the gc router or create new one if it doesn't exist
			$gc_router = get_post_meta($central_post_id, '_grand_central_router', true) ?: array();

			if (!isset($gc_router[$site_id]) && !isset($gc_router[$site_id][$original_attachment_id])) {
				// Add the post we're editing to it
				$gc_router[$site_id]['ID'] = $original_attachment_id;
			}
			// Set it to detached so we know not to overwrite it later
			$gc_router[$site_id]['detached'] = 1;
			update_post_meta($central_post_id, '_grand_central_router', $gc_router);
			restore_current_blog();
		}

		update_post_meta($original_attachment_id, '_gc_detached', 1);
		return;
	}

	$file_path       = get_attached_file($original_attachment_id);
	$file_url        = wp_get_attachment_url($original_attachment_id);
	$file_type_data  = wp_check_filetype(basename($file_path), null);
	$file_type       = $file_type_data['type'];
	$timeout_seconds = 15; //seconds timeout to download the image from the central site


	$sites = get_sites(array(
		'site__not_in' => array(CENTRAL_SITE)
	));
	$original_post   = get_post($original_attachment_id);
	$post_complete = prepare_post_meta_and_terms_for_insert($original_attachment_id, $original_post);
	$post_data = $post_complete['post_data'];
	$post_terms = $post_complete['post_terms'];
	$post_meta = get_post_custom($original_attachment_id);

	foreach ($sites as $site) {

		$site_id = $site->blog_id;

		// Loop through sub-sites in network.
		$gc_router = get_post_meta($original_attachment_id, '_grand_central_router', true) ?: array();

		switch_to_blog($site_id);

		// If the blog is already in the gc_router then we already know the post id
		if (isset($gc_router[$site_id])) {
			if (isset($gc_router[$site_id]['detached']) && 1 == $gc_router[$site_id]['detached']) {
				// This post is detached so don't update
				restore_current_blog();
				continue;
			}

			// Set the id of the post so we can update it
			$post_data['ID'] = $gc_router[$site_id]['ID'];
		}
		// Otherwise, if a post exists in the target site and has the same slug, type and status, let's assume it's the same thing and update it
		elseif ($destination_post = get_posts(array('name' => $post_data['post_name'], 'post_type' => $post_data['post_type'], 'post_status' => 'inherit'))) {
			// If the ID is the same then we know it's the same post that would have been created during the clone
			if ($original_attachment_id == $destination_post[0]->ID && !get_post_meta($destination_post[0]->ID, '_gc_detached', true)) {
				$post_data['ID'] = $destination_post[0]->ID;
			}
		}
		// If none of the above, the post doesn't exist so we want to create a new one. To do this we need to make sure there's no post id set in the data
		else {
			unset($post_data['ID']);
		}

		// Sideload media file
		$sideload_result = sideload_media_file($file_url, $file_type, $timeout_seconds);

		// If an error occurred while trying to sideload the media file; continue to next blog.
		if (!$sideload_result || !empty($sideload_result['error'])) {
			_one_log('sideload_media_file = error');
			ob_start();
			var_dump($sideload_result);
			$sideload_error_string = ob_get_clean();
			_one_log($sideload_error_string);
			_one_log('sideload_media_file = error end');
			switch_to_blog(CENTRAL_SITE);
			continue;
		}

		$new_file_path = $sideload_result['file']; //tmp/image4--34-.kfkf
		$new_file_type = $sideload_result['type'];

		// Insert media file into uploads directory.
		$inserted_attachment_id = insert_media_file($new_file_path, $new_file_type, $post_data);

		foreach ($post_meta as $meta_key => $meta_values) {
			// we do not need these redirects
			if ('_wp_old_slug' === $meta_key) {
				continue;
			}
			// Get rid of the edit lock
			if ('_edit_lock' === $meta_key) {
				continue;
			}

			foreach ($meta_values as $meta_value) {
				if ('_grand_central_router' === $meta_key || preg_match("/^_gc_.*_router$/", $meta_key)) {
					continue;
				}
				// this is skipped and wont be coppied from the original attachment,
				// it will be a new path, for the multi-sub-site
				if ('_wp_attached_file' === $meta_key) {
					continue;
				}

				if ('condition' === get_post_type($inserted_attachment_id)) {
					// We need to check and see if the value should be replaced with a branch specific value
					// For example, if the linked  post has a different ID on the branch site
					// This is stored in the post meta as _cg_{field_name}_router
					$gc_post_router_raw = $post_meta["_gc_{$meta_key}_router"];

					if ($gc_post_router_raw[0]) {
						$gc_post_router = unserialize($gc_post_router_raw[0]);
						$meta_value = $gc_post_router[$site_id];
					}
				}
				if (is_serialized($meta_value)) {
					$meta_value = unserialize($meta_value);
				}
				update_post_meta($inserted_attachment_id, $meta_key, $meta_value);
			}
		}

		if ($inserted_attachment_id > 0) {
			$gc_router[$site_id]['ID'] = $inserted_attachment_id;
		}

		// Add a new meta value pointing back to the original post
		update_post_meta($inserted_attachment_id, '_gc_original_post', $original_attachment_id);

		switch_to_blog(CENTRAL_SITE);

		update_post_meta($original_attachment_id, '_grand_central_router', $gc_router);
	}
}

function sideload_media_file($file_url, $file_type, $timeout_seconds) {

	// Gives us access to the download_url() and wp_handle_sideload() functions.
	require_once(ABSPATH . 'wp-admin/includes/file.php');

	// required only for dev, because of invalid SSLs
	add_filter('https_local_ssl_verify', '__return_false');
	add_filter('https_ssl_verify', '__return_false');

	// Download file to temp dir.
	$temp_file = download_url($file_url, $timeout_seconds);

	$capturing_error = is_wp_error($temp_file);
	if ($capturing_error) {
		_one_log('capturing_error = start');
		ob_start();
		var_dump($capturing_error);
		$error_string = ob_get_clean();
		_one_log($error_string);
		_one_log('capturing_error = end');
		return false;
	}

	// Array based on $_FILE as seen in PHP file uploads.
	$file = array(
		'name'     => basename($file_url),
		'type'     => $file_type,
		'tmp_name' => $temp_file,
		'error'    => 0,
		'size'     => filesize($temp_file),
	);

	$overrides = array(
		// Tells WordPress to not look for the POST form
		// fields that would normally be present, default is true,
		// we downloaded the file from a remote server, so there
		// will be no form fields.
		'test_form'   => false,

		// Setting this to false lets WordPress allow empty files � not recommended.
		'test_size'   => true,

		// A properly uploaded file will pass this test.
		// There should be no reason to override this one.
		'test_upload' => true,
	);

	// Move the temporary file into the uploads directory.
	// Sanitizes file names, checks extensions for mime type, and moves the file to the appropriate directory within the uploads directory.
	return wp_handle_sideload($file, $overrides);
}

function insert_media_file($file_path = '', $file_type = '', $post_data  = '') {

	if (!$file_path || !$file_type) {
		_one_log('Error, insert_media_file = empty file path used or empty file type,  ' .  $file_path  . ' - ' . $file_type);
		return 0;
	}

	// Get the path to the uploads directory.
	$wp_upload_dir = wp_upload_dir();
	// empty if its a new attachment
	$original_post_id = isset($post_data['ID']) && !empty($post_data['ID']) ? $post_data['ID'] : '';

	// Prepare an array of post data for the attachment.
	$attachment_data = array(
		'guid'           => $wp_upload_dir['url'] . '/' . basename($file_path),
		'post_mime_type' => $file_type,
		'post_title'     => sanitize_file_name(preg_replace('/\.[^.]+$/', '', basename($file_path))),
		'post_content'   => '',
		'post_status'    => 'inherit',
		'ID'				  => $original_post_id
	);

	// Insert the attachment.
	$inserted_attachment_id   = wp_insert_attachment($attachment_data, $file_path, $original_post_id);
	$inserted_attachment_path = get_attached_file($inserted_attachment_id);

	// Update the attachment metadata.
	update_inserted_attachment_metadata($inserted_attachment_id, $inserted_attachment_path);

	return $inserted_attachment_id;
}

function update_inserted_attachment_metadata($inserted_attachment_id, $file_path) {

	// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
	require_once(ABSPATH . 'wp-admin/includes/image.php');

	// Generate metadata for the attachment and update the database record.
	$attach_data = wp_generate_attachment_metadata($inserted_attachment_id, $file_path);
	wp_update_attachment_metadata($inserted_attachment_id, $attach_data);
}

add_action('copy_data_to_destination_site', 'mm_copy_data_to_destination_site');
function mm_copy_data_to_destination_site($item) {

	global $wpdb;

	$limit = 0;

	$table = $item['table'];
	$src_table_prefix = $item['src_prefix'];
	$dst_table_prefix = $item['dst_prefix'];
	$src_url = $item['src_url'];
	$dst_url = $item['dst_url'];
	$excluded_options = $item['excluded_options'];

	$query = "SELECT * FROM {$src_table_prefix}{$table};";
	$rows = $wpdb->get_results($query);

	// We don't want to copy anything frm actionscheduler
	if (strpos($table, 'actionscheduler') > -1) {
		$rows = [];
	}

	// Some data appears to be getting into some tables before this runs so let's make sure it's clean
	$table_exists = $wpdb->get_var("SHOW TABLES FROM {$wpdb->dbname} WHERE Tables_in_{$wpdb->dbname} = '{$dst_table_prefix}{$table}'");



	if ($table_exists) {
		$query_drop = "DROP TABLE {$dst_table_prefix}{$table}";
		$wpdb->query($query_drop);
	}
	// $query_create_table = "CREATE TABLE {$dst_table_prefix}{$table} LIKE {$src_table_prefix}{$table};";
	$query_create_table = $wpdb->get_var("SHOW CREATE TABLE `{$src_table_prefix}{$table}`", 1);

	$query_create_table = preg_replace("|$src_table_prefix|", $dst_table_prefix, $query_create_table);


	// If the query has a constraint (todo: on a table that doesn't exist yet), defer it and add it to the back of the queue
	preg_match_all('/CONSTRAINT.*(' . $dst_table_prefix . '[^\'\"\` ]+)/', $query_create_table, $matches);

	foreach ($matches[1] as $constraint) {
		$table_exists = $wpdb->get_var("SHOW TABLES FROM {$wpdb->dbname} WHERE Tables_in_{$wpdb->dbname} = '{$constraint}'");
		if ($constraint !== $dst_table_prefix . $table && !$table_exists) {

			as_enqueue_async_action('copy_data_to_destination_site', array($item));
			return;
		}
	}

	$wpdb->query($query_create_table);

	// Get the data ready for import
	foreach ($rows as $row) {

		$data = array();

		// If we're on the options table then we need to replace some data
		if ('options' == $table) {
			foreach ($excluded_options as $key => $option_to_keep) {
				if ($row->option_name == $option_to_keep['option_name']) {

					// We don't want to include the "central." in the siteurl so remove it
					if ('siteurl' == $key || 'home' == $key) {
						$row->option_value = str_replace('central.', '', $option_to_keep['option_value']);
					} else {
						$row->option_value = $option_to_keep['option_value'];
					}
					unset($excluded_options[$key]);
					break;
				}
				// The option_names for wp_user_roles are different so we need to check them separately
				else if ($src_table_prefix . 'user_roles' == $row->option_name && $dst_table_prefix . 'user_roles' == $option_to_keep['option_name']) {
					$row->option_name = $option_to_keep['option_name'];
					$row->option_value = $option_to_keep['option_value'];
					unset($excluded_options[$key]);
					break;
				}
			}
		}

		if (
			strpos('_transient', $row->option_name) > -1 ||
			strpos('_edit_lock', $row->meta_key) > -1 ||
			strpos('_edit_last', $row->meta_key) > -1
		) {
			continue;
		}

		$formats = array();

		foreach ($row as $col => $cell) {

			// Need to create another search string with escaped slashes to match urls in blocks
			$escaped_str = str_replace('/', '\/', $src_url);

			// Do search and replace
			if (
				strpos($cell, $src_url) > -1 ||
				strpos($cell, $escaped_str) > -1
			) {


				$search_str = $src_url;
				$replace_str = $dst_url;
				if (strpos($cell, $escaped_str) > -1) {
					$search_str = $escaped_str;
					// Need to create another replace sting with escaped slashes to match urls in blocks
					$replace_str = str_replace('/', '\/', $dst_url);
				}

				$serialized = false;
				// Is the cell serialized?
				if (is_serialized($cell)) {
					$serialized_cell = unserialize($cell);

					// Recursive search and replace
					$serialized_cell = recurse_search_replace($serialized_cell, $search_str, $replace_str);

					$cell = serialize($serialized_cell);
				} else {
					$cell = str_replace($search_str, $replace_str, $cell);
				}
			}

			$data[$col] = $cell;
			$formats[] = '%s';
		}

		$columns = '`' . implode('`, `', array_keys($data)) . '`';
		$formats = implode(', ', $formats);
		$query_insert = "INSERT INTO {$dst_table_prefix}{$table} ({$columns})
			VALUES ({$formats});";
		$prepared_query = $wpdb->prepare($query_insert, $data);
		$result = $wpdb->query($prepared_query);

		// Flush the cache to make sure we see what we're supposed to see
		wp_cache_flush();
	}
}

function recurse_search_replace($array, $search_for, $replace_with) {
	foreach ($array as $key => $value) {
		// If the item is an array then recurse
		if (is_array($value)) {
			$array[$key] = recurse_search_replace($value, $search_for, $replace_with);
		} else {
			$array[$key] = str_replace($search_for, $replace_with, $value);
		}
	}
	return $array;
}
