<?php
class RevisionaryProSettingsUI {
    function __construct() {
        add_action('revisionary_settings_ui', [$this, 'actSettingsUI']);
    }

    function actSettingsUI($ui) {
        $ui->option_captions['display_pp_branding'] = __('Display PublishPress Branding in Admin', 'revisionary');

        $ui->section_captions = ['license' => __('License Key', 'revisionary'), 'branding' => __('Branding', 'revisionary')] + $ui->section_captions;

        $ui->form_options['features']['branding'] = ['display_pp_branding'];
    }
}