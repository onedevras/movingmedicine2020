(function($) {

  var queryVars = getUrlVars();

  function init() {
    $('dd > a').click(update_finder);

    if(queryVars.p) {
      $('a.' + queryVars.p).trigger('click');
      $('a.' + queryVars.c).trigger('click');
    }
    else {
      $('div > dl > dd:first-of-type > a').trigger('click');
    }

    $(window).on('resize', update_finder_height);
  }

  function update_finder() {
		var child = $(this).parent().find('dd');
		
  	if(!child.length){
  		var href = $(this).attr('href');
  		if(href) window.location.href = href;
  		return false;
  	}

    var levels = $('[data-levels]').attr('data-levels');

    $(this).parent().addClass("active").siblings().removeClass('active');

    if(levels > 2)
      $(this).parent().find('dd').first().addClass('active').siblings().removeClass('active');
    if(levels > 3)
      $(this).parent().find('dd dd').first().addClass('active').siblings().removeClass('active');

    update_finder_height();

    if($(window).width() < 768) {
      $('html, body').animate({
        scrollTop: $(this).parent().offset().top
      }, 300);
    }

  	return false;
  }

  function update_finder_height() {
    var height = 0,
      leftColumnHeight;

    $('dd.active > dl').each(function(){
      var h = $(this).outerHeight(true);
      if(h > height)
        height = h;
    });

    leftColumnHeight = $('.finder_inner dl:first-child').outerHeight(true);
    height = Math.max(height, leftColumnHeight);

    if($('.activity-finder').length > 0)
      height += $('.activity-finder dl:first-of-type').outerHeight(true);

    $('.finder_inner').css('min-height', Math.max(height, 400));

    var finderOuterContainer = $('.finder_outer-container dl');
    if(finderOuterContainer.length) {
      $('.finder_outer-container').css('min-height', finderOuterContainer[0].offsetHeight);
    }
  }

  function getUrlVars()
  {
      var vars = [], hash;
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for(var i = 0; i < hashes.length; i++)
      {
          hash = hashes[i].split('=');
          vars.push(hash[0]);
          vars[hash[0]] = hash[1];
      }
      return vars;
  }
  
  $(init);
})(jQuery);
