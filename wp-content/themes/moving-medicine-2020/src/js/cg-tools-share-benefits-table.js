(function($) {

  function init() {
    $('.share-benefits-table a[data-condition]').click(update_condition);
    $('.share-benefits-table a[data-category]').click(update_category);
	}
	
	function update_condition() {
    var table = $(this).parents('.share-benefits-table').first(),
  		condition = $(this).attr('data-condition'),
  		conditions = table.find('.share-benefits-conditions .tool-item'),
      items = table.find('div[data-condition="'+condition+'"]');
  	
  	items.addClass('active')
  		 .siblings().removeClass('active');

  	conditions.removeClass('active');
		$(this).closest('.tool-item').addClass('active');
		
		$(document).trigger('slideHeightChanged');
  	return false;
  }

  function update_category() {
		var tableContainer = $(this).parents('.table-container').first(),
        category = $(this).attr('data-category'),
				categories = tableContainer.find('.table-categories a'),
				items = tableContainer.find('div[data-category="'+category+'"]');
				
		items.addClass('active')
			.siblings()
			.removeClass('active');

		categories.removeClass('active');
		$(this).addClass('active');

		$(document).trigger('slideHeightChanged');
  	return false;
	}
  
  $(init);
})(jQuery);
