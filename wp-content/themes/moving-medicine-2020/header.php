<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
  <title><?php wp_title('&mdash;', true, 'right'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id="></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=<?php the_field('tracking_id', 'options');?>"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', '<?php the_field('tracking_id', 'options');?>');
  </script>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <a class="skip-to-content-link" href="#main">
    Skip to content
  </a>
  <?php mm_offer_location_redirect(); ?>
  <header id="main-header">
    <div class="container">
      <nav class="logo">
        <a href="<?php echo get_site_url(); ?>">
          <?php one_get_content('content-parts', 'mm_logo', array("gradient_id" => "header_gradient")); ?>
        </a>
      </nav>

      <nav class="main-navigation">
        <div class="top-nav-container">
          <div class="header-attention-button">
            <?php $header_btn = get_field('header_attention_button', 'option'); ?>
            <?php if ($header_btn) : ?>
              <a class="button" href="<?= $header_btn['url']; ?>" target=<?= $header_btn['target']; ?>""><?= $header_btn['title']; ?></a>
            <?php endif; ?>
          </div>
          <?php
          wp_nav_menu(array(
            'theme_location' => 'top-navigation',
            'container' => '',
            'menu_class' => 'top-menu',
            'walker' => new MM_Top_Nav_Walker,
          )); ?>
        </div>

        <?php wp_nav_menu(array(
          'theme_location' => 'main-navigation',
          'container' => '',
          'menu_class' => 'main-menu',
          'walker' => new MM_Main_Nav_Walker,
        ));
        ?>
      </nav>

      <nav class="hamburger">
        <span class="hamburger-icon">
          <i class="hamburger-lines" aria-label="Toggle Navigation Menu">×</i>
          </span>
      </nav>

      <a href="#" class="menu-back-button">Back</a>
    </div>
  </header>