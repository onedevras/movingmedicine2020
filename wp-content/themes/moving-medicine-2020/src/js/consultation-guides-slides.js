(function($) {

  var sections,
      section_menu,
      slide_menu,
      slide_nav,
      secondary_menu;

  function init() {

    if(!document.querySelectorAll('.consultation-guide-conversation').length)
      return;

    sections = tns({
      "container": ".sections",
      "items": 1,
      "nested": "inner",
      "loop": false,
      "slideBy": "page",
      "swipeAngle": false,
      "speed": 400,
      "controls": true,
      "controlsContainer": "#custom-controls",
      "nav": false,
      "touch": false,
      "mouseDrag": false,
      "autoHeight": true,
    });

    // "controlsText": ['<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg>',
    //                  '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z"/></svg>'],

    slide_nav = tns({
      "container": ".nav-slider",
      "axis": 'horizontal',
      "center": true,
      "autoWidth": true,
      "loop": false,
      "slideBy": 1,
      "speed": 400,
      "controls": false,
      "nav": false,
      "touch": false,
      "mouseDrag": false
    });

    section_menu = $(".consultation-guide_section-menu");
    slide_menu = $(".consultation-guide_slide-menu");
    secondary_menu = $(".consultation-header");

    // If the menu is not at the top of the window then we need to adjust for the transforms
    // (Safari seems to ignore the transform applied to the parent item)
    if(secondary_menu.offset().top === 128) {
      $(document.body).addClass("transform-adjustments");
    }

    // Bind Events
    section_menu.find('a').on( 'click', navigateToSection );
    slide_menu.find('a').on( 'click', navigateToSlide );
    sections.events.on('indexChanged', updateNavState);
    sections.events.on('transitionStart', updateControls);
    $(document).on('slideHeightChanged', updateSliderHeight);

    // Update the initial state
    updateNavState();

    // Update the control and slide nav if position-sticky isn't supported
    if(!browserSupportsPositionSticky()) {
      $('.consultation-guide .tns-controls').css({
        'position': 'fixed',
        'top': '50vh',
        'margin': '0'
      });

      $('.consultation-guide_slide-menu').css({
        'position': 'fixed',
        'transform': 'none'
      });
    }

  }

  function updateControls(event) {
    var info = sections.getInfo(),
      index = info.index;
    if(index+1 === info.slideCount) {
      $('#custom-controls button[data-controls="next"]').attr('disabled', null).on('click', function() {
        $('.patient-info-link')[0].click();
      });

    }
  }

  function browserSupportsPositionSticky() {
    var prop = 'position:';
    var value = 'sticky';
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
  
    var el = document.createElement('a');
    var mStyle = el.style;
    mStyle.cssText = prop + prefixes.join(value + ';' + prop).slice(0, - prop.length);
    
    return mStyle.position.indexOf(value) !== -1;
  }

  function navigateToSlide(event) {
    sections.goTo($(event.target).closest("li").index());
    event.preventDefault();
  }

  function navigateToSection(event) {
    var index = event.target.dataset.slideIndex;
    if(index) {
      sections.goTo(index);
      event.preventDefault();
    }
  }

  function updateNavState() {
    var info = sections.getInfo(),  
      index = info.index,
      section_item = $('.consultation-guide_section-menu a[data-slide-index='+index+']'),
      slider_item = $('.consultation-guide_slide-menu a[data-slide-index='+index+']');

    slide_nav.goTo(index);

    $('html, body').animate({ scrollTop: 0 }, 300);

    slide_menu.attr('data-current-index', index);

    if(section_item.length){
      var top_parent = section_item.parents('li').last().find('a[data-slide-index]').parent();
      $(".consultation-guide_section-menu .active").removeClass('active');
      top_parent.addClass('active');
      top_parent.attr('data-current-index', index);
    }

    if(slider_item.length){
      $(".consultation-guide_slide-menu .active").removeClass('active');
      slide_menu.find('a[data-slide-index='+index+']').parent().addClass('active');
    }

  }

  function updateSliderHeight() {
    sections.updateSliderHeight();
  }
  
  $(init);
})(jQuery);
