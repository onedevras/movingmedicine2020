(function($) {

	var body_class = 'modal-open',
		player;

  function init() {
		var modalsRes = [];
		$.each($('.resources-modal[data-modal-id]'), function (index, value) {
		 $(this).hide();
		  var classes = $(this).parents('.slide').first().attr('class'),
				slideNum = classes ? classes.match(/slide_\d+/g) : 0;
		  $(this).addClass(slideNum.length ? slideNum[0] : 'slide_10');
		 modalsRes.push($(this));
		});

	   $(document.body).append(modalsRes);

		$('[data-modal]').keyup(function(e) {
			if (e.key === "Enter") { // escape key maps to keycode `27`
				$(e.target).trigger("click");
		 }
		});
		$('[data-modal]').click(open_modal_window);
		$('[data-modal-action="close"]').click(close_modal_window);

		$(document).keyup(function(e) {
			if (e.key === "Escape") { // escape key maps to keycode `27`
				close_modal_window();
		 }
 		});

	}

  function open_modal_window() {
  	var id = $(this).attr('data-modal'),
				target = $('.modal[data-modal-id='+id+']'),
				video = $(target).find("iframe[data-video-id]");

  	if(!target.length)
			return;

		target.find('[tabindex]').attr('tabindex', 0);

		target.one('transitionend', function() {
			if(video.length) {
				try {
					player = new Vimeo.Player(video[0]);
					player.play();
				}
				catch(e) {}

				try {
					window[video[0].id].playVideo();
				}
				catch(e) {}
			}
		});

  	target.css('display', 'block');
	//timeout required for iOs lowend devices
	setTimeout(function() {
		target.addClass('open');
	}, 0);
	
  	$('body').addClass(body_class);

  	return false;
  }

  function close_modal_window() {
		var target = $('.modal.open').first(),
			video = $(target).find("iframe[data-video-id]");

  	if(!target.length)
			return;

		target.find('[tabindex]').attr('tabindex', -1);

		try {
			player.pause();
		}
		catch(e) {}

		try {
			window[video[0].id].pauseVideo();
		}
		catch(e) {}

		target.one('transitionend', removeBodyClass);
  	target.removeClass('open');

  	return false;
	}

	function removeBodyClass(event) {
		$('body').removeClass(body_class);
	}

  $(init);
})(jQuery);
