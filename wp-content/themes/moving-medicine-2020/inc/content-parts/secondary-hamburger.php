<nav class="hamburger <?php echo $args['classes'] ?: ''; ?>" tabindex="0">
  <span class="hamburger-icon">
    <i class="hamburger-lines" aria-label="Toggle Main Menu">×</i>
  </span>
</nav>