<?php if( get_field('freeform') ): ?>

<section class="section cg-tool content-section content-tool freeform-content">
	<div class="container thin center">

		<?php echo get_field('freeform'); ?>	

	</div>
</section>

<?php endif; ?>