(function($) {

  function init() {
    
    $('.close-geo').click(closeGeo);
		
  }
  
  // Closes the geo bar and drops a cookie to show that the user doesn't want to redirect
  function closeGeo(e) {
    $('.redirector').remove();

    // Drop cookie
    document.cookie = "loc_dismissed=1;max-age=60*60*24*365";
  }
  
  $(init);
})(jQuery);
