<section class="section cg-tool content-section activity-calculator">
	<div class="container mm-group">
		<div class="mm-group_inner thin center">
			<div class="intro-text">
				<p>Consider calculating their physical activity vital sign</p>
			</div>
			<div class="questions">
				<div class="question">
					<p><span class="q-yellow">Q1</span> On average, how many days each week do you do moderate or greater physical activity (like a brisk walk) ?</p>
					<div class="input">
						<input type="number" name="days" class="activity_days single" value="" min="0" max="7" step="any" />
					</div>
				</div>

				<div class="question">
					<p><span class="q-yellow">Q2</span> On those days, on average how many minutes do you do this physical activity for?</p>
					<div class="input">
						<input type="number" name="mins" class="activity_mins single" value="" min="0" max="1440" step="any" />
					</div>
				</div>
			</div>
			<div class="get-retsults-container"> 
				<button id="activity_get_results" class="get-results"> Get Results </button>
			</div>
		</div>
	</div>
	<section class="secret">
	<div class="container mm-group">
		<div class="mm-group_inner thin center">
			<div class="intro-text">
				<p>Results</p>
			</div>
			<div class="block">
				<div class="results">
					<div class="results-text-container">
						<p class="results-text high">
							Inactive.  Only  small  increases  in  physical  activity  across  the  week  can  have  large  health  benefits. Explain that just 30 minutes physical activity in one week can make a huge difference to their health.
						</p>

						<p class="results-text medium">
							Insufficiently  active.  A  good  start,  but  there  is  still  a  lot  to  be  gained  by  increasing  physical  activity  levels. 
						</p>

						<p class="results-text low">
							Active.  Well  done.  Maintaining  this  level  of  activity  is  really  important.  
						</p>
					</div>
				</div>
			</div>
			<div class="activity-graph-container">
				<div class="activity-graph" aria-hidden="true">
					<div class="meter"></div>

					<div class="relative-risk small graph-info-text bold upper">Relative Risk</div>
					<div class="bottom zero big graph-info-text">0</div>
					<div class="bottom thirty big graph-info-text">30</div>
					<div class="bottom sixty big graph-info-text">60</div>
					<div class="bottom hundred-fifty big graph-info-text">150</div>
					<div class="bottom three-hundred big graph-info-text"></div>
					<div class="minutes-per-week small graph-info-text bold upper">Minutes of Physical Activity Each Week</div>

				</div>

				<div class="restart-container">
					<button id="restart_activity" class="button restart-activity">Start Again</button>
				</div>
			</div>
		</div>
	</div>
</section>
</section>