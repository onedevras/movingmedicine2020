<section class="cg-tool content-section decision-tool">
	<div class="container thin center">

			<div class="decision-container">

				<div class="decision-inner">
					<?php $decision = get_field('decision'); ?>
					<div class="decision-not-ready">
					
						<div class="triangle-for-tools"></div>

						<div class="decision-spacer">

							<h5><?php echo $decision['not_ready']['title']; ?></h5>
							
							<div class="content">
								<?php echo $decision['not_ready']['content']; ?>
							</div>

						</div>

					</div>

					<div class="decision-ready">
					
						<div class="triangle-for-tools"></div>

						<div class="decision-spacer">

							<h5><?php echo $decision['ready']['title']; ?></h5>

							<div class="content">
								<?php echo $decision['ready']['content']; ?>
							</div>
						
						</div>

					</div>

				</div>

			</div>

	</div>
</section>