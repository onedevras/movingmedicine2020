<?php /* Template Name: Finder: Consultation */ ?>

<?php 
function mm_body_classes_for_consultation_finder($classes) {

  $classes[] = 'consultation-guide';

  return $classes;
}
add_filter('body_class', 'mm_body_classes_for_consultation_finder');
?>

<?php get_header(); ?>

<?php one_get_content('content-parts', 'article-header'); ?>

  <div class="finder_outer-container consultation-finder" id="main" data-levels="2">
    <div class="container">
      <div class="finder_inner group">
      <?php 
        if( false === ( $finder = get_transient('consultation_finder_list') ) ):
          $finder = build_consultation_finder_list();
        endif;
        if($finder): ?>
          <dl>
            <?php array_walk($finder, 'finder_from_list'); ?>
          </dl>
        <?php else: ?>
          <p>No consultations were found.</p>
        <?php endif; ?>
      </div>
    </div>
  </div>

<?php get_footer(); ?>