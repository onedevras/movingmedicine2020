<?php
add_action('revisionary_load_options_ui', '_rvy_load_revisionary_pro_options_ui');

add_action('revisionary_refresh_updates', '_rvy_clear_edd_cache', 2);

function _rvy_load_revisionary_pro_options_ui() {
    require_once(RVY_ABSPATH . '/includes-pro/SettingsProUI.php');
	$license_ui = new RevisionaryProSettingsUI();
}

function _rvy_clear_edd_cache() {
    revisionary()->keyStatus(true);
    set_transient('revisionary-pro-refresh-update-info', true, 86400);

    delete_site_transient('update_plugins');
    delete_option('_site_transient_update_plugins');

    $opt_val = get_option('rvy_edd_key');
    if (is_array($opt_val) && !empty($opt_val['license_key'])) {
        $plugin_slug = basename(REVISIONARY_FILE, '.php'); // 'revisionary-pro';
        $plugin_relpath = basename(dirname(REVISIONARY_FILE)) . '/' . basename(REVISIONARY_FILE);  // $_REQUEST['plugin']
        $license_key = $opt_val['license_key'];
        $beta = false;

        delete_option(md5(serialize($plugin_slug . $license_key . $beta)));
        delete_option('edd_api_request_' . md5(serialize($plugin_slug . $license_key . $beta)));
        delete_option(md5('edd_plugin_' . sanitize_key($plugin_relpath) . '_' . $beta . '_version_info'));
    }

    wp_update_plugins();
    //wp_version_check(array(), true);

    if (current_user_can('update_plugins')) {
        $url = remove_query_arg('rvy_refresh_updates', esc_url($_SERVER['REQUEST_URI']));
        $url = add_query_arg('rvy_refresh_done', 1, $url);
        $url = "//" . esc_url($_SERVER['HTTP_HOST']) . $url;
        wp_redirect($url);
        exit;
    }
}
