<?php

namespace app\wisdmlabs\edwiserBridge\BulkPurchase;

function getUserProfileURL($userId = '', $with_a = true, $default = '&ndash;')
{
    $url = $default;

    $user_info = get_userdata($userId);

    if ($user_info) {
        $edit_link = get_edit_user_link($userId);
        $url = $with_a ? '<a class="mucp_username_redirection" href="'.esc_url($edit_link).'">'.$user_info->user_login.'</a>' : $edit_link;
    }

    return apply_filters('mucp_user_profile_url', $url, $userId, $with_a, $default);
}

/**
 * Updating cohort id in the order meta
 *@since 2.0.1
 */
function updateCohortIdInOrderMeta($orderId, $newCohortId, $quantity)
{
    $cohortData = get_post_meta($orderId, "eb_bp_mdl_cohort_id", 1);

    if ($cohortData) {
        $cohortData[$newCohortId] = $quantity;
        // array_push($cohortData, $newCohortId);
    } else {
        $cohortData = array($newCohortId => $quantity);
    }
    update_post_meta($orderId, "eb_bp_mdl_cohort_id", $cohortData);
}
