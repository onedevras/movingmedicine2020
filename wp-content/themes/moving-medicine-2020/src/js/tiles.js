(function($) {

	var tiles;

  function init() {
    fix_tile_image_height();
    $(window).on('resize', fix_tile_image_height);
  }

  // Ensures that tile images are always square
  function fix_tile_image_height() {
  	tiles = $('.tiles');
    tiles.each(function() {
    	var width = $(this).find('.tile').first().outerWidth(),
	  			image = $(this).find('.tile .image');
	  	image.css('height', width);
    });
  }
  
  $(init);
})(jQuery);
