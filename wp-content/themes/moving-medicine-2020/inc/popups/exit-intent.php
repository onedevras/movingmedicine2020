<section class="billboard-section">
	<div class="exit-intent-forms">
		<div class="exit-intent-form">
			<div class="content">
				<div class="top-bar">
					<a href="#">
						<i class="exit-intent-close" aria-label="close popup"></i>
					</a>
				</div>
				<?php if (get_field('exit_intent_title', 'options')): ?>
					<div class="heading">
						<h1><?php the_field('exit_intent_title', 'options');?></h1>
					</div>
				<?php endif ?>
				<p><?php the_field('exit_intent_content', 'options');?></p>
				<?php if (get_field('exit_intent_button_url', 'options')): ?>
					<p><a href="<?php the_field('exit_intent_button_url', 'options');?>" target="<?php the_field('exit_intent_button_open_new_tab', 'options');?>" class="button"><?php the_field('exit_intent_button_text', 'options');?></a></p>
				<?php endif ?>

			</div>
		</div>
	</div>
</section>