
<?php /* Template Name: Finder: Evidence */ ?>
<?php get_header(); ?>

<?php one_get_content('content-parts', 'article-header'); ?>

  <div class="finder_outer-container evidence-finder" id="main" data-levels="3">
    <div class="container">
      <div class="finder_inner">
        <?php 
        if( false === ( $finder = get_transient('evidence_finder_list') ) ):
          $finder = build_evidence_finder_list();
        endif;
        if($finder): ?>
          <dl>
            <?php array_walk($finder, 'finder_from_list'); ?>
          </dl>
        <?php else: ?>
          <p>No evidence was found.</p>
        <?php endif; ?>
      </div>
    </div>
  </div>

<?php get_footer(); ?>