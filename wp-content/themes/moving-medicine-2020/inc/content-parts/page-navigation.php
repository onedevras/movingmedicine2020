<?php
$ah_post = get_page_by_path( 'active-hospitals' );
$ah_post_id = $ah_post->ID;

function console($data) {
	echo "<script>";
	$output    =    explode("\n", print_r($data, true));
	foreach ($output as $line) {
			if (trim($line)) {
					$line    =    addslashes($line);
			}
	}
	echo "</script>";
}

if( false === ( $menu = get_transient('active_hospital_menu') ) ):
  $menu = build_active_hospital_menu();
endif;
?>
<header class="secondary-header active-hospital-header">
  <div class="container">
    <h1>
      <a href="<?= get_permalink($ah_post_id); ?>"><?php echo get_the_title($ah_post_id); ?></a>
    </h1>

    <nav class="active-hospitals-nav">
    	<ul>
	    	<?php
				foreach ($menu as $slug => $item):

	    		if($item['ID'] === get_the_ID() ||
	    			 $item['ID'] === get_post()->post_parent )
	    			$active = true;
	    		else
	    			$active = false;
	    		
	    		if($active) echo '<li class="active">';
	    		else echo '<li>';

	    		echo '<a href="'.$item['url'].'">'.$item['text'].'</a>';

	    		$children = $item['children'];
	    		if($children):
	    			echo '<ul>';
	    			foreach ($children as $child_item):

	    				if($child_item['ID'] === get_the_ID())
			    			$active = ' class="active"';
			    		else
			    			$active = false;

	    				echo '<li'.$active.'><a href="'.$child_item['url'].'">'.$child_item['text'].'</a></li>';
	    			endforeach;
	    			echo '</ul>';
	    		endif;

	    		echo '</li>';

	    	endforeach;
	    	?>
	    </ul>
		</nav>
		<?php one_get_content('content-parts', 'secondary-hamburger', array('classes' => 'secondary-hamburger')); ?>

  </div>
</header>