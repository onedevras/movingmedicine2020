<?php // Need to break out of existing structure to go full screen ?>

<?php 
$video = get_field('video');
if ( $video ):
	$guid = uniqid();
	$params = array(
		'controls' => 1,
		'hd' => 1,
		'autoplay' => 0,
		'loop' => 0,
		'mute' => 0,
		'title' => get_field('title') ?: false
	);
?>

<!-- Video --> 
<section class="section video-section">
	<div class="inner container full">
		<div class="container thin">
			<?php the_video($video, $params); ?>
			<div class="image poster" tabindex="0" data-modal="qr-<?php echo $guid; ?>" style="background-image: url(<?php echo get_field('image')['url']; ?>)"></div>
			<div class="content">
				<div class="play circular <?php echo get_field('title') ? '' : 'no-title' ?>"><?php echo get_svg('play-button'); ?></div>
				<?php if( get_field('title') ): ?>
				<div class="heading">
					<h4><?php the_field('title'); ?></h4>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php 
if ( $video ):
	$args = array(
		'guid' => $guid,
		'modal_video' => get_video($video, $params, $guid),
		'title' => get_field('title') ?: false
	);
	one_get_content('content-parts', 'video-modal', $args);
endif; 
?>

<?php endif; ?>