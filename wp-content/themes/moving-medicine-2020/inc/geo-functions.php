<?php

function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . 
');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}

function mm_offer_location_redirect()
{

  $current_location = getenv('HTTP_GEOIP_COUNTRY_CODE');
  if ('GB' === $current_location) {
    $current_location = getenv('HTTP_GEOIP_REGION');
  }
  $current_city = getenv('HTTP_GEOIP_CITY');

  $available_locations = build_list_of_supported_locations(true);

  // Check to see if the city's listed in available locations, 
  // Also check whether the country field is populated in case of a city
  // with a common name
  if(isset($available_locations[$current_city])) {
    if(isset($available_locations[$current_city]['country'])) {
      if($available_locations[$current_city]['country'] === $current_location) {
        $current_location = $current_city;
      }
    }
    else {
      $current_location = $current_city;
    }
  }


  // If the user hasn't dismissed the geo bar, the location is available and we're not at that current location
  if (!isset($_COOKIE['loc_dismissed']) &&
    isset($available_locations[$current_location]['url']) &&
    ($_SERVER[HTTPS] ? 'https://' : 'http://') . $_SERVER[HTTP_HOST] !== $available_locations[$current_location]['url'] ):

      $location_url = $available_locations[$current_location]['url'];
      $location_name = $available_locations[$current_location]['name'];
      $message = get_field('redirection_message', 'options') ?: "There is content specific to your location, {{location}}. Would you like to go to it?";
    ?>

    <aside class="redirector">
      <div class="container">
        <p><?php echo str_replace('{{location}}', $location_name, $message) ?> <a class="button small rounded dark-bg" href="<?= $location_url ?>">Continue</a></p>
        <div class="close-geo" aria-label="close"><?php echo get_svg('close'); ?></div>
      </div>
    </aside>

<?php

  endif;
}

/**
 * Create list of available locations
 *
 * Array keys will be location identifier andvalues will be the lin to the site
 *
 * @param boolean $force Force a new list ot be built
 *
 * @return Array The list of supported locations
 *
 */
function build_list_of_supported_locations($force = false)
{

  if (!$force && false !== ($supported_locations = get_transient('supported_locations')))
    return $supported_locations;

  $sites = get_sites(array(
    'site__not_in' => array(CENTRAL_SITE)
  ));
  $supported_locations = array();


  foreach ($sites as $site) {
    $site_id = $site->blog_id;
    switch_to_blog($site_id);

    if(!get_field('make_live', 'options')) {
      restore_current_blog();
      continue;
    }

    $cities = explode(',', get_field('city', 'options'));

    $countries = get_field('international_codes', 'options');

    // Make sure $country_codes is an array
    $countries = !is_array($countries) ? array($countries) : $countries;
    
    $locations = array_merge($cities, $countries);

    foreach ($locations as $location) {
      console_log($location);
      if ( isset( $location['value'] ) ) {
        // Country (ACF List item)
        if(get_field('city', 'options')) {
          continue;
        }
        $supported_locations[$location['value']]['url'] = get_bloginfo('url');
        $supported_locations[$location['value']]['name'] = $location['label'];
      }
      else {
        // City (ACF Text item)
        $supported_locations[$location]['url'] = get_bloginfo('url');
        $supported_locations[$location]['name'] = $location;;
        if($countries[0])
          $supported_locations[$location]['country'] = $countries[0]['value'];
      }
    }
    restore_current_blog();
  }

  set_transient('supported_locations', $supported_locations, 1 * MONTH_IN_SECONDS);

  return $supported_locations;
}

function get_supported_locations()
{
  return build_list_of_supported_locations();
}
