<?php

$content = get_the_content();
if($content) {
  $blocks = parse_blocks($content);
  if( $blocks && count($blocks) && 'acf/mm-billboard' === $blocks[0]['blockName'] )
    return;
}

?>

<div class="section-header">
  <div class="container section-header_inner">
    <?php if( !get_field('hide_title') ): ?>
    <h1><?php the_title() ?></h1>
    <?php endif; ?>
    <?php if (get_field('social_buttons')) : ?>
      <div class="share">
        <span>Share this</span>
        <?php get_social_share_buttons(); ?>
      </div>
    <?php endif; ?>
  </div>
</div>