<?php get_header(); ?>

<section class="outer product-page" id="main">
  <div class="inner container">
    <div class="content">

      <?php woocommerce_content(); ?>

    </div>
  </div>
</section>

<?php get_footer(); ?>