=== Bulk Purchase ===
Contributors: WisdmLabs
Tags: WordPress, Moodle, Courses, Users, Synchronization, Sell Courses, Learning Management System, LMS, LMS Integration, Moodle WordPress, WordPress Moodle, WP Moodle,
Requires at least: 4.0
Tested up to: 5.4.1
Stable tag: trunk
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Bulk purchase provides the functionality for the course bulk course purchase.

== Description ==
Bulk Purchase Integration is an extension for Edwiser Bridge and allows users to Purchase Moodle courses from a WooCommerce store on WordPress. Below are the full list of features of the Bulk Purchase extension.

== Purchase Courses in bulk ==
The extensions provide functionality to purchase Moodle courses in bulk with a WordPress website effortlessly.

== Enroll users to the courses ==
Provides the functionality for bulk user enrollment using csv file or by manually filling the forms.

== Installation ==

= Minimum Requirements =
* WordPress Version 5.2.2
* WooCommerce Version 3.7.0
* Moodle min version 3.0.3
* Moodle max Version 3.8.0
* PHP version 5.6 or greater

= Automatic Installation =


== Changelog ==

= 2.2.0 =
* Feature: Added Support to the variable product.
* Feature: Added functionality to delete group users in bulk.
* Feature: Added functionality to show course progress on enroll-students page.
* Tweak: Added functionality to process user enrollment in chunks to avoid timeout issue on enroll-students page.
* Fix: Fixed email issue when product is purchased without checking bulk purchase in frontend.

= 2.1.0 =
* Feature: Added functionality to delete group from Wordpress and the same cohort will be deleted from the Moodle.
* Feature: Added option to remove user from group on enroll-students page.
* Feature: Added functionality to reuse purchased quantity after removing user from Group.
* Feature: Added product search input box in the Add-Product dialog box of enroll-students page.
* Feature: Added functionality to refund user seats partially or fully from woocommerce orders.
* Feature: Option to enter group name on checkout page while purchasing bulk product.
* Feature: Option to edit the Group Name.
* Fix: Fixed the issue which was causing wordpress site to send so many requests to Moodle site.


= 2.0.1 =
* Fix: Fixed test email issue.
* Fix: User was unable to enter hyphen on woocommerce checkout page fixed this issue.
* Fix: Add Quantity button issue fixed in Enroll students page.
* Fix: Enrollment issue with bigger product names fixed.
* Fix: User enrollment issue in Enroll students page for existing WordPress user fixed.


= 2.0.0 =
* Feature: Added functionality to create the groups on moodle.
* Feature: Added functionality to add the products in same group.
* Feature: Functionality to add the members in group.
* Feature: Functionality to add more courses in the same group.
* Feature: Functionality to add more quantity for all the group products.
* Feature: Functionality to view group associated courses.
* Feature: Functionality to edit the group members details from user enrollment page.
* Feature: Functionality to create not editing teacher role for the group manager on moodle.
* Feature: Functionality to create manager role for the group manager on group product purchase.
* Feature: Create single group for multiple products at the time of purchase.
* Feature: Functionality to un-enroll user from group for the backend.
* Feature: Email notification on group course product purchase.
* Feature: Email notification on new members when added to the group.
* Feature: Email notification on group member removal from group.
* Feature: Functionality to edit email templates using Edwiser Bridge email manager.
* Feature: Functionality to disable email notifications.
* Feature: Functionality to display the cohort enrollment details on the manage enrollment page.
* Tweak: Woocommerce version 3.0.4 compatibility.
* Tweak: New layout for the enroll user page.
* Tweak: Used responsive data table for enroll student list.
* Tweak: On single product page removed self enroll option and added group purchase option to enable group purchase for the course product.
* Tweak: Automated self enrollment on single course product purchase, user will get enrolled to the product courses directly.


= 1.0.2 =
* Fix: Fixed the warnings on the 404 page.

= 1.0.1 =
* Feature: Made plugin translation ready.
* Feature: Do not allow to add or remove courses from a product if it is sold and number of available seats of the sold product's are greater than 0.
* Feature: Do not allow a product to be permanently deleted when it is sold and number of available seats of the sold product's are greater than 0.
* Tweak: Prevent same user enrollment in the same course product.
* Tweak: Added message "Login required to enroll users" on Students Enrollment page.
* Tweak: Added styles and animations on Students Enrollement page.
* Fix: Improved performance.
* Fix: Fixed the compatibility issue with Revolution slider and the The7 theme.

= 1.0.0 =
* Plugin Launched
