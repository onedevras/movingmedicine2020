( function( blocks, element ) {
  var el = element.createElement;

  blocks.registerBlockType( 'mm/cg-insight-heading', {
      title: 'Insight Heading',
      icon: 'universal-access-alt',
      category: 'mm-blocks',
      example: {},
      edit: function( props ) {
          return el(
              'h2',
              { className: props.className },
              'Insight'
          );
      },
      save: function( ) {
          return el(
              'h2',
              { className: 'insight-heading slide-title' },
              'Insight'
          );
      },
  } );
}(
  window.wp.blocks,
  window.wp.element
) );