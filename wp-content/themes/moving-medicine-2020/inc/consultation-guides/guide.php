<?php get_header(); ?>

<header class="secondary-header consultation-header">
  <div class="container">
    <h1>
      <a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a>
    </h1>

<?php 

$patient_types = get_the_terms(get_the_ID(), 'patient');
// There should be only one of patient type
foreach ($patient_types as $patient_type) {
  $patient_slug = $patient_type->slug;
}

$guide = $args['guide']; 

// Adding this to look for the fields directly
// Changing the name of the slide-title block made the 
// keys disappear. Probaby won't be needed in prod
$fields = array(
  // field name => key pairs
  'engage_5' => 'field_5e57f5b68cab3',
  'explore_5' => 'field_5e57f72ee9c03',
  'decide_5' => 'field_5e57f78fe9c0a',
  'engage_m' => 'field_5e57f83bcad49',
  'focus_m' => 'field_5e57f822cad41',
  'explore_m' => 'field_5e58fbe40ed1f',
  'strengthen_m'  => 'field_5e58fc120ed25',
  'decide_m' => 'field_5e58fc9f0ed2c',
  'plan_m' => 'field_5e58fcda0ed33',
  'support_m' => 'field_5e58fd1c0ed39',
);


// The one minute guide: A single section
if(preg_match('/^one/', $guide)) {
  $sections = get_field( $guide );
  $result = get_content_for_section( $sections );
  $menu[ $guide ] = $result['menu'];
  $html .= $result['html'];
}
// The five and more minute guides: Multiple sections
else { 
  $sections = get_field( $guide );  
  
  $count = 0;
  foreach($sections as $key => $section) {
    $label = get_field_object( $fields[ $key ] )['label'];
    $result = get_content_for_section( $section, $count, $key );
    $menu[ $guide ][ $label ] = $result['menu'];
    $count = $result['count'];
    $html .= $result['html'];
  }
}

$patient_info = get_field('patient_info_finder', 'options');
$patient_info_link = "";
if($patient_info) {
  $url = get_permalink($patient_info);
  // $condition = get_post_field( 'post_name', get_post() );
  $condition = slugify(get_the_title());
  $patient_info_link .= "<li><a class='patient-info-link' href='$url?p=$patient_slug&c=$condition'>Patient Info</a></li>";
}

// Output the top nav
generate_menu_with_classes($menu[ $guide ], 'consultation-guide_section-menu', $patient_info_link);
// Output the html ?>
    <?php one_get_content('content-parts', 'secondary-hamburger', array('classes' => 'secondary-hamburger')); ?>
  </div>
</header>
<div class="tns-controls" id="custom-controls" aria-label="Use arrow keys to navigate" tabindex="0">
  <button data-controls="prev" aria-label="Previous" tabindex="-1" aria-controls="main" disabled>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"></path></svg>
  </button>
  <button data-controls="next" aria-label="Next" tabindex="-1" aria-controls="main" disabled>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z"></path></svg>
  </button>
</div>
<div class="sections" id="main">

  <?php echo $html; ?>

</div>

<?php // Output the slides nav ?>
<nav class="consultation-guide_slide-menu">
  <ul class="nav-slider">
  <?php
  global $slide_num;
  $slide_num = 0;
  array_walk($menu[ $guide ], 'array_to_list', array(
    'multi' => false
  ));
  ?>
  </ul>
</nav>
